var searchData=
[
  ['samples',['samples',['../class_ray_tracer.html#a352f7cb6352afbc395bac32df143982e',1,'RayTracer']]],
  ['scene',['scene',['../class_ray_tracer.html#ac9cdbb6f2d03e8c1fe9895ac87d64589',1,'RayTracer']]],
  ['shadow_5fsamples',['shadow_samples',['../class_ray_tracer.html#aca8693132318560401e7196689f2aaf3',1,'RayTracer']]],
  ['smooth_5fshading',['smooth_shading',['../class_surface.html#aa7675dc7633490a99cd39620054e2bdb',1,'Surface']]],
  ['start',['start',['../struct_b_v_h_build_entry.html#aa2488e70cce3d1834a184e8472a843fe',1,'BVHBuildEntry::start()'],['../struct_b_v_h_flat_node.html#a574e6ec522dfaf5ac6ed6263877c0358',1,'BVHFlatNode::start()']]],
  ['surface',['surface',['../structhit__rec.html#acf7ee9d8bc9a6080f672b55bc9fa632a',1,'hit_rec']]],
  ['surfaces',['surfaces',['../class_mesh.html#acbef3f84de109c5f89ee4aa377a3657c',1,'Mesh::surfaces()'],['../class_scene.html#a0196c28bebf7f0cd29345066887ac4f1',1,'Scene::surfaces()']]]
];
