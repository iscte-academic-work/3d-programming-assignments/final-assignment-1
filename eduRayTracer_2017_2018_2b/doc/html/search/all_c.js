var searchData=
[
  ['m',['m',['../class_surface.html#a99bb89c6eeea7dd90b3fda098b51422b',1,'Surface']]],
  ['main_5floop',['main_loop',['../utils_8cpp.html#adbd1409868b52e433ed84baf91620dff',1,'main_loop(RayTracer &amp;renderer, void(*callback)(RayTracer *, SDL_Event &amp;), bool view_look_at_sphere, bool refresh_bvh, bool default_events):&#160;utils.cpp'],['../utils_8hpp.html#adbd1409868b52e433ed84baf91620dff',1,'main_loop(RayTracer &amp;renderer, void(*callback)(RayTracer *, SDL_Event &amp;), bool view_look_at_sphere, bool refresh_bvh, bool default_events):&#160;utils.cpp']]],
  ['mat',['Mat',['../class_mat.html',1,'Mat'],['../class_mat.html#a7205342eec3270c1aa954f35d8af27c7',1,'Mat::Mat()'],['../class_mat.html#a8ade1cac47d130a334e4e800d1111875',1,'Mat::Mat(Init t)'],['../class_mat.html#a1e30e252a1e556ac5e131edac7b08a23',1,'Mat::Mat(const Mat &amp;m)'],['../class_mat.html#a9bc6ed47532e2ce5174aac9c80a20ba4',1,'Mat::Mat(const double m[4][4])']]],
  ['mat_2ecpp',['Mat.cpp',['../_mat_8cpp.html',1,'']]],
  ['mat_2ehpp',['Mat.hpp',['../_mat_8hpp.html',1,'']]],
  ['material',['Material',['../class_material.html',1,'Material'],['../class_material.html#aa798c4ee73c9078a8c5c23fee3edebae',1,'Material::Material(Vec _cr, Vec _cp, Vec _ca, double _p, double _decay, double _rft, bool _emissive, bool _refl, bool _refr, bool _bump, const char *_filename)'],['../class_material.html#a137e987401b63eb7c6c27c3e38bc74b5',1,'Material::Material()']]],
  ['material_2ecpp',['Material.cpp',['../_material_8cpp.html',1,'']]],
  ['material_2ehpp',['Material.hpp',['../_material_8hpp.html',1,'']]],
  ['max',['max',['../class_box.html#ae853f5935d9a8aa3823993d6d80e0300',1,'Box']]],
  ['maxdimension',['maxDimension',['../class_box.html#a09a048ff5995a621847e675e9bf51856',1,'Box']]],
  ['mesh',['Mesh',['../class_mesh.html',1,'Mesh'],['../class_mesh.html#a580c671b9f4f22257a82c5ac252e311c',1,'Mesh::Mesh(const char *filename, Material *_m, bool _smooth_shading)'],['../class_mesh.html#ae2598c2ab155e0ad3182798a5c8efbc0',1,'Mesh::Mesh(std::string _id, const char *filename, Material *m, bool _smooth_shading)']]],
  ['mesh_2ecpp',['Mesh.cpp',['../_mesh_8cpp.html',1,'']]],
  ['mesh_2ehpp',['Mesh.hpp',['../_mesh_8hpp.html',1,'']]],
  ['meshfromid',['meshFromID',['../class_scene.html#ad7dfd803c8a3e7c5379ec9866206b4f3',1,'Scene']]],
  ['min',['min',['../class_box.html#ad1f3063a392fa6d1fc2b5aec6d8f0b3c',1,'Box']]],
  ['mint',['mint',['../struct_b_v_h_traversal.html#a996b4877b7c46d9a9fce5b3b32553c8e',1,'BVHTraversal']]],
  ['move',['move',['../class_camera.html#ab08bb2f3987c704bb67aa9475f617976',1,'Camera']]],
  ['move_5fat',['move_at',['../class_camera.html#a6b34435333ba29dca4a9e136322a8923',1,'Camera']]],
  ['mult',['mult',['../class_vec.html#a40f86c7549206f61a8b92c0b0f734b7e',1,'Vec']]]
];
