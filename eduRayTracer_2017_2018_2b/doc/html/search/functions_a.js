var searchData=
[
  ['light',['Light',['../class_light.html#aa603d04b2e28195a4819e34ad2604df6',1,'Light::Light(Vec _o, Vec _cl, double _decay, Surface *_geode)'],['../class_light.html#a0ed414882edab464744201553a2ebf4e',1,'Light::Light(Vec _o, Vec _cl, double _decay)']]],
  ['lightbulb',['LightBulb',['../utils_8cpp.html#a37bdfd1760746e99eb5ca412b996b518',1,'LightBulb(Scene *scene, const Vec &amp;pos, const Vec &amp;cl, double decay, Transform *tf):&#160;utils.cpp'],['../utils_8hpp.html#a37bdfd1760746e99eb5ca412b996b518',1,'LightBulb(Scene *scene, const Vec &amp;pos, const Vec &amp;cl, double decay, Transform *tf):&#160;utils.cpp']]],
  ['loadppm',['loadPPM',['../class_material.html#a7ec5589e2e78d79fa32c25ee00a3b824',1,'Material']]],
  ['look_5fat',['look_at',['../class_camera.html#aa16f32bf8c56b12f31dfcf6f39231431',1,'Camera']]],
  ['lookat',['LookAt',['../class_mat.html#a0c3c288d54e1da52cfdc6282957518aa',1,'Mat']]]
];
