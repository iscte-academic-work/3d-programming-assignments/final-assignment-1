var searchData=
[
  ['addchild',['addChild',['../class_transform.html#a6d505ddbe3c2e9df2c3e42dd2630476f',1,'Transform']]],
  ['addlight',['addLight',['../class_scene.html#a5715863fe55bc46732c50ffc496e0f33',1,'Scene']]],
  ['addsurface',['addSurface',['../class_scene.html#add3094f0f28ce5af3ceba0c73332cff9',1,'Scene::addSurface(Surface *_surface, std::string _id)'],['../class_scene.html#a9ecf32c817a0b506a7bec7d0cca62993',1,'Scene::addSurface(Surface *_surface)']]],
  ['addsurfaces',['addSurfaces',['../class_mesh.html#afa6d51dcd0b3636a992b509621afef87',1,'Mesh::addSurfaces()'],['../class_sphere.html#a52b2eaae5dac35fa93b5e254af875828',1,'Sphere::addSurfaces()'],['../class_surface.html#a6eaec7d61d4a87ce7cd42089487850bf',1,'Surface::addSurfaces()'],['../class_triangle.html#a78d01c00d6c3ddd7cd15b7d8270622fa',1,'Triangle::addSurfaces()']]]
];
