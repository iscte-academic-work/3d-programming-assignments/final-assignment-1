var searchData=
[
  ['i',['i',['../struct_b_v_h_traversal.html#a36b5ae53c0e261971cecdddf6c1812b8',1,'BVHTraversal']]],
  ['id',['id',['../class_surface.html#a60490e6d2fe723957e8814289f538624',1,'Surface::id()'],['../class_transform.html#a9460cedd048ae3d6a8e0680f36638600',1,'Transform::id()']]],
  ['image',['image',['../class_ray_tracer.html#addc15de08e27710ac4fd444dec9319b5',1,'RayTracer']]],
  ['init',['Init',['../class_mat.html#a81e79dbc41eec4017d02700a3e2e160b',1,'Mat']]],
  ['inv_5fd',['inv_d',['../class_ray.html#a28fc10d4a43d1a1e11111cee9f0ea2c0',1,'Ray']]],
  ['inv_5fnormal',['inv_normal',['../class_sphere.html#adb39e77a4ce7a55f090af7be040fab7b',1,'Sphere']]],
  ['invert',['invert',['../class_mat.html#aff86e230a7fdd2e204fc5e19dfce6b5c',1,'Mat']]],
  ['isbumpmapped',['isBumpMapped',['../class_material.html#a9456e7bde8f91663bb7bae200877e45a',1,'Material']]],
  ['isemissive',['isEmissive',['../class_material.html#a6175de849601404346db9607b44986dd',1,'Material']]],
  ['isreflective',['isReflective',['../class_material.html#a4d14399b390d6795965b51ae9f51e9f4',1,'Material']]],
  ['isrefractive',['isRefractive',['../class_material.html#ad7ca9400c928202eaca2082e291125b1',1,'Material']]],
  ['issmoothshaded',['isSmoothShaded',['../class_surface.html#ab13775c761a7b51a95b57d300b94a9c8',1,'Surface']]],
  ['istexturemapped',['isTextureMapped',['../class_material.html#a6f2ba344ff230ccb271f60968af8b0e1',1,'Material']]]
];
