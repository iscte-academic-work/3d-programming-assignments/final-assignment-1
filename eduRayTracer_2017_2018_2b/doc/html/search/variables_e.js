var searchData=
[
  ['r',['r',['../class_camera.html#a4309dd7d7a05fd6346db6de286c6ee5a',1,'Camera']]],
  ['radius',['radius',['../class_sphere.html#a45d6c6c870fac7c2a885ad2e226334ad',1,'Sphere']]],
  ['reflective',['reflective',['../class_material.html#a396bd4e93971db0e58297864217038b4',1,'Material']]],
  ['refractive',['refractive',['../class_material.html#a9af8008c4823e80a06e541f9e4813f7d',1,'Material']]],
  ['rft',['rft',['../class_material.html#a9fccedcfa4d38d3b36f8c09a48811213',1,'Material']]],
  ['rightoffset',['rightOffset',['../struct_b_v_h_flat_node.html#afb4a21e37575d580a8f78e7e2aff1698',1,'BVHFlatNode']]],
  ['root_5ftf',['root_tf',['../class_scene.html#a979988030703212b02a2ce950b5ed0a6',1,'Scene']]]
];
