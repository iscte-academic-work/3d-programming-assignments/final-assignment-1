var searchData=
[
  ['n',['n',['../class_surface.html#a7df9cc390062b02fa30e18eaea47dc3b',1,'Surface::n()'],['../class_vertex.html#ad5c064cd79d633ab294dd6623da86163',1,'Vertex::n()']]],
  ['norm',['norm',['../class_vec.html#ae690197cdfb7a9a0c07bed671263b2bf',1,'Vec']]],
  ['normal',['normal',['../structhit__rec.html#a19e1d9c17ffdc6cd5d49e1f1caa93e3f',1,'hit_rec']]],
  ['normalize',['normalize',['../class_vec.html#ad565fbb5b8815047fc201f415e2779b2',1,'Vec']]],
  ['normals_5fat_5fvertexes',['normals_at_vertexes',['../class_mesh.html#a3600172f6279d1d50bbecf3abbb7b0bd',1,'Mesh']]],
  ['nprims',['nPrims',['../struct_b_v_h_flat_node.html#a353b7a623510bace3e8e9e99acc1f382',1,'BVHFlatNode']]],
  ['nx',['nx',['../class_camera.html#ab24cd1c55c90594697eaff3bb6b6d93a',1,'Camera']]],
  ['ny',['ny',['../class_camera.html#a61a6f5c9296cba95f70190ee228b9ef6',1,'Camera']]]
];
