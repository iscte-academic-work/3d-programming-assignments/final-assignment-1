var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz~",
  1: "bchlmrstv",
  2: "bclmrstuv",
  3: "_abcdefghilmnoprstuv~",
  4: "abcdeghiklmnoprstuvwxyz",
  5: "i",
  6: "eoz"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

