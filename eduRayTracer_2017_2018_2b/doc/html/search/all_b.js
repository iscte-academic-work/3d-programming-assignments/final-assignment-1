var searchData=
[
  ['l',['l',['../class_camera.html#a8a30c1d630ed7a772dca55e7275bc5cb',1,'Camera']]],
  ['light',['Light',['../class_light.html',1,'Light'],['../class_light.html#aa603d04b2e28195a4819e34ad2604df6',1,'Light::Light(Vec _o, Vec _cl, double _decay, Surface *_geode)'],['../class_light.html#a0ed414882edab464744201553a2ebf4e',1,'Light::Light(Vec _o, Vec _cl, double _decay)']]],
  ['light_2ecpp',['Light.cpp',['../_light_8cpp.html',1,'']]],
  ['light_2ehpp',['Light.hpp',['../_light_8hpp.html',1,'']]],
  ['lightbulb',['LightBulb',['../utils_8cpp.html#a37bdfd1760746e99eb5ca412b996b518',1,'LightBulb(Scene *scene, const Vec &amp;pos, const Vec &amp;cl, double decay, Transform *tf):&#160;utils.cpp'],['../utils_8hpp.html#a37bdfd1760746e99eb5ca412b996b518',1,'LightBulb(Scene *scene, const Vec &amp;pos, const Vec &amp;cl, double decay, Transform *tf):&#160;utils.cpp']]],
  ['lights',['lights',['../class_scene.html#a4ecc3182a80435e1c4dfbe1b20e559bd',1,'Scene']]],
  ['loadppm',['loadPPM',['../class_material.html#a7ec5589e2e78d79fa32c25ee00a3b824',1,'Material']]],
  ['local_5fn',['local_n',['../class_surface.html#ad3cebceee209a3762a5dc2550d69064c',1,'Surface::local_n()'],['../class_vertex.html#a749a6f6b57ce11fda6ce662619351939',1,'Vertex::local_n()']]],
  ['local_5fo',['local_o',['../class_light.html#a2b2d9c78fdb73ac943a6a35b221f1cfd',1,'Light::local_o()'],['../class_vertex.html#ac8b579bc65f2832530987cae7bca5c3e',1,'Vertex::local_o()']]],
  ['local_5ftf',['local_tf',['../class_transform.html#a66ac17fab8f59fa9b26ac109b16f6bf0',1,'Transform']]],
  ['look_5fat',['look_at',['../class_camera.html#aa16f32bf8c56b12f31dfcf6f39231431',1,'Camera']]],
  ['lookat',['LookAt',['../class_mat.html#a0c3c288d54e1da52cfdc6282957518aa',1,'Mat']]]
];
