var searchData=
[
  ['camera',['Camera',['../class_camera.html#ab31d5c9fab32c5a72d54b1ae99b81ef2',1,'Camera::Camera(double _l, double _r, double _t, double _b, double _d, int _nx, int _ny, int _viewport_width, int _viewport_height)'],['../class_camera.html#a01f94c3543f56ede7af49dc778f19331',1,'Camera::Camera()']]],
  ['clamp',['clamp',['../_ray_tracer_8hpp.html#a59491c5991850e50f02d5196e1ed5264',1,'RayTracer.hpp']]],
  ['cornellbox',['CornellBox',['../utils_8cpp.html#affc96c8859e998422a378d17baf5cb15',1,'CornellBox(Scene *scene, Transform *tf):&#160;utils.cpp'],['../utils_8hpp.html#affc96c8859e998422a378d17baf5cb15',1,'CornellBox(Scene *scene, Transform *tf):&#160;utils.cpp']]],
  ['cornellboxtextured',['CornellBoxTextured',['../utils_8cpp.html#a8bb50e55e89fa38966a77ec9f4b96c6d',1,'CornellBoxTextured(Scene *scene, Transform *tf):&#160;utils.cpp'],['../utils_8hpp.html#a8bb50e55e89fa38966a77ec9f4b96c6d',1,'CornellBoxTextured(Scene *scene, Transform *tf):&#160;utils.cpp']]]
];
