var searchData=
[
  ['t',['t',['../class_camera.html#a4d5917a7f2bbd9a43c476d6db6fd6f1d',1,'Camera::t()'],['../structhit__rec.html#a974e27fdf82ec6b3678d7ab07d11c4e9',1,'hit_rec::t()']]],
  ['tex_5fcoord',['tex_coord',['../structhit__rec.html#ac649739da1e743acb234e66146b1d3a6',1,'hit_rec::tex_coord()'],['../class_vertex.html#a7b05126ac048975668016c73345e01b1',1,'Vertex::tex_coord()']]],
  ['tex_5fheight',['tex_height',['../class_material.html#a64903bf3624a095b95004620cc4c3688',1,'Material']]],
  ['tex_5fimage',['tex_image',['../class_material.html#ae87327bf313e3ba962250e6bc330f990',1,'Material']]],
  ['tex_5fmap',['tex_map',['../class_material.html#aa888d8c2af8d3e4b83c84b5bf1a7a44f',1,'Material']]],
  ['tex_5fpixel',['tex_pixel',['../class_material.html#afbe529757a43277bb1d1a65c42741e68',1,'Material']]],
  ['tex_5fwidth',['tex_width',['../class_material.html#ae8dc3db8a8b55b6709017d953e8e6ce8',1,'Material']]],
  ['tf',['tf',['../class_camera.html#a33bab7beb6bd121e399776c37e529471',1,'Camera::tf()'],['../class_light.html#a33549e9047c9103a2fdf43be0825407f',1,'Light::tf()'],['../class_surface.html#a444077659d77f2e034ae163cbb103134',1,'Surface::tf()']]],
  ['tiling',['tiling',['../class_material.html#ae9546ff9da82a0f7de667d3f9f1ee3fe',1,'Material']]],
  ['tip_5fpoint',['tip_point',['../class_ray.html#a921c1b4282b604067d1e22de870c362d',1,'Ray']]],
  ['toint',['toInt',['../_ray_tracer_8hpp.html#a8d0a9e9e4bc37a5ae80b8bf3856b807e',1,'RayTracer.hpp']]],
  ['transform',['Transform',['../class_transform.html',1,'Transform'],['../class_camera.html#a3dd8170ac8e2ddb7175ac2ef5e02ac83',1,'Camera::transform()'],['../class_camera.html#a187bed1cba664e132932dcb94ab99486',1,'Camera::transform(Mat m)'],['../class_light.html#ab8637968f4709709716577ca196f5e74',1,'Light::transform()'],['../class_mesh.html#afca381025e36aeaf6dcab4f8aa89113c',1,'Mesh::transform()'],['../class_scene.html#aa164a99f7b7b95f91ad96c065da409d5',1,'Scene::transform()'],['../class_surface.html#a10655735b685f4363df818a2c6fdfb9b',1,'Surface::transform()'],['../class_vertex.html#a0910d793f80561fbf49e8a81be889afc',1,'Vertex::transform()'],['../class_transform.html#aa08ca4266efabc768973cdeea51945ab',1,'Transform::Transform()'],['../class_transform.html#a13428997892b461d999ca0d75d01ab34',1,'Transform::Transform(int _id)'],['../class_transform.html#abba40138d02697e7175fda74379b1945',1,'Transform::Transform(Mat _m, int _id)'],['../class_transform.html#aee8ca24171a9005d6bf2667d5fe04381',1,'Transform::Transform(double yaw, double pitch, double roll, double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, int _id)']]],
  ['transform_2ecpp',['Transform.cpp',['../_transform_8cpp.html',1,'']]],
  ['transform_2ehpp',['Transform.hpp',['../_transform_8hpp.html',1,'']]],
  ['translate',['Translate',['../class_mat.html#a274248708a0553b5edaabbab255afdcb',1,'Mat']]],
  ['transpose',['transpose',['../class_mat.html#a55e6cc33fc65042a4cff351fe59ae476',1,'Mat']]],
  ['triangle',['Triangle',['../class_triangle.html',1,'Triangle'],['../class_triangle.html#ad1123953a9627f69f12bb966e002390d',1,'Triangle::Triangle(Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m)'],['../class_triangle.html#a77b03e725df90ade03220f59cc63e519',1,'Triangle::Triangle(std::string _id, Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m)'],['../class_triangle.html#af8a74c4394267154cc4aeace055ede87',1,'Triangle::Triangle(Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m, bool _smooth_shading)'],['../class_triangle.html#a832e2951677446251badffbfc3936715',1,'Triangle::Triangle(std::string _id, Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m, bool _smooth_shading)']]],
  ['triangle_2ecpp',['Triangle.cpp',['../_triangle_8cpp.html',1,'']]],
  ['triangle_2ehpp',['Triangle.hpp',['../_triangle_8hpp.html',1,'']]],
  ['trianglefromid',['triangleFromID',['../class_scene.html#adf0478423fdf556ddb7759ad358e1597',1,'Scene']]]
];
