var searchData=
[
  ['invert',['invert',['../class_mat.html#aff86e230a7fdd2e204fc5e19dfce6b5c',1,'Mat']]],
  ['isbumpmapped',['isBumpMapped',['../class_material.html#a9456e7bde8f91663bb7bae200877e45a',1,'Material']]],
  ['isemissive',['isEmissive',['../class_material.html#a6175de849601404346db9607b44986dd',1,'Material']]],
  ['isreflective',['isReflective',['../class_material.html#a4d14399b390d6795965b51ae9f51e9f4',1,'Material']]],
  ['isrefractive',['isRefractive',['../class_material.html#ad7ca9400c928202eaca2082e291125b1',1,'Material']]],
  ['issmoothshaded',['isSmoothShaded',['../class_surface.html#ab13775c761a7b51a95b57d300b94a9c8',1,'Surface']]],
  ['istexturemapped',['isTextureMapped',['../class_material.html#a6f2ba344ff230ccb271f60968af8b0e1',1,'Material']]]
];
