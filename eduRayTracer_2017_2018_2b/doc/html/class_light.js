var class_light =
[
    [ "Light", "class_light.html#aa603d04b2e28195a4819e34ad2604df6", null ],
    [ "Light", "class_light.html#a0ed414882edab464744201553a2ebf4e", null ],
    [ "setTransform", "class_light.html#af1ae0f3ac995d07909a6fe6f2cc9ddb4", null ],
    [ "transform", "class_light.html#ab8637968f4709709716577ca196f5e74", null ],
    [ "cl", "class_light.html#aa556ccba60a14514c231438403b55db0", null ],
    [ "decay", "class_light.html#ae028a60a89a219e6eff0ff1b24482a71", null ],
    [ "geode", "class_light.html#aeb8b1184fc6cf93bbcc205f50bd004c8", null ],
    [ "local_o", "class_light.html#a2b2d9c78fdb73ac943a6a35b221f1cfd", null ],
    [ "o", "class_light.html#a40e3c201ef2e17b89e937833f2da9111", null ],
    [ "on", "class_light.html#a37ee0b241dbdaff4a13cc269de33a8bf", null ],
    [ "tf", "class_light.html#a33549e9047c9103a2fdf43be0825407f", null ]
];