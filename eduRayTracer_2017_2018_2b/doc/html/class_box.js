var class_box =
[
    [ "Box", "class_box.html#aca78d7db44972bfa78d46b7bbc8796f6", null ],
    [ "Box", "class_box.html#a585d0404a35714d72dc7b538a942a646", null ],
    [ "Box", "class_box.html#a2d3c07d8475cba63a7b481b40d6deb37", null ],
    [ "~Box", "class_box.html#a6a5e09398e85d602a046b429062fb9c2", null ],
    [ "expandToInclude", "class_box.html#aa440a95b15ae156b61dc6952440ecf4d", null ],
    [ "expandToInclude", "class_box.html#a91c05db61f3d6ee482f023b52beef709", null ],
    [ "hit", "class_box.html#af8eb0b6a51547413ba7b644c03ba733f", null ],
    [ "maxDimension", "class_box.html#a09a048ff5995a621847e675e9bf51856", null ],
    [ "surfaceArea", "class_box.html#a3a04958793a3430d2bcab9a819c77d33", null ],
    [ "extent", "class_box.html#ad846af015e917b52877ca9af26e771f8", null ],
    [ "max", "class_box.html#ae853f5935d9a8aa3823993d6d80e0300", null ],
    [ "min", "class_box.html#ad1f3063a392fa6d1fc2b5aec6d8f0b3c", null ]
];