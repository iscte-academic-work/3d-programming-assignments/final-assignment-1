var class_ray_tracer =
[
    [ "RayTracer", "class_ray_tracer.html#a82688d0457a19e0a96f389ef9b89f729", null ],
    [ "get_pixel_from_3D", "class_ray_tracer.html#afb77b881327160bda0aa9dd0bae7a5f5", null ],
    [ "get_shadding", "class_ray_tracer.html#a4239f82baac1501dc9bd5884e2b5eddc", null ],
    [ "get_visible_surface", "class_ray_tracer.html#a51007e02e88d5213dd47284360fcae45", null ],
    [ "getCamera", "class_ray_tracer.html#a93e5af568fdcc8f5d6a45cead69fe337", null ],
    [ "getScene", "class_ray_tracer.html#a529ef735a2c804625b8e4e6208eec9b7", null ],
    [ "ray_shading", "class_ray_tracer.html#ada62841980d0982da632d6aa53081538", null ],
    [ "render", "class_ray_tracer.html#a5a7a1453c882cf71dc64827e343c1ae4", null ],
    [ "save_to_file", "class_ray_tracer.html#a8f974dd303bcdb1de773d6fccf3127f0", null ],
    [ "setDepthRayRecursion", "class_ray_tracer.html#ac8442d74a14bb488634460d587e5ac48", null ],
    [ "setNrAntiAliasingSamples", "class_ray_tracer.html#ae355f48cf8bd6ee996c204cde4f851dc", null ],
    [ "setNrShadowSamples", "class_ray_tracer.html#acaf0197e36cd4b7dc85dc42d1fe0b60a", null ],
    [ "shadow_rays", "class_ray_tracer.html#aaa54f1ee77877741364c27665d4088ab", null ],
    [ "camera", "class_ray_tracer.html#a920e316dba30cabe8f44f61354dcdd83", null ],
    [ "hops", "class_ray_tracer.html#a93125040fd72bb71d06678bdcc0bbf08", null ],
    [ "image", "class_ray_tracer.html#addc15de08e27710ac4fd444dec9319b5", null ],
    [ "pixels", "class_ray_tracer.html#a7181bf515b74ec7d8d89b055b48b1d88", null ],
    [ "samples", "class_ray_tracer.html#a352f7cb6352afbc395bac32df143982e", null ],
    [ "scene", "class_ray_tracer.html#ac9cdbb6f2d03e8c1fe9895ac87d64589", null ],
    [ "shadow_samples", "class_ray_tracer.html#aca8693132318560401e7196689f2aaf3", null ]
];