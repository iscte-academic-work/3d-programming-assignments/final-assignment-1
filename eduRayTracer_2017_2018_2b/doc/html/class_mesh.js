var class_mesh =
[
    [ "Mesh", "class_mesh.html#a580c671b9f4f22257a82c5ac252e311c", null ],
    [ "Mesh", "class_mesh.html#ae2598c2ab155e0ad3182798a5c8efbc0", null ],
    [ "addSurfaces", "class_mesh.html#afa6d51dcd0b3636a992b509621afef87", null ],
    [ "bounding_box", "class_mesh.html#a0560f1ba7ad6b24a176ab302723b0fa2", null ],
    [ "getBBox", "class_mesh.html#acc5d10d24bcc8b127ce9033cf01cb73b", null ],
    [ "getCentroid", "class_mesh.html#aed5f345616e0d840eb59e1f4a0c77df9", null ],
    [ "hit", "class_mesh.html#abb73d5cb7028e1138cb714b34d01bbca", null ],
    [ "normals_at_vertexes", "class_mesh.html#a3600172f6279d1d50bbecf3abbb7b0bd", null ],
    [ "setMaterial", "class_mesh.html#a216201cebabdd7341983615e19239c94", null ],
    [ "setTransform", "class_mesh.html#a3a5773b7b3f1457e0e1667a8b5c49975", null ],
    [ "transform", "class_mesh.html#afca381025e36aeaf6dcab4f8aa89113c", null ],
    [ "surfaces", "class_mesh.html#acbef3f84de109c5f89ee4aa377a3657c", null ],
    [ "verts", "class_mesh.html#ab0f87ac2cccc51a28e2e296c1e665609", null ]
];