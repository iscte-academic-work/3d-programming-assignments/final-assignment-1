var class_sphere =
[
    [ "Sphere", "class_sphere.html#a890a63ff583cb88e7ec4e840b4ef5eb9", null ],
    [ "Sphere", "class_sphere.html#ad736d31cbc3078179737e203f3fe2e28", null ],
    [ "Sphere", "class_sphere.html#aa0963e7749cf2a3040b149b31cece46f", null ],
    [ "addSurfaces", "class_sphere.html#a52b2eaae5dac35fa93b5e254af875828", null ],
    [ "bounding_box", "class_sphere.html#a8e7068ae2787aa9ff47eefe23b245c9f", null ],
    [ "getBBox", "class_sphere.html#ab5aec7813daccf5bca5831cde5a30c00", null ],
    [ "getCentroid", "class_sphere.html#af245199d60ad587999c8b4e10f2c8dc0", null ],
    [ "getPosLocalCoords", "class_sphere.html#a70c60796fb681dc31dbce78e3ca8c36b", null ],
    [ "getPosWorldCoords", "class_sphere.html#ab81f16923614f7ff3f5172239135ef72", null ],
    [ "getRadius", "class_sphere.html#abc5160404dc9fa0d6a7ae8028dc1a7f1", null ],
    [ "hit", "class_sphere.html#afb9d78112a4d75bff15caf6d4e7d0362", null ],
    [ "setPosLocalCoords", "class_sphere.html#a7679526478966e1e143360dfd12d57a3", null ],
    [ "setRadius", "class_sphere.html#a43f31e0eaf092bc14fe8be5c2051e659", null ],
    [ "inv_normal", "class_sphere.html#adb39e77a4ce7a55f090af7be040fab7b", null ],
    [ "radius", "class_sphere.html#a45d6c6c870fac7c2a885ad2e226334ad", null ]
];