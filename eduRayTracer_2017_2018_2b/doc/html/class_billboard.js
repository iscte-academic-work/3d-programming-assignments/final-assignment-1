var class_billboard =
[
    [ "Billboard", "class_billboard.html#a66d24fc23c054419387c9c5794cd6951", null ],
    [ "Billboard", "class_billboard.html#a6e0c4a964797ed626ef5887eec0785bc", null ],
    [ "getAlignVertical", "class_billboard.html#a23e233cc3b365c63ad341823bbb51087", null ],
    [ "getMaterial", "class_billboard.html#acf92777d2fda920b2b64625aa450bd3c", null ],
    [ "getTransform", "class_billboard.html#ae57727f59f1ce912cb9884289c5e36a7", null ],
    [ "setAlignVertical", "class_billboard.html#a71045dffcdb7bf62c8e3534dc6d5e0eb", null ],
    [ "setMaterial", "class_billboard.html#a3c39b6f8b74eab1252daa939856bf314", null ],
    [ "camera", "class_billboard.html#ab3ec3cd19e91a85426a66070095ed243", null ],
    [ "uid", "class_billboard.html#ab27ce66552535bfb5d2f823f555dde56", null ]
];