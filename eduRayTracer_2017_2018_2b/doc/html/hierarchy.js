var hierarchy =
[
    [ "Billboard", "class_billboard.html", null ],
    [ "Box", "class_box.html", null ],
    [ "BVH", "class_b_v_h.html", null ],
    [ "BVHBuildEntry", "struct_b_v_h_build_entry.html", null ],
    [ "BVHFlatNode", "struct_b_v_h_flat_node.html", null ],
    [ "BVHTraversal", "struct_b_v_h_traversal.html", null ],
    [ "Camera", "class_camera.html", null ],
    [ "hit_rec", "structhit__rec.html", null ],
    [ "Light", "class_light.html", null ],
    [ "Mat", "class_mat.html", null ],
    [ "Material", "class_material.html", null ],
    [ "Ray", "class_ray.html", null ],
    [ "RayTracer", "class_ray_tracer.html", null ],
    [ "Scene", "class_scene.html", null ],
    [ "Surface", "class_surface.html", [
      [ "Mesh", "class_mesh.html", null ],
      [ "Sphere", "class_sphere.html", null ],
      [ "Triangle", "class_triangle.html", null ]
    ] ],
    [ "Transform", "class_transform.html", [
      [ "BillboardTransform", "class_billboard_transform.html", null ]
    ] ],
    [ "Vec", "class_vec.html", null ],
    [ "Vertex", "class_vertex.html", null ]
];