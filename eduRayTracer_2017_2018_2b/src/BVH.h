#ifndef BVH_h
#define BVH_h

#include "Box.hpp"
#include <vector>
#include <stdint.h>
#include "Surface.hpp"
#include "Ray.hpp"

//! Node descriptor for the flattened tree
struct BVHFlatNode {
  Box bbox;
  uint32_t start, nPrims, rightOffset;
};

//! \author Brandon Pelfrey
//! A Bounding Volume Hierarchy system for fast Ray-Object intersection tests
class BVH {
  uint32_t nNodes, nLeafs, leafSize;
  std::vector<Surface*>* build_prims;

  //! Build the BVH tree out of build_prims
  void build();

  // Fast Traversal System
  BVHFlatNode *flatTree;

  public:
  BVH() {};
  BVH(std::vector<Surface*>* objects, uint32_t leafSize=4);
  bool getIntersection(const Ray& ray, hit_rec *h_r, bool occlusion)  ;

  ~BVH();
};

#endif
