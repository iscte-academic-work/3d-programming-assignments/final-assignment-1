/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_RayTracer_
#define _H_RayTracer_

#include <string>
#include "Scene.hpp"
#include "Camera.hpp"


inline double clamp(double x){ return x<0 ? 0 : x>1 ? 1 : x; }
inline int toInt(double x){ return int(pow(clamp(x),1/2.2)*255+.5); }
//inline int toInt(double x){ return int(x*255); }


//! A class to create and manipulate surface materials
class RayTracer {

private:
    double tmp;

public:

    //! Pointer to the Scene to be rendered
    Scene *scene;
    
    //! Pointer to the rendering Camera 
    Camera *camera;

    //! Floating point rendered image
    Vec* image;

    //! 8-bit rendered image (built from image)
    char* pixels;

    //! Number of samples used for anti-aliasing
    int samples;

    //! Number of shadow rays for soft shadows
    int shadow_samples;

    //! Number of recursive ray tracing hops (secondary rays)
    int hops;

    //! A RayTracer constructor
    /*!
        \param _camera Pointer to the rendering Camera 
        \param _scene Pointer to the Scene to be rendered
    */
    RayTracer(Camera *_camera, Scene *_scene);

    //! Triggers the rendering process by creating viewing rays and tracing them across the scene by calling get_shadding()
    void render();

    //! Returns a pointer to the Scene object.
    /*!
     \returns A pointer to the Scene object.
     */
    Scene* getScene(){return scene;}
    
    //! Returns a pointer to the Camera object.
    /*!
     \returns A pointer to the Scene object.
     */
    Camera* getCamera(){return camera;}
    
    //! Sets the number of samples used for anti-aliasing.
    /*!
     \param _samples The number of samples used for anti-aliasing.
     */
    void setNrAntiAliasingSamples(int _samples){samples = _samples;};
 
    //! Sets the number of shadow rays for soft shadows.
    /*!
     \param _shadow_samples The number of shadow rays for soft shadows.
     */
    void setNrShadowSamples(int _shadow_samples){shadow_samples = _shadow_samples;};
    
    //! Sets the number of recursive ray tracing hops (secondary rays).
    /*!
     \param _hops The number of recursive ray tracing hops (secondary rays).
     */
    void setDepthRayRecursion(int _hops){hops = _hops;};

    //! Returns the index of the visible Surface object on a given pixel. It does this by finding the closest surface intersected by the ray that passes through the given pixel.
    /*!
        \param i Pixel coordinate
        \param j Pixel coordinate
        \param h_r Hit record that will be filled with the information regarding the visible surface
        \returns Index of the visible surface on the surfaces vector stored in the object Scene (scene.surfaces)
    */
    int get_visible_surface(int i, int j, hit_rec &h_r);


    //! Returns the projection, in pixel coordinates, of a given 3D point.
    /*!
        \param point 3D point to be projected
        \param pixel Pixel coordinates corresponding to the projection of the 3D point
    */
    void get_pixel_from_3D(Vec point, Vec &pixel);

    //! Recursively traces the scene from a given Ray (when called from render() it is a viewing/primary ray, later on it may be a reflection, refraction or shadow secondary ray).
    //! When needed, the shading equation is computed by calling get_shadding()
    /*!
        \param _scene Pointer to the Scene to be rendered (it contains all geometry for the intersection tests)
        \param r Ray to be traced recursively
        \param hops Maximum number of hops (recursion steps) still available (this number is recursively decreased whenever an intersection is observed and a secondary ray casted)
        \returns RGB vector resulting from applying integrating the shading equation along the recursive tracing process
    */
    Vec ray_shading(Scene *scene, Ray &ray, int hops);

    //! Applies the shading equation on the point defined in a given hit record
    /*!
        \param h_r Hit record that provides the information regarding the point and its supporting surface to shade
        \param bump Flag stating if bump mapping is to be considered in the shadding equation
        \returns RGB vector resulting from applying the shading equation
    */
    Vec get_shadding(hit_rec &h_r, bool bump);
    
    //! Casts a shadow ray recursively from a given surface point to a Light source. Returns how much light is attenuated by intersected surfaces. 
    /*!
        \param _scene Pointer to the Scene to be rendered
        \param h_r Hit record holding information regarding the point to be shadowed
        \param light Pointer to the Light source 
        \param rnd Randomised vector to be used when casting the shadow ray
        \returns How much light is attenuated by intersected surfaces
    */
    Vec shadow_rays(Scene *scene, hit_rec h_r, Light* light, Vec rnd);

    //! Saves the last rendering to a .ppm file with a timestamped name
    void save_to_file();


};


#endif
