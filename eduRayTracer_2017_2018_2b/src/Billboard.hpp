/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Billboard_
#define _H_Billboard_

#include "Mat.hpp"
#include "Camera.hpp"
#include "Transform.hpp"
#include "BillboardTransform.hpp"
#include "Scene.hpp"
#include "Material.hpp"
#include "Triangle.hpp"
#include <iostream>
#include <vector>
#include <string>

//! A class to handle Billboards of simple textures. The billboard is composed of two quads rotated 90 degs from each other.
class Billboard {

private:

    BillboardTransform *bb_tf1;
    Transform *bb_tf2;

    //! A pointer to the material associated to this billboard.
    Material *m;

    //! One of the triangles that compose the billboard
    Triangle* t1;
    //! One of the triangles that compose the billboard
    Triangle *t2;
    //! One of the triangles that compose the billboard
    Triangle* t3;
    //! One of the triangles that compose the billboard
    Triangle *t4;

public:

	//! A unique identififier for this transform
	std::string uid;

	//! A pointer to the camera
	Camera* camera;
    
	//! A BillboardTransform constructor properly initialised from position and scale data. 
	// This constructor reads the texture from a file, which can be expensive if many billboards
	// are to be used. If that is the case, then consider using the alternative constructor.
	/*!
		\param dx Displacement along x-axis
		\param dy Displacement along y-axis
		\param dz Displacement along z-axis
		\param scale_x Scale along x-axis
		\param scale_y Scale along y-axis
		\param scale_z Scale along z-axis
		\param scene Pointer to Scene object
		\param camera Pointer to Camera object
		\param filename String with texture file's name
	*/ 
	Billboard (double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera, const char *filename);

	//! A BillboardTransform constructor properly initialised from position and scale data. 
	//  This constructor receives a pointer to a material. You can use the material already defined in another 
	//  billboard by accessing its 'm' attribute.
	/*!
		\param dx Displacement along x-axis
		\param dy Displacement along y-axis
		\param dz Displacement along z-axis
		\param scale_x Scale along x-axis
		\param scale_y Scale along y-axis
		\param scale_z Scale along z-axis
		\param scene Pointer to Scene object
		\param camera Pointer to Camera object
		\param material Pointer to material (associated to the texture file)
	*/ 
	Billboard (double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera, Material *m);
	
    //! Returns a pointer to the Material associated to this Billboard.
    /*!
     \returns A pointer to the Material associated to this Billboard.
     */
	Material* getMaterial() {return m;}

    //! Sets the material for this Billboard.
    /*!
     \param _m A pointer to the Material associated to this Billboard.
     */
    void setMaterial(Material *_m);

    //! Returns a pointer to the BillboardTransform node associated to this Billboard.
    /*!
     \returns a pointer to the BillboardTransform node associated to this Billboard.
     */
    Transform* getTransform() {return bb_tf1;}

    //! State whether the billboard should always be vertically aligned (true by default)
    /*!
        \param _align Boolean stating whether the billboard should always be vertically aligned
    */
    void setAlignVertical(bool _align){bb_tf1->setAlignVertical(_align);}

    //! Return true if the billboard has been setup to be always vertically aligned (true by default)
    /*!
        \returns true if the billboard has been setup to be always vertically aligned
    */
    bool getAlignVertical(){return bb_tf1->getAlignVertical();}

private:

	void createBillboard(double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera);

};

#endif
