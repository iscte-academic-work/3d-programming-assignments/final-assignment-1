/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "SDL2/SDL.h"
#include "RayTracer.hpp"
#include "Scene.hpp"
#include "Vec.hpp"
#include "Transform.hpp"
#include "RayTracer.hpp"

//! Renders the scene and reacts to keyboard events in a cycle. The acceleration structure is updated at each new rendering cycle depending on the value of the refresh_bhv flag. 
//! This flag should be true if the scene contains animated elements.
/*!
    \param renderer RayTracer object responsible for the rendering of the scene
    \param callback Pointer to user-space function that will be called before each new rendering 
    \param view_look_at_sphere Flag stating whether the user wants to view the look_at_sphere
    \param refresh_bvh Flag stating whether the acceleration structure should be updated at each new rendering cycle.
    \param default_events Flag stating whether default keyboard/mouse events should be on (such as setting anti-alliasing if key 'd' is pressed).
*/
void main_loop(RayTracer &renderer, void(*callback)(RayTracer*, SDL_Event&), bool view_look_at_sphere, bool refresh_bvh, bool default_events);

//! Associates a Light object with a Sphere to create a light bulb
/*!
    \param scene Pointer to the Scene in which the light bulb will be appended
    \param pos 3D location of the light bulb
    \param cl Light's intensity vector
    \param decay light intensity decay exponent
    \param tf Pointer to Transform object responsible for controlling the pose/scale of the light bulb (set to NULL if unwanted)
*/
void LightBulb(Scene* scene, const Vec &pos, const Vec &cl, double decay, Transform *tf);

//! Creates a simple Cornell Box.
/*!
    \param scene Pointer to the Scene in which the light bulb will be appended
    \param tf Pointer to Transform object responsible for controlling the pose/scale of the light bulb (set to NULL if unwanted)
*/
int CornellBox(Scene* scene, Transform *tf);

//! Creates a textured Cornell Box.
/*!
    \param scene Pointer to the Scene in which the light bulb will be appended
    \param tf Pointer to Transform object responsible for controlling the pose/scale of the light bulb (set to NULL if unwanted)
*/
int CornellBoxTextured(Scene* scene, Transform *tf);

void print_matrix(std::vector<int> level_matrix);

//! Draws a given bounding box of type Box.
/*!
    \param scene Pointer to the Scene in which the light bulb will be appended
    \param m Pointer to Material used to paint the Box
    \param box Pointer to the Box object
*/
void drawBox(Scene* scene, Material *m, Box box);
