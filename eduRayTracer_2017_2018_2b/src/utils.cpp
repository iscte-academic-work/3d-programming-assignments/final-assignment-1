/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2017)
 */

#include "utils.hpp"
#include "Triangle.hpp"
#include "Sphere.hpp"
#include "Mesh.hpp"
#include "BVH.h"
#include "Box.hpp"

void main_loop(RayTracer &renderer, 
               void(*callback)(RayTracer*, SDL_Event&), 
               bool view_look_at_sphere, 
               bool refresh_bvh,
               bool default_events){

    Camera *camera = renderer.camera;
    Scene* scene = renderer.scene;

    // A sphere located at the point to which the camera is looking at
    
    Sphere *at_sphere;
    if (view_look_at_sphere){
        at_sphere = new Sphere ((camera->a), 0.2, new Material(Vec(1,0,0), Vec(0.1,0.1,0.1), Vec(1,0.0,0.0), 1e20, 1, 1.3, false, false, false, false, NULL), false);
        scene->surfaces[0] = at_sphere;
    }

    // A set of control variables for the user interface
    bool render = 1, continuous = 1, move = 1;

    SDL_Event event;

    // A loop that will iterate at each new render request

    if (SDL_Init(SDL_INIT_VIDEO) < 0) return ;
    atexit(SDL_Quit);

    SDL_Window *window = SDL_CreateWindow("eduRayTracer rendered image", 0, 0, camera->viewport_width, camera->viewport_height, SDL_WINDOW_RESIZABLE);        
    SDL_Renderer * sdl_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_Texture * texture = SDL_CreateTexture(sdl_renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, camera->nx, camera->ny);

    BVH *bvh;
    // Update the scene according to the transforms current state (these can be changed for animation purposes)
    scene->transform();
    //update the camera pose
    camera->transform();

    // bvh stuff
    scene->setBVH();
    bvh = new BVH(&(scene->objects));
    scene->bvh = *bvh;  
     
    while( true ){
        
        //invoke the callback function
        if (callback != NULL) 
            (*callback)(&renderer, event);

        // Update the location of the shere indicating to where the camera is looking at
        if (view_look_at_sphere)
            at_sphere->setPosLocalCoords(camera->a);
        
       if (refresh_bvh){
            // Update the scene according to the transforms current state (these can be changed for animation purposes)
            scene->transform();
            //update the camera pose
            camera->transform();

            // bvh stuff
            delete bvh;
            scene->setBVH();
            bvh = new BVH(&(scene->objects));
            scene->bvh = *bvh;  
        }
         
        // Render the scene
        renderer.render();
        
        // A control variable
        render = false;

        // All the stuff required to create a SDL window to show the rendering result        
        SDL_UpdateTexture(texture, NULL, renderer.pixels, camera->nx*3);
        SDL_RenderClear(sdl_renderer);
        SDL_RenderCopy(sdl_renderer, texture, NULL, NULL);
        SDL_RenderPresent(sdl_renderer);

        // ---------------
      
        // A loop that will iterate at each new keyboard hit for viewing and rendering setup
        while (!render){

            SDL_WaitEvent( &event );

            if (event.type == SDL_KEYDOWN && (event.key.keysym.sym == SDLK_q || event.type == SDL_QUIT) ){
                std::cout << "Exiting." << std::endl;
                exit(1);
            }

            if (event.type == SDL_KEYDOWN || event.type == SDL_MOUSEBUTTONDOWN){
                render = true;

                std::cout << "eye = (" << camera->e.x << ", " << camera->e.y << ", " << camera->e.z << ")" << std::endl;
                std::cout << "at = (" << camera->a.x << ", " << camera->a.y << ", " << camera->a.z << ")" << std::endl;

            }

            if (default_events){
                if (event.type == SDL_KEYDOWN){
                    switch(event.key.keysym.sym){
                        case SDLK_d:
                            (renderer.samples==1 ? renderer.samples=4 : renderer.samples=1);
                            std::cout << "samples: " << renderer.samples << std::endl; break;
                        case SDLK_s:
                            (renderer.shadow_samples==1 ? renderer.shadow_samples=20 : renderer.shadow_samples=1);
                            std::cout << "shadow samples: " << renderer.shadow_samples << std::endl; break;
                        case SDLK_r: render = true;break;
                        case SDLK_m: move = !move; std::cout << "move: " << move << std::endl; break;;
                        case SDLK_PLUS: camera->nx+=100; camera->ny+=100;
                            std::cout << "resolution: " << camera->nx << std::endl; 
                            SDL_DestroyTexture(texture); 
                            texture = SDL_CreateTexture(sdl_renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, camera->nx, camera->ny);
                            break;
                        case SDLK_MINUS: camera->nx-=100; camera->ny-=100;
                            std::cout << "resolution: " << camera->nx << std::endl; 
                            SDL_DestroyTexture(texture); 
                            texture = SDL_CreateTexture(sdl_renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, camera->nx, camera->ny);
                            break;
                        case SDLK_f: camera->nx=camera->ny=100;
                            std::cout << "full: " << camera->nx << std::endl; 
                            SDL_DestroyTexture(texture); 
                            texture = SDL_CreateTexture(sdl_renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, camera->nx, camera->ny);
                            break;
                        case SDLK_h: (renderer.hops==0 ? renderer.hops=4 : renderer.hops=0);
                            std::cout << "hops: " << renderer.hops << std::endl; break;
                        case SDLK_UP: (move ? camera->move(Vec(0,0.5,0)) : camera->move_at(Vec(0,0.5,0)));
                            std::cout << "up " << std::endl; break;
                        case SDLK_DOWN: (move ? camera->move(Vec(0,-0.5,0)) : camera->move_at(Vec(0,-0.5,0)));
                            std::cout << "down " << std::endl; break;
                        case SDLK_LEFT: (move ? camera->move(Vec(-0.5,0,0)) : camera->move_at(Vec(-0.5,0,0)));
                            std::cout << "left " << std::endl; break;
                        case SDLK_RIGHT: (move ? camera->move(Vec(0.5,0,0)) : camera->move_at(Vec(0.5,0,0)));
                            std::cout << "right " << std::endl;break;
                        case SDLK_a: (move ? camera->move(Vec(0,0,-0.5)) : camera->move_at(Vec(0,0,-0.5)));
                            std::cout << "into " << std::endl; break;
                        case SDLK_z: (move ? camera->move(Vec(0,0,0.5)) : camera->move_at(Vec(0,0,0.5)));
                            std::cout << "out " << std::endl;break;
                        case SDLK_1 ... SDLK_9:
                        {
                            if(scene->lights.size() > event.key.keysym.sym-49) {
                                scene->lights[event.key.keysym.sym-49]->on = !scene->lights[event.key.keysym.sym-49]->on;
                                std::cout << "Light " << event.key.keysym.sym-49 << " " << scene->lights[event.key.keysym.sym-49]->on << std::endl;
                            }
                            break;
                        }
                        case SDLK_g: renderer.save_to_file();break;

                        default:
                            std::cout << "arrows: move along the xy-plane" << std::endl;
                            std::cout << "a,z: move along the z-axis" << std::endl;
                            std::cout << "r: render the current scene" << std::endl;
                            std::cout << "m: swicht between moving camera and moving 'look at' point " << std::endl;
                            std::cout << "h: turn on/off reflections" << std::endl;
                            std::cout << "s: switch to hard shadows and soft shadows" << std::endl;
                            std::cout << "d: turn on/off antialiasing " << std::endl;
                            std::cout << "+: increase render resolution " << std::endl;
                            std::cout << "-: decrease render resolution " << std::endl;
                            std::cout << "f: change to 100x100 resolution " << std::endl;
                            std::cout << "g: save the rendered image to a timestamped .ppm file" << std::endl;
                            std::cout << "[mouse left button]: cast a ray through pixel to remove visible surface" << std::endl;
                            std::cout << "[mouse right button]: cast a ray through pixel to append a sphere at closest intersection point" << std::endl;
                    }

                }

            }
        }
        
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(sdl_renderer);
    SDL_DestroyWindow(window);

}



void LightBulb(Scene* scene, const Vec &pos, const Vec &cl, double decay, Transform *tf){

    Sphere *lightBulb = new Sphere (pos, 0.2, new Material(cl, Vec(0.1,0.1,0.1), Vec(0.03,0.03,0.03), 50, 1, 1.3, false, false, false, false, "../data/earth.ppm"), false);
    
    #ifndef AT_LIGHT
    #ifndef HEADLIGHT
        scene->surfaces.push_back(lightBulb);
    #endif
    #endif
    
    lightBulb->setTransform(tf);
    
    Light *light0 = new Light (lightBulb->vertexes[0]->local_o, lightBulb->m->getCr(), decay, lightBulb);
    
    scene->lights.push_back(light0);
    
    light0->setTransform(tf);
}


int CornellBox(Scene* scene, Transform *tf){

    // a white diffuse material with a dim white ambient component
    Material * m1 = new Material(Vec(1,1,1), Vec(1,1,1), Vec(0.2,0.2,0.2), 1, 1, 1, false, false, false, false, NULL);
    //Material * m2 = new Material(Vec(1,1,1), Vec(0,0,0), Vec(0.5,0.5,0.5), 0, 1, 1.3, false, true, false, false, NULL);

    // left wall
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5)), new Vertex(Vec(-5,-5,5)), new Vertex(Vec(-5,5,-5)), true, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5)), new Vertex(Vec(-5,-5,5)), new Vertex(Vec(-5,5,5)), true, m1));

    // ceilling
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5)), new Vertex(Vec(5,5,-5)), new Vertex(Vec(-5,5,5)), false, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,5)), new Vertex(Vec(5,5,-5)), new Vertex(Vec(5,5,5)), false, m1));

    // floor
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(-5,-5,5)), true, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(5,-5,5)), true, m1));
    
    // front wall
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(-5,5,-5)), false, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(5,5,-5)), false, m1));
    
    // right wall
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(5,-5,-5)), new Vertex(Vec(5,-5,5)), new Vertex(Vec(5,5,-5)), false, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(5,5,-5)), new Vertex(Vec(5,-5,5)), new Vertex(Vec(5,5,5)), false, m1));

    // changing the origin-centre previous walls to their new pose, according to transform t
    for (int i = scene->surfaces.size()-10; i < scene->surfaces.size(); i++)
        scene->surfaces[i]->setTransform(tf);

}


int CornellBoxTextured(Scene* scene, Transform *tf){

    // left wall
    Material *m1= new Material (Vec(1,1,1), Vec(0.5,0.5,0.5), Vec(0.2,0.2,0.2), 50, 1, 1.0, false, false, false, false, "../data/wall.ppm");
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5),Vec(0,0,0)), new Vertex(Vec(-5,-5,5),Vec(1,0,0)), new Vertex(Vec(-5,5,-5),Vec(0,1,0)), true, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5),Vec(0,1,0)), new Vertex(Vec(-5,-5,5),Vec(1,0,0)), new Vertex(Vec(-5,5,5),Vec(1,1,0)), true, m1));

    // ceilling
    Material *m2= new Material (Vec(1,1,1), Vec(0.5,0.5,0.5), Vec(0.2,0.2,0.2), 50, 1, 1.0, false, false, false, true, "../data/ceiling.ppm");
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5),Vec(0,0,0)), new Vertex(Vec(5,5,-5),Vec(1,0,0)), new Vertex(Vec(-5,5,5),Vec(0,1,0)), false, m2));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,5),Vec(0,1,0)), new Vertex(Vec(5,5,-5),Vec(1,0,0)), new Vertex(Vec(5,5,5),Vec(1,1,0)), false, m2));

    // floor
    Material *m3= new Material (Vec(1,1,1), Vec(0.5,0.5,0.5), Vec(0.2,0.2,0.2), 50, 0.1, 1.0, false, true, false, false, "../data/floor.ppm");
    //m3->setTiling(5);
    //Material * m3 = new Material(Vec(0,0,0), Vec(0.,0.,0.), Vec(1,1,1), 0, 1, 1.0, false, false, true, false, NULL);
    //scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(-5,-5,5)), true, m3));
    //scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,5)), new Vertex(Vec(5,-5,-5)), new Vertex(Vec(5,-5,5)), true, m3));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5),Vec(0,0,0)), new Vertex(Vec(5,-5,-5),Vec(1,0,0)), new Vertex(Vec(-5,-5,5),Vec(0,1,0)), true, m3));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,5),Vec(0,1,0)), new Vertex(Vec(5,-5,-5),Vec(1,0,0)), new Vertex(Vec(5,-5,5),Vec(1,1,0)), true, m3));
    
    // front wall
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,-5,-5),Vec(0,0,0)), new Vertex(Vec(5,-5,-5),Vec(1,0,0)), new Vertex(Vec(-5,5,-5),Vec(0,1,0)), false, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(-5,5,-5),Vec(0,1,0)), new Vertex(Vec(5,-5,-5),Vec(1,0,0)), new Vertex(Vec(5,5,-5),Vec(1,1,0)), false, m1));
    
    // right wall
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(5,-5,-5),Vec(0,0,0)), new Vertex(Vec(5,-5,5),Vec(1,0,0)), new Vertex(Vec(5,5,-5),Vec(0,1,0)), false, m1));
    scene->surfaces.push_back(new Triangle (new Vertex(Vec(5,5,-5),Vec(0,1,0)), new Vertex(Vec(5,-5,5),Vec(1,0,0)), new Vertex(Vec(5,5,5),Vec(1,1,0)), false, m1));

    // changing the origin-centre previous walls to their new pose, according to transform t
    for (int i = scene->surfaces.size()-10; i < scene->surfaces.size(); i++)
        scene->surfaces[i]->setTransform(tf);

}

void print_matrix(std::vector<int> level_matrix){
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            printf("%d-%i ", i*j, level_matrix.at(i*j));
        }
        printf("\n");
    }
}

void __createTriangle(Scene* scene, Material *m, Transform *tf, Vec v1, Vec v2, Vec v3, Vec vt1, Vec vt2, Vec vt3){
    Triangle *t = new Triangle(new Vertex(v1, vt1), new Vertex(v2, vt2), new Vertex(v3, vt3), true, m, false);
    t->setTransform(tf);
    scene->addSurface(t);
}

void __createTexturedCube(Scene* scene, Material *m, Transform *tf){

    Vec v1(-1,-1,1);
    Vec v2(-1,1,1);
    Vec v3(1,1,1);
    Vec v4(1,-1,1);
    Vec v5(-1,-1,-1);
    Vec v6(-1,1,-1);
    Vec v7(1,1,-1);
    Vec v8(1,-1,-1);

    Vec vt1(0,1);     Vec vt2(0.33,1);     Vec vt3(0.66,1);    Vec vt4(1,1);
    Vec vt5(0,0.5);   Vec vt6(0.33,0.5);   Vec vt7(0.66,0.5);  Vec vt8(1,0.5);
    Vec vt9(0,0);     Vec vt10(0.33,0);    Vec vt11(0.66,0);   Vec vt12(1,0);

    // Face 1
    __createTriangle(scene, m, tf, v1, v2, v3, vt5, vt1, vt2);
    __createTriangle(scene, m, tf, v3, v4, v1, vt2, vt6, vt5);
    // Face 2
    __createTriangle(scene, m, tf, v2, v6, v7, vt6, vt2, vt3);
    __createTriangle(scene, m, tf, v7, v3, v2, vt3, vt7, vt6);
    // Face 3
    __createTriangle(scene, m, tf, v6, v5, v8, vt7, vt3, vt4);
    __createTriangle(scene, m, tf, v8, v7, v6, vt4, vt8, vt7);
    // Face 4
    __createTriangle(scene, m, tf, v8, v5, v1, vt5, vt6, vt10);
    __createTriangle(scene, m, tf, v1, v4, v8, vt10, vt9, vt5);
    // Face 5
    __createTriangle(scene, m, tf, v5, v6, v2, vt10, vt6, vt7);
    __createTriangle(scene, m, tf, v2, v1, v5, vt7, vt11, vt10);
    // Face 6
    __createTriangle(scene, m, tf, v4, v3, v7, vt11, vt7, vt8);
    __createTriangle(scene, m, tf, v7, v8, v4, vt8, vt12, vt11);

}

void drawBox(Scene* scene, Material *m, Box box){
    Transform *tf = new Transform(0,0.,0., (box.max.x+box.min.x)/2, (box.max.y+box.min.y)/2, (box.max.z+box.min.z)/2, (box.max.x-box.min.x)/2, (box.max.y-box.min.y)/2, (box.max.z-box.min.z)/2, 1);
    __createTexturedCube(scene, m, tf);
}
