/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Transform_
#define _H_Transform_

class Camera;

#include "Mat.hpp"
#include "Camera.hpp"
#include <iostream>
#include <vector>

//! A class to handle rigid transformations
class Transform {

protected:

    //! A Vector of pointers to the set of Transform objects that are hierarchically below this Transform.
    std::vector<Transform*> childs;

    //! Homogeneous transformation matrix of this Transform.
    //! Set and get this matrix with methods setMat and getMat
    Mat local_tf;

    //! Homogeneous transformation matrix resulting from chainning all hierarchically above transformations with the local_tf transformation matrix
    //! This matrix is updated with the transform method, ideally invoked from the Scene object (to go through the hierarchy of transforms)
    Mat global_tf;

    //! Inverted global_tf
    Mat global_tf_inv;

public:

    //! The id of this transform.
	int id;

	//! A Transform constructor which does nothing.
	Transform() {}

	//! A Transform constructor. Applying this Transform without further updates generates undefined behaviour.
	/*!
		\param _id Transform's numerial unique identifier (useful for debugging printouts)
	*/ 
	Transform(int _id) : id(_id) {}

	//! A Transform constructor properly initialised with a given homogeneous transformation matrix.
	/*!
		\para _m Homogeneous transformation matrix Mat
		\param _id Transform's numerial unique identifier (useful for debugging printouts)
	*/ 
	Transform(Mat _m, int _id);

	//! A Transform constructor properly initialised from orientation and position data. 
	//! Orientation and position data is assumed to be read with co-moving axes. The order to transformations is as follows: 
	//! displacement->yaw->pitch->roll->scale 
	/*!
		\param yaw Angle of rotation (in radians) around y-axis
		\param pitch Angle of rotation (in radians) around x-axis
		\param roll Angle of rotation (in radians) around z-axis
		\param dx Displacement along x-axis
		\param dy Displacement along y-axis
		\param dz Displacement along z-axis
		\param scale_x Scale along x-axis
		\param scale_y Scale along y-axis
		\param scale_z Scale along z-axis
		\param _id Transform's numerial unique identifier (useful for debugging printouts)
	*/ 
	Transform(double yaw, double pitch, double roll, double dx, double dy, double dz, 
			  double scale_x, double scale_y, double scale_z, int _id);
	
    
    //! Sets the matrix (Mat) associated to this Transform.
    /*!
     \_mat The matrix (Mat) to be associated to this Transform.
     */
    void setMat(Mat _mat){ local_tf = _mat; global_tf = local_tf; update_global_tf();}
    
    //! Returns the matrix (Mat) associated to this Transform (local_tf).
    /*!
     \returns The matrix (Mat) associated to this Transform.
     */
    Mat getMat(){ return local_tf;}
    
    //! Returns the global matrix (Mat) that is obtained by multiplying all transformations along the hierarchy (invoked with transform() method in Scene).
    /*!
     \returns The matrix (Mat) associated to this Transform (global_tf).
     */
    Mat getGlobalMat(){ return global_tf;}

    //! Returns the inverted global matrix (for normals transformation).
    /*!
     \returns The inverted global matrix associated to this Transform (global_tf_inv).
     */
    Mat getGlobalMatInv(){ return global_tf_inv;}

	//! Adds a given transform as a child of this transform 
	/*!
		\param _tf Pointer to the child transform.
	*/ 
	void addChild(Transform* _tf);

	//! Returns a pointer to the i-th child of this transform (the index i follows the order taken
	//! when adding the child transforms with the addChild method)
	/*!
		\param _i The index of the child to be returned
		\returns Pointer to the i-th child transform
	*/ 
	Transform* getChild(int _i) {return childs[_i];};

	//! Updates the Transform node from orientation and position data. 
	//! Orientation and position data is assumed to be read with co-moving axes. The order to transformations is as follows: 
	//! displacement->yaw->pitch->roll->scale 
	/*!
		\param yaw Angle of rotation (in radians) around y-axis
		\param pitch Angle of rotation (in radians) around x-axis
		\param roll Angle of rotation (in radians) around z-axis
		\param dx Displacement along x-axis
		\param dy Displacement along y-axis
		\param dz Displacement along z-axis
		\param scale_x Scale along x-axis
		\param scale_y Scale along y-axis
		\param scale_z Scale along z-axis
	*/ 
	void update_local_tf(double yaw, double pitch, double roll, double dx, double dy, double dz, 
			  		double scale_x, double scale_y, double scale_z);
			  
	//! Triggers the recursive update of the global_tf transformation matrices of all Transform objects that are hierarchically below this Transform.
	virtual void update_global_tf();

	virtual void update_global_tf(Mat &current);
};

#endif
