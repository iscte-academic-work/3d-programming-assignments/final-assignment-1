/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Box.hpp"
#include <algorithm>

Box::Box (Vec p) : min(p), max(p) { extent = max - min; }

Box::Box (Vec min, Vec max) : min(min), max(max) { extent = max - min; }


bool Box::hit(const Ray& r, double *tnear, double *tfar) {

    double txmin, txmax, tymin, tymax, tzmin, tzmax;
    double a  = 1./r.d.x;

    if (a >= 0){
        txmin = (min.x - r.o.x)*a;
        txmax = (max.x - r.o.x)*a;
    }else{
        txmin = (max.x - r.o.x)*a;
        txmax = (min.x - r.o.x)*a;
    }

    a = 1./r.d.y;
    if (a >= 0){
        tymin = (min.y - r.o.y)*a;
        tymax = (max.y - r.o.y)*a;
    }else{
        tymin = (max.y - r.o.y)*a;
        tymax = (min.y - r.o.y)*a;
    }

    if ((txmin > tymax)||(tymin > txmax))
        return false;
    
    double tmin = (txmin > tymin ? txmin : tymin);
    double tmax = (txmax > tymax ? tymax : txmax);

    a = 1./r.d.z;
    if (a >= 0){
        tzmin = (min.z - r.o.z)/r.d.z;
        tzmax = (max.z - r.o.z)/r.d.z;
    }else{
        tzmin = (max.z - r.o.z)/r.d.z;
        tzmax = (min.z - r.o.z)/r.d.z;
    }
        
    if ((tzmin > tmax)||(tmin > tzmax))
        return false;

    double tmin2 = (tmin > tzmin ? tmin : tzmin);
    double tmax2 = (tmax > tzmax ? tzmax : tmax);

    *tnear = tmin2;
    *tfar = tmax2;

    return true;

}


void Box::expandToInclude(const Box& b) {
  min = Vec(fmin(min.x,b.min.x),fmin(min.y,b.min.y),fmin(min.z,b.min.z));
  max = Vec(fmax(max.x,b.max.x),fmax(max.y,b.max.y),fmax(max.z,b.max.z));
  extent = max - min;
}

void Box::expandToInclude(const Vec& p) {
    min = Vec(fmin(min.x,p.x),fmin(min.y,p.y),fmin(min.z,p.z));
    max = Vec(fmax(max.x,p.x),fmax(max.y,p.y),fmax(max.z,p.z));
    extent = max - min;
}

uint32_t Box::maxDimension() {
  uint32_t result = 0;
  if(extent.y > extent.x) result = 1;
  if(extent.z > extent.y) result = 2;
  return result;
}

double Box::surfaceArea() {
  return 2.f*( extent.x*extent.z + extent.x*extent.y + extent.y*extent.z );
}