/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_BillboardTransform_
#define _H_BillboardTransform_

#include "Mat.hpp"
#include "Camera.hpp"
#include "Transform.hpp"
#include <iostream>
#include <vector>
#include <string>

//! A class to handle Billboard transformations
class BillboardTransform : public Transform {


private:

	Mat RX,RY,RZ,S,T;
	double o_x,o_y,o_z;

    //! true if the billboard is to be aligned with vertical (true by default)
    bool align_vertical;

public:

	//! A unique identififier for this transform
	std::string uid;

	//! A pointer to the camera
	Camera* camera;

	//! A BillboardTransform constructor. Applying this Transform without further updates generates undefined behaviour.
	/*!
		\param _id Transform's numerial unique identifier (useful for debugging printouts)
	*/ 
    BillboardTransform() {align_vertical = true;}

	
	//! A BillboardTransform constructor properly initialised from orientation and position data. 
	//! Orientation and position data is assumed to be read with co-moving axes. The order to transformations is as follows: 
	//! displacement->yaw->pitch->roll->scale 
	/*!
		\param yaw Angle of rotation (in radians) around y-axis
		\param pitch Angle of rotation (in radians) around x-axis
		\param roll Angle of rotation (in radians) around z-axis
		\param dx Displacement along x-axis
		\param dy Displacement along y-axis
		\param dz Displacement along z-axis
		\param scale_x Scale along x-axis
		\param scale_y Scale along y-axis
		\param scale_z Scale along z-axis
		\param _id Transform's numerial unique identifier (useful for debugging printouts)
		\param _camera Pointer to Camera object
	*/ 
	BillboardTransform(double yaw, double pitch, double roll, double dx, double dy, double dz, 
			  double scale_x, double scale_y, double scale_z, Camera* _camera);
	
	//! Triggers the  update of the global_tf transformation matrice. It should be invoked whenever camera changes its pose.
	virtual void update_global_tf();
	virtual void update_global_tf(Mat &current);

    //! State whether the billboard should always be vertically aligned (true by default)
    /*!
        \param _align Boolean stating whether the billboard should always be vertically aligned
    */
    void setAlignVertical(bool _align){align_vertical = _align;}

    //! Return true if the billboard has been setup to be always vertically aligned (true by default)
    /*!
        \returns true if the billboard has been setup to be always vertically aligned
    */
    bool getAlignVertical(){return align_vertical;}

};

#endif
