/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Surface.hpp"
#include "Box.hpp"
#include "Vec.hpp"

void Surface::transform() {

    // surface-level n transformation
    if (tf != NULL){
        n = (tf->getGlobalMatInv() * local_n).normalize();
    }

    // vertexes-level n transformation
	for (int i=0;i<vertexes.size();i++)
		vertexes[i]->transform(tf);

    // compute bounding box of the surface
    bounding_box();
}

