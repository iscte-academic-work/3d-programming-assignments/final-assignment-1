/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Mat.hpp"

Mat::Mat() {
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            data[i][j] = 0;
}

Mat::Mat (Init t){
    if (t == ONES){
        for (int i=0; i<4; i++)
            for (int j=0; j<4;j++)
                data[i][j] = 1;
    }
    if (t == ZEROS){
        for (int i=0; i<4; i++)
            for (int j=0; j<4;j++)
                data[i][j] = 0;
    }
    if (t == EYE){
        for (int i=0; i<4; i++)
            for (int j=0; j<4;j++)
                if (i==j)
                    data[i][j] = 1;
                else
                    data[i][j] = 0;
    }
}
 
Mat Mat::RotX(double _angle){

    double cangle = cos(_angle), sangle = sin(_angle); 
    double RX[4][4] = { 1, 0     , 0       , 0,
                        0, cangle, -sangle , 0, 
                        0, sangle, cangle  , 0,
                        0, 0     , 0       , 1};
    return Mat(RX);

} 

Mat Mat::RotY(double _angle){

    double cangle = cos(_angle), sangle = sin(_angle); 
    double RY[4][4] = {cangle  , 0, sangle, 0,
                        0      , 1, 0     , 0,
                        -sangle, 0, cangle, 0, 
                        0      , 0, 0     , 1};
    return Mat(RY);

} 

Mat Mat::RotZ(double _angle){

    double cangle = cos(_angle), sangle = sin(_angle); 
    double RZ[4][4] = {cangle , -sangle, 0, 0,
                        sangle, cangle , 0, 0, 
                        0     , 0      , 1, 0,
                        0     , 0      , 0, 1};
    return Mat(RZ);

} 

Mat Mat::Scale(double _sx, double _sy, double _sz){

    double S[4][4] = {_sx, 0  , 0  , 0,
                        0, _sy, 0  , 0, 
                        0, 0  , _sz, 0,
                        0, 0  , 0  , 1};
    return Mat(S);

} 

Mat Mat::Translate(double _tx, double _ty, double _tz){

    double T[4][4] = {1, 0, 0, _tx,
                      0, 1, 0, _ty,
                      0, 0, 1, _tz, 
                      0, 0, 0, 1};
    return Mat(T);

} 

Mat::Mat (const Mat &m) {
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            data[i][j] = m.data[i][j];
}

Mat::Mat (const double m[4][4]) {
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            data[i][j] = m[i][j];
}

Mat Mat::LookAt (Vec e, Vec a, Vec up){
    Vec g = a-e;
    Vec w = g*(-1./g.norm());
    Vec u = (up%w)*(1./(up%w).norm());
    Vec v = w%u;
    double rot[4][4] = {u.x,v.x,w.x,e.x,
                        u.y,v.y,w.y,e.y,
                        u.z,v.z,w.z,e.z,
                        0,0,0,1};
    return Mat(rot);
}

Mat Mat::FullPose(double yaw, double pitch, double roll, double dx, double dy, double dz, 
                 double scale_x, double scale_y, double scale_z){

    Mat RX = Mat::RotX(pitch);
    Mat RY = Mat::RotY(yaw);
    Mat RZ = Mat::RotZ(roll);
    Mat S  = Mat::Scale(scale_x, scale_y, scale_z);
    Mat T  = Mat::Translate(dx, dy, dz);

    return T*RY*RX*RZ*S;
}

Mat Mat::transpose() {
    Mat res;
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            res.data[i][j] = data[j][i];
    return res;

}

Mat Mat::operator=(const Mat &m) {
    if (this == &m)
        return *this;
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            data[i][j] = m.data[i][j];
    return *this;
}

Mat Mat::operator+(const Mat &m) {
    Mat sum(m);
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            sum.data[i][j] += data[i][j];
    return sum;
}

Mat Mat::operator*(const Mat &m) {
    Mat prod;
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++){
            prod.data[i][j] = 0;
            for (int k=0; k<4; k++) {
                prod.data[i][j] += data[i][k] * m.data[k][j];
            }
        }
    return prod;
}

Vec Mat::operator*(const Vec &v) {
    Vec prod;
        
    double p1 = data[0][0]*v.x+data[0][1]*v.y+data[0][2]*v.z+data[0][3];
    double p2 = data[1][0]*v.x+data[1][1]*v.y+data[1][2]*v.z+data[1][3];
    double p3 = data[2][0]*v.x+data[2][1]*v.y+data[2][2]*v.z+data[2][3];
    double p4 = data[3][0]*v.x+data[3][1]*v.y+data[3][2]*v.z+data[3][3];
    p1 = p1/p4, p2 = p2/p4, p3 = p3 /p4;
    
    return Vec(p1,p2,p3);
}

void Mat::print(){
    for (int i=0;i<4;i++){
        for (int j=0;j<4;j++)
            std::cout << data[i][j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

Mat operator*(double c, const Mat &m) {
    Mat prod(m);
    for (int i=0; i<4; i++)
        for (int j=0; j<4;j++)
            prod.data[i][j] = c*m.data[i][j];
    return prod;
}

Mat operator*(const Mat &m, double c) {
    return (c*m);
}

Mat Mat::invert() {
    Mat res;
    res.data[0][0] = data[1][1]*data[2][2]-data[1][2]*data[2][1]; res.data[0][1] = data[1][2]*data[2][0]-data[1][0]*data[2][2]; res.data[0][2] = data[1][0]*data[2][1]-data[1][1]*data[2][0]; res.data[0][3] = 0;
    res.data[1][0] = data[0][2]*data[2][1]-data[0][1]*data[2][2]; res.data[1][1] = data[0][0]*data[2][2]-data[0][2]*data[2][0]; res.data[1][2] = data[0][1]*data[2][0]-data[0][0]*data[2][1]; res.data[1][3] = 0;
    res.data[2][0] = data[0][1]*data[1][2]-data[0][2]*data[1][1]; res.data[2][1] = data[0][2]*data[1][0]-data[0][0]*data[1][2]; res.data[2][2] = data[0][0]*data[1][1]-data[0][1]*data[1][0]; res.data[2][3] = 0;
    res.data[3][0] = 0; res.data[3][1] = 0; res.data[3][2] = 0; res.data[3][3] = 1;
    return res;

}

Mat OnlyRot(const Mat &m) {
    Mat res(m);
    for (int i=0; i<3; i++)
        for (int j=0; j<3;j++)
            res.data[i][j] = m.data[i][j];
    res.data[0][3] = res.data[1][3] = res.data[2][3] = 0;
    res.data[3][3] = 1;
    return res;
}

