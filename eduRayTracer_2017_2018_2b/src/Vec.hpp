/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Vec_
#define _H_Vec_

//#include "RayTracer.hpp"

#include <iostream>
#include <math.h>


//! A class to create and manipulate 3D vectors. These can be used to store positions, normals, and RGB triplets.
class Vec {

public:

    //! First coordinate of this vector
    double x;
    //! Second coordinate of this vector
    double y;
    //! Third coordinate of this vector
    double z;

    double& operator[](const unsigned int i) { 
        if (i==0)
            return x;
        else if (i==1)
            return y;
        else 
            return z;
     }

    //! A Vec constructor.
    Vec(double x_=0, double y_=0, double z_=0){ x=x_; y=y_; z=z_; }

    //! Returns the result of adding a given vector to this vector
    Vec operator+(const Vec &b) const { return Vec(x+b.x,y+b.y,z+b.z); }

    //! Returns the result of subtracting a given vector to this vector
    Vec operator-(const Vec &b) const { return Vec(x-b.x,y-b.y,z-b.z); }

    //! Returns the result of performing an element-wise multiplication between this vector and a scalar
    Vec operator*(double b) const { return Vec(x*b,y*b,z*b); }
    
    //! Returns the result of performing an element-wise division between this vector and a given scalar
    Vec operator/(double b) const { return Vec(x/b,y/b,z/b); }

    //! Returns the result of performing an element-wise multiplication between this vector and another given vector
    Vec mult(const Vec &b) const { return Vec(x*b.x,y*b.y,z*b.z); }

    //! Returns the result of performing an element-wise division between this vector and another given vector
    Vec div(const Vec &b) const { return Vec(x/b.x,y/b.y,z/b.z); }

    //! Returns a unit-length scaled version of this vector
    Vec& normalize(){ return *this = *this * (1/sqrt(x*x+y*y+z*z)); }

    //! Returns the norm of this vector
    double norm(){ return sqrt(x*x+y*y+z*z); }
    
    //! Returns the dot-product between this vector and another given vector
    double dot(const Vec &b) const { return x*b.x+y*b.y+z*b.z; }

    //! Returns the cross-product between this vector and another given vector
    Vec operator%(Vec&b){return Vec(y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);}
    
    //! Sends to the std::out information regarding this vector
	void print() {std::cout<<"Vec("<<x<<", " <<y<<", "<<z<<") ";}
};


#endif
