/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Transform.hpp"

Transform::Transform (double yaw, double pitch, double roll, double dx, double dy, double dz, 
						double scale_x, double scale_y, double scale_z, int _id) : id(_id) {

	global_tf = Mat::FullPose(yaw, pitch, roll, dx, dy, dz, scale_x, scale_y, scale_z);

	local_tf = global_tf;

	global_tf_inv = global_tf.invert();

}

Transform::Transform(Mat _m, int _id) : local_tf(_m), global_tf(_m), id (_id) {

	global_tf_inv = global_tf.invert();
}

void Transform::addChild(Transform* _tf) {
	childs.push_back(_tf);
    update_global_tf();
}

void Transform::update_local_tf (double yaw, double pitch, double roll, double dx, double dy, double dz, 
						double scale_x, double scale_y, double scale_z) {

	Transform (yaw, pitch, roll, dx, dy, dz, scale_x, scale_y, scale_z, id);
}

void Transform::update_global_tf() {
	global_tf = local_tf;
	global_tf_inv = global_tf.invert();
	for (int i=0;i<childs.size(); i++) {		
		childs[i]->update_global_tf(global_tf);
	}
}

void Transform::update_global_tf(Mat &current) {
	global_tf = current * local_tf;
	global_tf_inv = global_tf.invert();
	for (int i=0;i<childs.size(); i++) {		
		childs[i]->update_global_tf(global_tf);
	}
}

