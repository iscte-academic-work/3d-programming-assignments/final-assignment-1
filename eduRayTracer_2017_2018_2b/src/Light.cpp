/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */
 
#include "Light.hpp"


Light::Light (Vec _o, Vec _cl, double _decay, Surface *_geode) : local_o (_o), o(_o), cl(_cl),decay(_decay), geode(_geode), tf(NULL) {
        on = true;
}

Light::Light (Vec _o, Vec _cl, double _decay) : o(_o), cl(_cl), decay(_decay) {
    on = true;
    geode = new Sphere (o, 0.6, new Material(Vec(1,1,1), Vec(0,0,0), Vec(0.1,0.1,0.1), 50, 1, 1.3, false, false, false, false, NULL), false);
}

void Light::transform(){
	if (tf != NULL){
        o = tf->getGlobalMat() * local_o;
    }
}
