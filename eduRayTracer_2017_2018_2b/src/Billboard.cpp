/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Billboard.hpp"

Billboard::Billboard (double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera, const char *filename) {
    m = new Material (Vec(1,1,1), Vec(0,0,0), Vec(0,0,0), 1e20, 1, 1.0, false, false, true, false, filename);
    createBillboard(dx, dy, dz, scale_x, scale_y, scale_z, scene, camera);
}


Billboard::Billboard (double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera, Material *_m) {
    m = _m;
    createBillboard(dx, dy, dz, scale_x, scale_y, scale_z, scene, camera);
}


void Billboard::createBillboard(double dx, double dy, double dz, double scale_x, double scale_y, double scale_z, Scene* scene, Camera* camera){
  
    t1 = new Triangle (new Vertex(Vec(-1,-1,0),Vec(0,0,0)), new Vertex(Vec(-1,1,0),Vec(0,1,0)), new Vertex(Vec(1,1,0),Vec(1,1,0)), true, m);
    t2 = new Triangle (new Vertex(Vec(-1,-1,0),Vec(0,0,0)), new Vertex(Vec(1,1,0),Vec(1,1,0)), new Vertex(Vec(1,-1,0),Vec(1,0,0)), true, m);
    scene->surfaces.push_back(t1);
    scene->surfaces.push_back(t2);
    bb_tf1 = new BillboardTransform(0.0, 0.0, 0.0, dx, dy, dz, scale_x, scale_y, scale_z, camera);
    t1->setTransform(bb_tf1);
    t2->setTransform(bb_tf1);
    
    t3 = new Triangle (new Vertex(Vec(-1,-1,0),Vec(0,0,0)), new Vertex(Vec(-1,1,0),Vec(0,1,0)), new Vertex(Vec(1,1,0),Vec(1,1,0)), true, m);
    t4 = new Triangle (new Vertex(Vec(-1,-1,0),Vec(0,0,0)), new Vertex(Vec(1,1,0),Vec(1,1,0)), new Vertex(Vec(1,-1,0),Vec(1,0,0)), true, m);
    scene->surfaces.push_back(t3);
    scene->surfaces.push_back(t4);
    bb_tf2 = new Transform(M_PI_2, 0.0, 0.0, 0, 0, 0, 1, 1, 1, 1);
    t3->setTransform(bb_tf2);
    t4->setTransform(bb_tf2);

    bb_tf1->addChild(bb_tf2);

}

void Billboard::setMaterial(Material *_m){
    m = _m;
    t1->setMaterial(m);
    t2->setMaterial(m);
    t3->setMaterial(m);
    t4->setMaterial(m);
}

