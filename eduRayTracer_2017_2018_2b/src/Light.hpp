/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */


#ifndef _H_LIGHT_
#define _H_LIGHT_

#include "Surface.hpp"
#include "Material.hpp"
#include "Sphere.hpp"
#include <iostream>

//! A class to create and manipulate light sources
class Light {
public:

    //! Light source's original location, as defined in the Constructor
    Vec local_o; 

	//! Light source's transformed location, after applying setTransform()
    Vec o; 

    //! Light's intensity vector
    Vec cl; 

    //! Light intensity decay exponent
    double decay; 

    //! Flag stating whether the light source is on or off
    bool on; 

    //!Pointer to Surface associated to this Light source
    Surface *geode; 

    //! Pointer to Transform node set via setTransform()
    Transform *tf; 

    //! A Light constructor. The light source is omnidirectional, exhibits some decay, 
    //! and it can be associated to a Surface, which will not cast shadows onto de the environment.
    /*!
      \param _o Light's 3D location
      \param _cl Light intensity vector
      \param _decay Light intensity decay exponent
      \param _geode Pointer to a Surface that can be associated to the light source (will not cast shadows onto de environment)
    */
    Light (Vec _o, Vec _cl, double _decay, Surface *_geode);

    //! A Light constructor. The light source is omnidirectional, exhibits some decay, 
    //! and it is automatically associated to a Sphere of radius 0.6.
    /*!
      \param _o Light's 3D location
      \param _cl Light intensity vector
      \param _decay Light intensity decay exponent
    */
    Light (Vec _o, Vec _cl, double _decay);

    //! Associates a Transform node to this light source. The method transform() can then be used to change the Light's position based on _tf.
    /*!
     \param _tf Pointer to the Transform node
     */
    virtual void setTransform(Transform *_tf) {tf = _tf;}

    //! Transforms the light source's location according to the Transform node passed with setTransform()
    void transform();
};


#endif
