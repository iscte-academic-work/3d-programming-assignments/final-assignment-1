/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Mesh.hpp"

void Mesh::transform() {

    // surface-level n transformation for rendering without normal interpolation for smooth shading
    for (int i=0;i<surfaces.size();i++){
        if (tf != NULL)
            surfaces[i]->transform();
    }
    // vertex-level n transformation 
    Surface::transform();
}

void Mesh::normals_at_vertexes(){

    for (int i=0;i<vertexes.size();i++){
        Vec n_tmp(0,0,0);
        for (int j=0;j<verts[i].size(); j++){
            vertexes[i]->n = vertexes[i]->n + verts[i][j]->n;
        }
        vertexes[i]->n = vertexes[i]->n / verts[i].size();
    }
}

Mesh::Mesh (const char* filename, Material *_m, bool _smooth_shading) : Surface(_m) {

    m = _m;

    std::ifstream File(filename);
    
    if (!File.is_open()){
		std::cout << "Error opening file: " << filename << " (check path/filename)" << std::endl;
		exit(1);
	}	
		
    std::string Line;
    std::string Name;
    int i=0;
    bool texture_coordinates = false;

    std::vector<Vec> tex_coords;
    int l = 0;
    
    while(std::getline(File, Line) ){

        l++;
                
        std::istringstream LineStream(Line);
        Name.clear();
        LineStream >> Name;
        
        if(Name == "v"){
            double vertex[3];
            sscanf(Line.c_str(), "%*s %lf %lf %lf", &vertex[0], &vertex[1], &vertex[2]);
            vertexes.push_back(new Vertex(Vec(vertex[0], vertex[1], vertex[2])));
            verts.resize(vertexes.size());
        }

        if(Name == "vt"){
            texture_coordinates = true;
            double vertex[3];
            sscanf(Line.c_str(), "%*s %lf %lf", &vertex[0], &vertex[1]);

            // negative texture coordinates mean wrapping in the other direction, so 1+vt fixes the problem
            if (vertex[0]<0 || vertex[1]<0)
                tex_coords.push_back(Vec(vertex[0], 1.+vertex[1], 0));
            else
                tex_coords.push_back(Vec(vertex[0], vertex[1], 0));
        }
        
        if(Name == "f"){
			
            int face[3];

            if (texture_coordinates){
                int vt1, vt2, vt3;
                sscanf(Line.c_str(), "%*s %d/%d %d/%d %d/%d", &face[0], &vt1, &face[1], &vt2, &face[2], &vt3);
                vertexes[face[0]-1]->tex_coord = tex_coords[vt1-1];
                vertexes[face[1]-1]->tex_coord = tex_coords[vt2-1];
                vertexes[face[2]-1]->tex_coord = tex_coords[vt3-1];
            }else{
                sscanf(Line.c_str(), "%*s %d %d %d", &face[0], &face[1], &face[2]);
            }

            surfaces.push_back(new Triangle (vertexes[face[0]-1],
                                             vertexes[face[1]-1],
                                            vertexes[face[2]-1], false, _m, _smooth_shading));
			
            verts[face[0]-1].push_back(surfaces.back());
            verts[face[1]-1].push_back(surfaces.back());
            verts[face[2]-1].push_back(surfaces.back());
        }
    }
    
    File.close();
	
    normals_at_vertexes();
    
    bounding_box();
    
}

Mesh::Mesh (std::string _id, const char* filename, Material *_m, bool _smooth_shading) : Surface(_m) {

    m = _m;

    std::ifstream File(filename);
    
    if (!File.is_open()){
		std::cout << "Error opening file: " << filename << " (check path/filename)" << std::endl;
		exit(1);
	}	
		
    std::string Line;
    std::string Name;
    int i=0;
    bool texture_coordinates = false;

    std::vector<Vec> tex_coords;
    int l = 0;
    
    while(std::getline(File, Line) ){

        l++;
                
        std::istringstream LineStream(Line);
        Name.clear();
        LineStream >> Name;
        
        if(Name == "v"){
            double vertex[3];
            sscanf(Line.c_str(), "%*s %lf %lf %lf", &vertex[0], &vertex[1], &vertex[2]);
            vertexes.push_back(new Vertex(Vec(vertex[0], vertex[1], vertex[2])));
            verts.resize(vertexes.size());
        }

        if(Name == "vt"){
            texture_coordinates = true;
            double vertex[3];
            sscanf(Line.c_str(), "%*s %lf %lf", &vertex[0], &vertex[1]);

            // negative texture coordinates mean wrapping in the other direction, so 1+vt fixes the problem
            if (vertex[0]<0 || vertex[1]<0)
                tex_coords.push_back(Vec(vertex[0], 1.+vertex[1], 0));
            else
                tex_coords.push_back(Vec(vertex[0], vertex[1], 0));
        }
        
        if(Name == "f"){
			
            int face[3];

            if (texture_coordinates){
                int vt1, vt2, vt3;
                sscanf(Line.c_str(), "%*s %d/%d %d/%d %d/%d", &face[0], &vt1, &face[1], &vt2, &face[2], &vt3);
                vertexes[face[0]-1]->tex_coord = tex_coords[vt1-1];
                vertexes[face[1]-1]->tex_coord = tex_coords[vt2-1];
                vertexes[face[2]-1]->tex_coord = tex_coords[vt3-1];
            }else{
                sscanf(Line.c_str(), "%*s %d %d %d", &face[0], &face[1], &face[2]);
            }

			Triangle* triangle = new Triangle (vertexes[face[0]-1],
                                             vertexes[face[1]-1],
                                            vertexes[face[2]-1], false, _m, _smooth_shading);
			triangle->setID(_id);
            surfaces.push_back(triangle);
			
            verts[face[0]-1].push_back(surfaces.back());
            verts[face[1]-1].push_back(surfaces.back());
            verts[face[2]-1].push_back(surfaces.back());
        }
    }
    
    File.close();
	
    normals_at_vertexes();
    
    bounding_box();
    
}



void Mesh::setMaterial(Material *_m){
    m = _m;
    for (int i=0;i<surfaces.size();i++){
        surfaces[i]->setMaterial(m);
    }
}
    
void Mesh::setTransform(Transform *_tf){
	tf = _tf;
	for (int i=0;i<surfaces.size();i++){
		surfaces[i]->setTransform(tf);
	}
    transform();
}

void Mesh::addSurfaces(std::vector<Surface*> &objects){
    for (int i = 0; i<surfaces.size(); i++)
        objects.push_back(surfaces[i]);    
}

bool Mesh::hit(const Ray &r, hit_rec &h_r, double t_min, double t_max){


    hit_rec h_r_tmp;
    h_r.t = 1e20;

    for (int i=0; i<surfaces.size(); i++){
        if (surfaces[i]->hit(r, h_r_tmp, t_min, t_max) && (h_r_tmp.t < h_r.t)){
            h_r = h_r_tmp;
            h_r.surface = surfaces[i];
        }
    }
    
    if (h_r.t < 1e20){
        return true;
    }else
        return false;
}

Box Mesh::getBBox(){
    return *b_box;
}

Vec Mesh::getCentroid(){
    Vec cent(0,0,0);
    for (int i=0;i<vertexes.size();i++)
        cent = cent + vertexes[i]->o;
    cent = cent * (1./vertexes.size());
    return cent;
}

void Mesh::bounding_box(){

    if (b_box == NULL){
        b_box = new Box(Vec(1e20,1e20,1e20), Vec(-1e20,-1e20,-1e20));
    }

    b_box->min = Vec(1e20,1e20,1e20);
    b_box->max = Vec(-1e20,-1e20,-1e20);

    for (int i=0;i<vertexes.size();i++){
        if (vertexes[i]->o.x < b_box->min.x) b_box->min.x = vertexes[i]->o.x;
        if (vertexes[i]->o.x > b_box->max.x) b_box->max.x = vertexes[i]->o.x;
        if (vertexes[i]->o.y < b_box->min.y) b_box->min.y = vertexes[i]->o.y;
        if (vertexes[i]->o.y > b_box->max.y) b_box->max.y = vertexes[i]->o.y;
        if (vertexes[i]->o.z < b_box->min.z) b_box->min.z = vertexes[i]->o.z;
        if (vertexes[i]->o.z > b_box->max.z) b_box->max.z = vertexes[i]->o.z;
    }

 }

