/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Sphere.hpp"

Sphere::Sphere(){
    vertexes.push_back(new Vertex(Vec(0,0,0)));
    bounding_box();
}

Sphere::Sphere (Vec _o, double _radius, Material *_m, bool _inv_normal) : inv_normal(_inv_normal), Surface(_m), radius(_radius) {
	vertexes.push_back(new Vertex(_o));
    bounding_box();
}

void Sphere::bounding_box(){

    if (b_box == NULL){
            b_box = new Box(Vec(vertexes[0]->o.x-radius, vertexes[0]->o.y-radius, vertexes[0]->o.z-radius),
                            Vec(vertexes[0]->o.x+radius, vertexes[0]->o.y+radius, vertexes[0]->o.z+radius));
    }else{
       b_box->min = Vec(vertexes[0]->o.x-radius, vertexes[0]->o.y-radius, vertexes[0]->o.z-radius);
       b_box->max = Vec(vertexes[0]->o.x+radius, vertexes[0]->o.y+radius, vertexes[0]->o.z+radius);
     }
}


bool Sphere::hit(const Ray &r, hit_rec &h_r, double t_min, double t_max){

    Vec o = vertexes[0]->o;
    
    double A = r.d.dot(r.d);
    double B = (r.d*2.).dot(r.o-o);
    double C = (r.o-o).dot(r.o-o)-pow(radius,2.);
    
    double BB4AC = B*B-4.*A*C;
    double BB4AC_sqr = sqrt(BB4AC);

    if ( BB4AC > 0){
        double t1 = (-B-BB4AC_sqr)/(2.*A);
        double t2 = (-B+BB4AC_sqr)/(2.*A);

        if (t1 > 0)
        	h_r.t = t1;
        else
         	h_r.t = t2;
        
        h_r.point = r.o + r.d*h_r.t;

        if (inv_normal)
        	h_r.normal = (o-h_r.point).normalize();
        else
        	h_r.normal = (h_r.point-o).normalize();

        h_r.surface = dynamic_cast<Surface*>(this);

        // texture mapping

        // putting the hit point in the sphere's frame of reference (which is displaced acording to the sphere's centre: vertexes[o])
        Vec pt = (vertexes[0]->local_o)*(-1) + h_r.point;

        // let us perform a different transformation to get pt if a transform has been associated to the sphere
        if (tf != NULL){
            
            Mat tf_no_trans = tf->getGlobalMat();
            Vec trans = Vec(tf_no_trans.data[0][3], tf_no_trans.data[1][3], tf_no_trans.data[2][3]);
            tf_no_trans.data[0][3] = tf_no_trans.data[1][3] = tf_no_trans.data[2][3] = 0;
            // let us apply the inverse (we use the local_o, as the o is already transformed by tf)
            // so, the original transform is: displace according to sphere' centre (local_o), rotate, and then translate accoring to tf
            // the inverse is: first apply the tf translation (inverted), then the rotation (inverted) and the the translation imposed by the sphere's centre (inverted)
            pt = (vertexes[0]->local_o)*(-1) + tf_no_trans.transpose() * (h_r.point-trans);
        }

        double theta = acos((pt.z) / radius);
        double phi = atan2((pt.y),(pt.x));

        h_r.tex_coord = Vec(phi/(2*M_PI),(M_PI-theta)/M_PI,0);
        
        if (h_r.t<t_min || h_r.t>t_max)
            return false;
        else
            return true;

    }else{
        h_r.t = -1.;
        return false;
    }

    #ifdef IMPLICIT_DOUBLE_SIDE
    // -- to handle implicitly double sided materials 
    // -- be aware that some rendering artifacts are likely to appear in imported meshes
    if (r.d.dot(h_r.normal) > r.d.dot(h_r.normal*(-1)))
        h_r.normal = h_r.normal*(-1);
    #endif

}


Box Sphere::getBBox(){
    return *b_box;
}

Vec Sphere::getCentroid(){
    return vertexes[0]->o;
}

void Sphere::addSurfaces(std::vector<Surface*> &objects){
    objects.push_back(this);    
}

    
