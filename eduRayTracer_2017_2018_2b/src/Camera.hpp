/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Camera_
#define _H_Camera_

class Transform;

#include <vector>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include "Ray.hpp"
#include "Transform.hpp"

//! A class to create and manipulate virtual projective cameras
class Camera {
public:

    //! u coordinate, in the camera's uvw frame of reference, of the viewing window's left cutting plane 
    double l;

    //! u coordinate, in the camera's uvw frame of reference, of the viewing window's right cutting plane 
    double r;

    //! v coordinate, in the camera's uvw frame of reference, of the viewing window's top cutting plane 
    double t;

    //! v coordinate, in the camera's uvw frame of reference, of the viewing window's bottom cutting plane 
    double b;

    //! Camera's focal distance, i.e., the w coordinate of the viewing window in the camera's uvw frame of reference
    double d;

    //! Width, in pixels, of the rendered image
    int nx;

    //! Height, in pixels, of the rendered image
    int ny;

    //! Camera's location in the world frame(eye)
    Vec e;

    //! u-axis of the camera's frame of reference
    Vec u;

    //! v-axis of the camera's frame of reference
    Vec v;

    //! w-axis of the camera's frame of reference
    Vec w;

    //! Camera's "looking-at" point in the world frame
    Vec a;

    //! Viewport's width (rendered image is scaled to fit the viewport)
    int viewport_width;

    //! Viewport's height (rendered image is scaled to fit the viewport)
    int viewport_height;

    //! Pointer to the Transform object associated to this Camera
    Transform *tf;

    //! Flag to state whether the camera orientation is to be changed by the Transform tf
    bool orientation_fixed;

    //! A projective camera constructor. The camera is set at (0,0,0), it is vertically aligned with the y-axis, and it 
    //! it is looking at (0,0,-10).
    /*!
        \param _l u coordinate, in the camera's uvw frame of reference, of the viewing window's left cutting plane 
        \param _r u coordinate, in the camera's uvw frame of reference, of the viewing window's right cutting plane 
        \param _t v coordinate, in the camera's uvw frame of reference, of the viewing window's top cutting plane 
        \param _b v coordinate, in the camera's uvw frame of reference, of the viewing window's bottom cutting plane 
        \param _d Camera's focal distance, i.e., the w coordinate of the viewing window in the camera's uvw frame of reference
        \param _nx Width, in pixels, of the rendered image
        \param _ny Height, in pixels, of the rendered image
        \param _viewport_width Viewport's width (rendered image is scaled to fit the viewport)
        \param _viewport_height Viewport's height (rendered image is scaled to fit the viewport)
    */
    Camera (double _l, double _r, double _t, double _b, double _d, int _nx, int _ny, int _viewport_width, int _viewport_height) ;

    Camera() {}
    
    //! Sets the camera's pose with the eye-gaze-up convention. The goal is to set the pose of the camera so that a given point 'at' is
    //! seen from a given desired location 'eye', while ensuring that an 'up' vector is vertically aligned with the camera.
    //! This call cancels the automatic application of any Transform node that has been associated to this camera via setTransform.
    /*!
        \param eye 3D point where to look 'from'
        \param at 3D point where to look 'at'
        \param up 3D vector vertically aligned with camera (constraint to set the camera's orientation)
    */
    void look_at (Vec eye, Vec at, Vec up);

    //! Sets the camera's pose using a Transform node. 
    /*!
        \param tf Pointer to Transform node to be applied to the camera
    */ 
    void setTransform(Transform* tf);

    //! Updates the camera's pose using a Transform node set with setTransform - don't call it directly.
    void transform();

    //! Sets the camera's pose using a transformation matrix.
    //! This call cancels the automatic application of any Transform node that has been associated to this camera via setTransform.
    /*!
     \param m The transformation matrix
     */
    void transform(Mat m);
    
    //! Applies a motion vector to the camera. 
    /*!
        \param motion Motion 3D vector to be applied to the camera's current pose
    */
    void move(Vec motion);
    
    //! Moves the camera to a given location 
    /*!
        \param motion New 3D location for the camera
    */
    void move_at(Vec motion);
    
     //! Computes the viewing ray that passes through a given pixel (i,j)
   
    Ray view_ray(const double i, const double j);
};


#endif
