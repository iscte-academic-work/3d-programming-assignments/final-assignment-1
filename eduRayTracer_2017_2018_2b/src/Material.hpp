/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Material_
#define _H_Material_

#include "Vec.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <math.h>
#include <stdlib.h>
#include <vector>

//! A class to create and manipulate surface materials
class Material {

public:

    //! Diffuse reflectance vector for this material
    Vec cr;
    
    //! Specular reflectance vector for this material
    Vec cp;
    
    //! Ambient light vector
    Vec ca;
    
    
    //! Phong exponent for this material
    double p;
    
    
    //! Flag stating whether this material is to be emissive and, if so, _cr will be used to define what is emitted (instead, the emission is controlled by the texture mapping if param _filename is not NULL)
    bool emissive;
    
    
    //! Flag stating whether this material is to be bump mapped
    bool bump;
    
    
    //! Number of times the texture is to be replicated.
    int tiling;
    
    
    //! Flag stating whether this material is associated to a texture map
    bool tex_map;
    
    //! Flag stating whether this material is to be reflective, i.e., mirror-like
    bool reflective;
    
    
    //! Flag stating whether this material is to be a dieletric, i.e., if it both reflects and refracts light
    bool refractive;
    
    
    //! Texture image used for texture mapping
    double *tex_image;
    
    //! Proportion of the incident light to be absorbed by this material, when defined as reflective/refractive
    //! Values < 1 results in light decay. Values > 1 results in light amplification.
    double decay;
    
    
    //! Refractive index for this material (only relevant if _refr = true). To save computation, if the value
    //! is exactly 1, the renderer will not cast a secondary reflection ray (only the refraction ray will be cast).
    double rft;
    
    //! Width, in pixels, of the texture image loaded for texture mapping
    int tex_width;
    
    //! Height, in pixels, of the texture image loaded for texture mapping
    int tex_height;
    
    int k;
    
    
    //! A Material constructor.
    /*!
      \param _cr Diffuse reflectance vector for this material
      \param _cp Specular reflectance vector for this material
      \param _ca Ambient light vector 
      \param _p Phong exponent for this material
      \param _decay Proportion of the incident light to be absorbed by this material, when defined as reflective (_refl = true) 
      \param _rft Refraction index for this material (only relevant if _refr = true)
      \param _emissive Flag stating whether this material is to be emissive and, if so, _cr will be used to define what is emitted (instead, the emission is controlled by the texture mapping if param _filename is not NULL)
      \param _refl Flag stating whether this material is to be reflective, i.e., mirror-like
      \param _refr Flag stating whether this material is to be a dieletric, i.e., if it both reflects and refracts light
      \param _bump Flag stating whether this material is to be bump mapped
      \param _filename path to a .ppm file to be used for mapping a texture to this material (use NULL is no texture mapping if to be used)
    */
    Material (Vec _cr, Vec _cp, Vec _ca, double _p, double _decay, double _rft, bool _emissive, bool _refl, bool _refr, bool _bump, const char* _filename);

    Material () : tiling(1), tex_map(false), bump(false), decay(1), reflective(false), refractive(false), emissive (false) {}

    ~Material();
    
    
    //! Returns the diffuse reflectance vector for this material.
    /*!
     \returns The diffuse reflectance vector for this material.
     */
    Vec getCr(){return cr;}

    //! Sets the diffuse reflectance vector for this material.
    /*!
     \param _cr The diffuse reflectance vector for this material.
     */
    void setCr(Vec _cr){cr = _cr;}
    
    //! Returns the specular reflectance vector for this material.
    /*!
     \returns The specular reflectance vector for this material.
     */
    Vec getCp(){return cp;}

    //! Sets the specular reflectance vector for this material.
    /*!
     \param _cp The specular reflectance vector for this material.
     */
    void setCp(Vec _cp){cp = _cp;}
    
    //! Returns the ambient light vector for this material.
    /*!
     \returns The ambient light vector for this material.
     */
    Vec getCa(){return ca;}
    
    //! Sets the ambient light vector for this material.
    /*!
     \param _ca The ambient light vector for this material.
     */
    void setCa(Vec _ca){ca = _ca;}
    
    //! Returns the Phong exponent for this material.
    /*!
     \returns The Phong exponent for this material.
     */
    double getPhong(){return p;}
    
    //! Sets the Phong exponent for this material.
    /*!
     \param _p The Phong exponent for this material.
     */
    void setPhong(double _p){p = _p;}
    
    //! Returns a boolean stating whether this is an emissive material.
    /*!
     \returns A boolean stating whether this is an emissive material.
     */
    bool isEmissive(){return emissive;}

    //! Sets a boolean stating whether this is an emissive material.
    /*!
     \param _emissive A boolean stating whether this is an emissive material.
     */
    void setEmissive(bool _emissive){emissive = _emissive;}

    //! Returns a boolean stating whether this is a bump mapped material.
    /*!
     \returns A boolean stating whether this is a bump mapped material.
     */    
    bool isBumpMapped(){return bump;}

    //! Sets a boolean stating whether this is a bump mapped material.
    /*!
     \param _bump A boolean stating whether this is a bump mapped material.
     */ 
    void setBumpMapped(bool _bump){bump = _bump;}
    
    //! Returns a boolean stating whether this is a textured mapped material.
    /*!
     \returns A boolean stating whether this is a textured mapped material.
     */
    bool isTextureMapped(){return tex_map;}

    //! Sets a boolean stating whether this is a textured mapped material.
    /*!
     \param _tex_map A boolean stating whether this is a textured mapped material.
     */
    void setTextureMapped(bool _tex_map){tex_map = _tex_map;}

    //! Returns a boolean stating whether this is a reflective material.
    /*!
     \returns A boolean stating whether this is a reflective material.
     */
    bool isReflective(){return reflective;}

    //! Sets a boolean stating whether this is a reflective material.
    /*!
     \param _reflective A boolean stating whether this is a reflective material.
     */
    void setReflective(bool _reflective){reflective = _reflective;}

    //! Returns a boolean stating whether this is a refractive material.
    /*!
     \returns A boolean stating whether this is a refractive material.
     */    
    bool isRefractive(){return refractive;}

    //! Sets a boolean stating whether this is a refractive material.
    /*!
     \param _refractive A boolean stating whether this is a refractive material.
     */  
    void setRefractive(bool _refractive){refractive = _refractive;}

    //! Returns the refractive index of this material.
    /*!
     \returns The refractive index of this material.
     */
    double getRefractiveIndex(){return rft;}

    //! Sets the refractive index of this material.
    /*!
     \param _rft The refractive index of this material.
     */
    void setRefractiveIndex(double _rft){rft = _rft;}

    //! Returns the number of times the texture is to be replicated in each direction.
    /*!
     \returns The the number of times the texture is to be replicated in each direction.
     */
    int getTilesNumber(){return tiling;}

    //! Sets the number of times the texture is to be replicated in each direction.
    /*!
     \param _tiling The the number of times the texture is to be replicated in each direction.
     */
    void setTilesNumber(int _tiling){tiling = _tiling;}

    //! Returns the proportion of the incident light to be absorbed by this material, when defined as reflective/refractive
    //! Values < 1 results in light decay. Values > 1 results in light amplification.
    /*!
     \returns The proportion of the incident light to be absorbed by this material.
     */
    double getLightDecay(){return decay;}

    //! Sets the proportion of the incident light to be absorbed by this material, when defined as reflective/refractive
    //! Values < 1 results in light decay. Values > 1 results in light amplification.
    /*!
     \param _decay The proportion of the incident light to be absorbed by this material.
     */
    void setLightDecay(double _decay){decay = _decay;}
 
    //! Returns the colour Vector obtained from the texture, given a 2D texture coordinate.
    /*!
      \param tex_coord A 2D texture coordinate;
      \return The colour of the pixel at tex_coord in the texture image.
    */
    Vec tex_pixel(Vec &tex_coord);
    
    //! Loads a texture from a .ppm file.
    /*!
      \param filename The name of the texture .ppm file (including complete path).
      \returns A buffer containing the loaded texture.
    */    
    double* loadPPM(const char* filename);
};

#endif
