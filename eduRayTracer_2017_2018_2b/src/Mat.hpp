/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Mat_
#define _H_Mat_

#include <iostream>
#include "Vec.hpp"

//! A class to create and manipulate matrices
class Mat {

public:

    //! labels for creating patterned matrices
    enum Init { /*! Identity matrix*/ONES, /*! Matrix filled with 0s*/ ZEROS, /*! Matrix filled with 1s */EYE };

    //! array containing the matrix data
    double data[4][4];

    //! A Mat constructor with all matrix elements set to 0.
    Mat ();

    //! A Mat constructor with matrix elements initialised according to an Init label
    /*!
        \param t Init label stating which kind of initialisation is desired
    */
    Mat (Init t);

    //! A Mat constructor with matrix elements initialised according to a given matrix
    /*!
        \param m Matrix used to initialise this matrix
    */
    Mat (const Mat &m);

    //! A Mat constructor with matrix elements initialised according to a given data array
    /*!
        \param m Data array used to initialise this matrix
    */    
    Mat (const double m[4][4]);

    //! A Mat constructor with matrix elements initialised according to eye-gaze-up convention
    /*!
        \param e Eye vector
        \param a 'at' Vector
        \param up Up vector
    */    
    static Mat LookAt (Vec e, Vec a, Vec up);

    //! Returns a Mat with its matrix elements initialised so as to implement a rotation around the x-axis.
    /*!
        \param _angle Rotation angle in radians.
    */    
    static Mat RotX(double _angle);
    
    //! Returns a Mat with its matrix elements initialised so as to implement a rotation around the y-axis.
    /*!
        \param _angle Rotation angle in radians.
    */    
    static Mat RotY(double _angle);

    //! Returns a Mat with its matrix elements initialised so as to implement a rotation around the z-axis.
    /*!
        \param _angle Rotation angle in radians.
    */    
    static Mat RotZ(double _angle);
    
    //! Returns a Mat with its matrix elements initialised so as to implement a non-uniform scaling.
    /*!
        \param _sx Scale factor along the x-axis.
        \param _sy Scale factor along the y-axis.
        \param _sz Scale factor along the z-axis.
    */    
    static Mat Scale(double _sx, double _sy, double _sz);
    
    //! Returns a Mat with its matrix elements initialised so as to implement a translation.
    /*!
        \param _tx Translation amount along the x-axis.
        \param _ty Translation amount along the y-axis.
        \param _tz Translation amount along the z-axis.
    */        
    static Mat Translate(double _tx, double _ty, double _tz);

    //! Returns a Mat properly initialised from orientation, position data, and scale data. 
    //! Orientation and position data is assumed to be read with co-moving axes. The order to transformations is as follows: 
    //! displacement->yaw->pitch->roll->scale 
    /*!
        \param yaw Angle of rotation (in radians) around y-axis
        \param pitch Angle of rotation (in radians) around x-axis
        \param roll Angle of rotation (in radians) around z-axis
        \param dx Displacement along x-axis
        \param dy Displacement along y-axis
        \param dz Displacement along z-axis
        \param scale_x Scale along x-axis
        \param scale_y Scale along y-axis
        \param scale_z Scale along z-axis
    */ 
    static Mat FullPose(double yaw, double pitch, double roll, double dx, double dy, double dz, 
                 double scale_x, double scale_y, double scale_z);

    //! Copies the contents of a given matrix to this matrix (assignment operator)
    Mat operator=(const Mat &m);

    //! Returns a matrix resulting from adding a given matrix to this matrix
    Mat operator+(const Mat &m);

    //! Returns a matrix resulting from multiplying this matrix to another given matrix (proper matrix dimensions must be handled by the user)
    Mat operator*(const Mat &m);
 
    //! Returns a matrix resulting from multiplying this matrix to a given vector (proper matrix and vector dimensions must be handled by the user)
    Vec operator*(const Vec &v);

    //! Returns the transpose of this matrix
    Mat transpose();

    //! Returns the inverted version of this matrix
    Mat invert();

    //! Sends to the std::out information regarding this matrix
    void print();
    
    
};

//! Returns the matrix resulting of element-wise multiplication between a given matrix and a given scalar
Mat operator*(double c, const Mat &m);

//! Returns the matrix resulting of element-wise multiplication between a given matrix and a given scalar
Mat operator*(const Mat &m, double c);

Mat OnlyRot(const Mat &m);

#endif
