/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Triangle_
#define _H_Triangle_

#include "Surface.hpp"
#include "Vertex.hpp"
#include "Ray.hpp"
#include "Material.hpp"

//! A class to create and manipulate triangles
 
class Triangle : public Surface{
public:

    //! Pointer to the first vertex of the Triangle
    Vertex *va;

    //! Pointer to the second vertex of the Triangle
    Vertex *vb;

    //! Pointer to the third vertex of the Triangle
    Vertex *vc;
    
    //! Flag stating if vertexes are provided in clock-wise order (for normal computation)
    bool cw;
    
    //! A Triangle's constructor for rendering with Phong interpolation.
    /*!
     	\param _va Pointer to the first vertex of the Triangle
     	\param _vb Pointer to the second vertex of the Triangle
     	\param _vc Pointer to the third vertex of the Triangle
     	\param _cw Flag stating if vertexes are provided in clock-wise order (for normal computation)
     	\param _m Pointer to Triangle's Material
     */
    Triangle (Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m);

    //! A Triangle's constructor for rendering with Phong interpolation.
    /*!
        \param _id A string containing the ID of the triangle
        \param _va Pointer to the first vertex of the Triangle
        \param _vb Pointer to the second vertex of the Triangle
        \param _vc Pointer to the third vertex of the Triangle
        \param _cw Flag stating if vertexes are provided in clock-wise order (for normal computation)
        \param _m Pointer to Triangle's Material
     */
    Triangle (std::string _id, Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m) : Triangle (_va, _vb, _vc, _cw, _m){
        id = _id;
    }

    //! A Triangle's constructor for rendering with/without with Phong interpolation.
    /*!
     	\param _va Pointer to the first vertex of the Triangle
     	\param _vb Pointer to the second vertex of the Triangle
     	\param _vc Pointer to the third vertex of the Triangle
     	\param _cw Flag stating whether vertexes are provided in clock-wise order (for normal computation)
     	\param _m Pointer to Triangle's Material
     	\param _smooth_shading Flag stating if Phong interpolation is to be used
     */
    Triangle (Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m, bool _smooth_shading);
 

    //! A Triangle's constructor for rendering with/without with Phong interpolation.
    /*!
        \param _id A string containing the ID of the triangle
        \param _va Pointer to the first vertex of the Triangle
        \param _vb Pointer to the second vertex of the Triangle
        \param _vc Pointer to the third vertex of the Triangle
        \param _cw Flag stating whether vertexes are provided in clock-wise order (for normal computation)
        \param _m Pointer to Triangle's Material
        \param _smooth_shading Flag stating if Phong interpolation is to be used
     */
    Triangle (std::string _id, Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m, bool _smooth_shading) :
                                                                            Triangle (_va, _vb, _vc, _cw, _m, _smooth_shading){
        id = _id;
    }

    //! Computes the bounding box of this triangle
    void bounding_box();

    //! Returns the bounding box of this triangle.
    /*!
     \returns The bounding box of this triangle.
     */
    virtual Box getBBox();
    
    //! Returns the centroid of this triangle.
    /*!
     \returns The centroid of this triangle.
     */
    virtual Vec getCentroid();

    //! Checks if a given Ray intersects this Triangle and, if so, a hit record is filled in.
    /*!
        \param r Ray to be tested
        \param h_r Hit record that stores the information regarding the intersection between r and this Triangle, provided that it ocurred
        \param t_min Controls the closest allowed intersection 
        \param t_max Controls the furthest allowed intersection
        \returns Flag stating if an intersection occurred
    */
    bool hit(const Ray &r, hit_rec &h_r, double t_min, double t_max);
    
    //! Adds this triangle (its pointer, actually) to a given vector of surfaces.
    /*!
     \param objects The vector of surfaces that will receive this triangle.
     */
    virtual void addSurfaces(std::vector<Surface*> &objects);
    
};

#endif
