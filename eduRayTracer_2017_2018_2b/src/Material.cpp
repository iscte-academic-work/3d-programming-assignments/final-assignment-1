/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Material.hpp"


Material::Material (Vec _cr, Vec _cp, Vec _ca, double _p, double _decay, double _rft, bool _emissive, bool _refl, bool _refr, bool _bump, const char* _filename){

    decay = _decay;
    cr = _cr;
    cp = _cp;
    ca = _ca;
    p = _p;
    rft = _rft;
    tiling = 1;
    bump = _bump; 
    emissive = _emissive;
    reflective = _refl;
    refractive = _refr;
    if (_filename != NULL){
        tex_map = true;
        tex_image = loadPPM(_filename);
    }else{
        tex_map = false;
    }
    

}

Material::~Material(){

    delete [] tex_image;

}

Vec Material::tex_pixel(Vec &tex_coord){

    // if tiling > 1, then use only the decimal part of the texture coordinates after being scaled by the number of times
    // the texture is to be replicated, that is, tiling times.
    Vec vt = tex_coord;	
    if (tiling > 1){
        vt.x = fmod(double(tiling)*vt.x,1);
        vt.y = fmod(double(tiling)*vt.y,1);
    }
    
	unsigned long index = floor(tex_height-vt.y*tex_height)*tex_width+vt.x*tex_width;
	
	Vec out(tex_image[3*index],tex_image[3*index+1], tex_image[3*index+2]);
    
	return out;
}

double* Material::loadPPM(const char* filename){
    
    std::ifstream File(filename);
     
   if (!File.is_open()){
		std::cout << "Error opening file: " << filename << " (check path/filename)" << std::endl;
		exit(1);
	}	
		
    std::string Line;
    int i=-4;
    double *tex_image;

    while(std::getline(File, Line) ){
        if(Line == "" || Line[0] == '#') continue;

        i++;
        
        if(i == -2){
            sscanf(Line.c_str(), "%d %d", &tex_width, &tex_height);
            tex_image = new double[3*(tex_width)*(tex_height)];
        }
        if (i>-1){
            int r, g, b;
            sscanf(Line.c_str(), "%d", &r);
            std::getline(File, Line);
            sscanf(Line.c_str(), "%d", &g);
            std::getline(File, Line);
            sscanf(Line.c_str(), "%d", &b);

            tex_image[3*i] = float(r)/255.0;
            tex_image[3*i+1] = float(g)/255.0;
            tex_image[3*i+2] = float(b)/255.0;
        }
    }
    
    File.close();

	return tex_image;
    
}
