/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Vertex_
#define _H_Vertex_

#include "Vec.hpp"
#include "Material.hpp"
#include "Transform.hpp"

#include <iostream>
#include <math.h>


//! A class to create and manipulate 3D vertexes
class Vertex{
public:

	//! Vertex's position in the local frame of reference (computed elsewhere)
    Vec local_o;

    //! Vertex's position in the global frame of reference - updated with transform()
    Vec o; 

	//! Vertex's associated normal in the local frame of reference (computed elsewhere)
    Vec local_n;

    //! Vertex's associated normal in the global frame of reference - updated with transform()
    Vec n;

    //! 2D texture coordinates of the Vertex (don't care about the 3rd coordinate of the Vec)
    Vec tex_coord;

    //! A Vertex constructor without texture coordinates.
    /*!
    	\param _o 3D location of the vertex in the object's frame of reference
    */
    Vertex(Vec _o) : local_o(_o), o(_o) {}

    //! A Vertex constructor with texture coordinates.
    /*!
    	\param _o 3D location of the Vertex in the local frame of reference
    	\param _tex_coord 2D texture coordinates of the Vertex (don't care about the 3rd coordinate of the Vec)
    */
    Vertex(Vec _o, Vec _tex_coord) : local_o(_o), o(_o), tex_coord(_tex_coord) {}
    
    Vertex() {}

    //! Transforms the vertex's location and normal vectors from the local frame of reference to the global 
    //! frame of reference, according to a given Transform object
    /*!
    	\param tf Pointer to a Transform object.
    */
    void transform(Transform *tf);
};


#endif
