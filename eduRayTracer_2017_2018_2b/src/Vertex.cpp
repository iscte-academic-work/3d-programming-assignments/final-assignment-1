/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Vertex.hpp"

void Vertex::transform(Transform *tf){
	if (tf != NULL){
        o = tf->getGlobalMat() * local_o;
        //n = (OnlyRot(tf->global_tf) * local_n).normalize();
    	//n = (tf->global_tf * local_n).normalize();
        n = (tf->getGlobalMatInv() * local_n).normalize();
    }else{
    	o = local_o;
    	n = local_n;
    }
}

