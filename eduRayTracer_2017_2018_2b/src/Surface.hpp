/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_SURFACE_
#define _H_SURFACE_

#include "Material.hpp"
#include "Vec.hpp"
#include "Transform.hpp"
#include "Vertex.hpp"
#include "Ray.hpp"
#include <iostream>
#include "Box.hpp"

class Surface;

//! A structure used to hold information regarding ray-surface intersection events
struct hit_rec {
    //! Pointer to the Surface hit by the Ray
    Surface *surface;
    //! Scalar to generate the hit/intersection point from the ray equation o+td
    double t;
    //! Hit/intersection point
    Vec point;
    //! Triangle barycentric coordinate of the hit/intersection point
    double alpha;
    //! Triangle barycentric coordinate of the hit/intersection point
    double gamma;
    //! Triangle barycentric coordinate of the hit/intersection point
    double beta;
    //! Surface's normal at the hit/intersection point
    Vec normal;
    //! Texture coordinate of the hit/intersection point
    Vec tex_coord;
};

//! A base class for every geometry class (e.g., Sphere, Triangle, Mesh). 
class Surface {

protected:

    //! Pointer to the Transform object associated to this Surface. Set this variable with setTransform
    Transform *tf;

public:

    //! Vector of vertexes associated to this Surface. Derived classes must use this vector if bounding boxes and transforms are to be applied.
    std::vector<Vertex*> vertexes;

    //! A string containing a unique ID for the surface
    std::string id;

    //! Flag stating if a bounding box has been defined already
    bool b_box_def;

    //! Material associated to this surface
    Material* m;

    //! Flag stating if Phong interpolation is to be used
    bool smooth_shading;

    //! Surface's associated normal in the local frame of reference (computed elsewhere)
    Vec local_n;

    //! Surface's associated normal in the global frame of reference - updated with transform()
    Vec n;

    //! Bounding box (a Surface itself - currently a Box) encompassing all vertexes of this surface
    Box *b_box;


    //! A Surface constructor. This is not supposed to be called directly by the user.
    Surface(Material *&_m) : m(_m), b_box(NULL), tf(NULL) {}
    
    Surface() : b_box(NULL), tf(NULL) {}
    
    ~Surface() {}

    //! Sets a unique identifier for this Surface.
    /*!
     \param _id The unique identifier.
     */
    void setID(std::string _id){id = _id;};

    //! Returns a boolean stating whether this surface should be renderered with smooth shading.
    /*!
     \returns A boolean stating whether this surface should be renderered with smooth shading.
     */
    bool isSmoothShaded(){return smooth_shading;};
    
    //! Sets a boolean stating whether this surface should be renderered with smooth shading.
    /*!
     \param _smooth_shading A boolean stating whether this surface should be renderered with smooth shading.
     */
    void setSmoothShading(bool _smooth_shading){smooth_shading = _smooth_shading;};

    //! Returns the position of the i-th vertex in the object frame (without being affected by any Transform).
    /*!
     \param _i The index of the vertex to be queried.
     \returns The position of the i-th vertex in the object frame.
     */
    Vec getVertexPosLocalCoords(int _i) {return vertexes[_i]->local_o;}
    
    //! Returns the position of the i-th vertex in the world frame (after being affected by the Transform hierarchy).
    /*!
     \param _i The index of the vertex to be queried.
     \returns The position of the i-th vertex in the world frame.
     */
    Vec getVertexPosWorldCoords(int _i) {return vertexes[_i]->o;}
    
    //! Sets the position of the i-th vertex in the object frame (without being affected by any Transform).
    /*!
     \param _i The index of the vertex to be updated.
     \param _pos The position of the i-th vertex in the object frame.
     */
    void setVertexPosLocalCoords(int _i, Vec _pos) {vertexes[_i]->local_o = _pos;}
    
    //! Returns a pointer to the Material associated to this Surface.
    /*!
     \returns A pointer to the Material associated to this Surface.
     */
    Material* getMaterial() {return m;}
    
    //! Sets the Material associated to this Surface (via a pointer).
    /*!
     \param _m A pointer to the Material to be associated to this Surface.
     */
    virtual void setMaterial(Material *_m) {m = _m;}
    
    //! Computes an Axis-Aligned Bounding Box that encompasses all vertexes of this Surface.
    virtual void bounding_box() {}
    
    //! Returns the bounding box of this surface.
    /*!
     \returns The bounding box of this surface.
     */
    virtual Box getBBox(){}
    
    //! Returns the centroid of this surface.
    /*!
     \returns The centroid of this surface.
     */
    virtual Vec getCentroid(){}

    //! Triggers the intersection tests over all geometry contained in this Mesh and a given Ray.
    //! Here, this method does nothing. Derived Classes (e.g., Triangle) must implement it.
    /*!
        \param r Ray to be tested
        \param h_r Hit record that stores the information regarding the first intersection between r and the Mesh's geometry
        \param t_min Controls the closest allowed intersection 
        \param t_max Controls the furthest allowed intersection
    */
    virtual bool hit(const Ray &r, hit_rec &h_r, double t_min, double t_max){}

    //! Associates a Transform node to this Surface. The method transform() can then be used to change the Surface's pose/scale based on _tf.
    /*!
     \param _tf Pointer to the Transform node
    */
    virtual void setTransform(Transform *_tf) {tf = _tf; transform();}
    
    //! Returns a pointer to the Transform node associated with this Surface through method setTransform.
    /*!
     \returns Pointer to the Transform node.
     */
    Transform* getTransform(){return tf;};

    //! Triggers the application of the transform provided with setTransform() to all Vertex objects associated to this surface
  	virtual void transform();
    
    //! Adds this surface (its pointer, actually) to a given vector of surfaces.
    /*!
     \param objects The vector of surfaces that will receive this surface.
     */
    virtual void addSurfaces(std::vector<Surface*> &objects){}
};

#endif
