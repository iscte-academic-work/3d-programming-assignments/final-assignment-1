/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Sphere_
#define _H_Sphere_

#include "Surface.hpp"
#include "Vec.hpp"
#include "Box.hpp"
#include "Material.hpp"
#include <vector>


//! A class to create and manipulate spheres
class Sphere : public Surface{
public:

    //! Sphere's radius
    double radius;

    //! Flag stating whether the normal vectors are to be pointing into the sphere's centre
    bool inv_normal;

    //! A simple constructor (don't use it directly).
    Sphere();

    //! A Sphere constructor.
    /*!
     	\param _o Sphere's 3D location
     	\param _radius Sphere's radius
     	\param _inv_normal Flag stating whether the normal vectors are to be pointing into the sphere's centre
     */
    Sphere (Vec _o, double _radius, Material *_m, bool _inv_normal);

    //! A Sphere constructor.
    /*!
        \param _id A string containing the ID of the sphere
        \param _o Sphere's 3D location
        \param _radius Sphere's radius
        \param _inv_normal Flag stating whether the normal vectors are to be pointing into the sphere's centre
     */
    Sphere (std::string _id, Vec _o, double _radius, Material *_m, bool _inv_normal) : Sphere (_o, _radius, _m, _inv_normal){
        id = _id;
        bounding_box();
    }

    //! Returns the radius of the sphere.
    /*!
     \returns The radius of the sphere.
     */
    double getRadius() {return radius;};
    
    //! Returns the position of the sphere in the object frame (without being affected by any Transform).
    /*!
     \returns The position of the sphere in the object frame.
     */
    Vec getPosLocalCoords() {return vertexes[0]->local_o;}

    //! Returns the position of the sphere in the world frame (after being affected by the Transform hierarchy).
    /*!
     \returns The position of the sphere in the world frame.
     */
    Vec getPosWorldCoords() {return vertexes[0]->o;}
    
    //! Sets the radius of the sphere.
    /*!
     \_radius The radius of the sphere.
     */
    void setRadius(double _radius) {radius = _radius;};
    
    //! Sets the position of the sphere in the object frame (without being affected by any Transform).
    /*!
     \param _centre The position of the shere in the object frame.
     */
    void setPosLocalCoords(Vec _centre) {vertexes[0]->local_o = _centre;}

    //! Computes the bounding box of the sphere
    void bounding_box();

    //! Returns the bounding box of this sphere.
    /*!
     \returns The bounding box of this sphere.
     */
    virtual Box getBBox();
    
    //! Returns the centroid of this sphere.
    /*!
     \returns The centroid of this sphere.
     */
    virtual Vec getCentroid();
    
    //! Adds this sphere (its pointer, actually) to a given vector of surfaces.
    /*!
     \param objects The vector of surfaces that will receive this sphere.
     */
    virtual void addSurfaces(std::vector<Surface*> &objects);

     //! Checks if a given Ray intersects this Sphere and, if so, a hit record is filled in.
    /*!
        \param r Ray to be tested
        \param h_r Hit record that stores the information regarding the intersection between r and this Sphere, provided that it ocurred
        \param t_min Controls the closest allowed intersection 
        \param t_max Controls the furthest allowed intersection
        \returns Flag stating if an intersection occurred
    */
    bool hit(const Ray &r, hit_rec &h_r, double t_min, double t_max);

};


#endif
