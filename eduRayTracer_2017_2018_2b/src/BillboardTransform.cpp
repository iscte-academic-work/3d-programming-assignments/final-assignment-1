/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "BillboardTransform.hpp"

BillboardTransform::BillboardTransform (double yaw, double pitch, double roll, double dx, double dy, double dz, 
										double scale_x, double scale_y, double scale_z, Camera* _camera) {
	o_x = dx;
	o_y = dy;
	o_z = dz;

	RX = Mat::RotX(pitch);
	RY = Mat::RotY(yaw);
	RZ = Mat::RotZ(roll);
	S  = Mat::Scale(scale_x, scale_y, scale_z);
	T  = Mat::Translate(dx, dy, dz);
	
	camera = _camera;

    align_vertical = true;

    update_global_tf(T);
}

void BillboardTransform::update_global_tf() {
    update_global_tf(T);
}

void BillboardTransform::update_global_tf(Mat &current) {

    Vec e(current.data[0][3]+o_x,current.data[1][3]+o_y,current.data[2][3]+o_z);
    Vec a;
    if (!align_vertical)
        a = Vec(camera->e.x, camera->e.y, camera->e.z);
    else
        a = Vec(camera->e.x, current.data[1][3]+o_y, camera->e.z);

	Vec u(0,1,0);

    Mat L_A = Mat::LookAt (e,a,u);

    local_tf = L_A*RY*RX*RZ*Mat::RotY(M_PI)*S;

    global_tf = local_tf;
	global_tf_inv = global_tf.invert();

	for (int i=0;i<childs.size(); i++) {		
		childs[i]->update_global_tf(global_tf);
	}
}

