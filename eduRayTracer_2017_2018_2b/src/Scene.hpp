/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Scene_
#define _H_Scene_

#include <vector>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include "Ray.hpp"
#include "Surface.hpp"
#include "Triangle.hpp"
#include "Sphere.hpp"
#include "Mesh.hpp"
#include "Light.hpp"
#include "BVH.h"

//! A class to create and control scenes from Surface and Light objects.
class Scene {

private:
	    double tmp;
   
public:
    
     //! Vector containing pointers to all Surface objects that are to be rendered.
    std::vector<Surface*> surfaces;

    //! An internal structure (do not use it).
    std::vector<Surface*> objects;

    //! The root transform node. All transforms that are not child from any other, must be
    //! made child of the root_tf.
    Transform root_tf;

    //! The bounding volume hierarchy object.
    BVH bvh;

    //! Vector containing pointers to all Light objects to be used while rendering.
    std::vector<Light*> lights;

    //! A straighforward Scene constructor.
    Scene();

    //! Returns a pointer to the vector of surfaces belonging to the Scene.
    /*!
     \returns A pointer to the vector of surfaces belonging to the Scene.
     */
    std::vector<Surface*>* getSurfacesVector(){return &surfaces;}

    //! Returns a pointer to Root transform associated to the Scene. All transforms need to under (directly or by transitivity) the root transform.
    /*!
     \returns A pointer to Root transform.
     */
    Transform* getRootTransform(){return &root_tf;}
    
    //! Returns a pointer to the vector of Lights belonging to the Scene.
    /*!
     \returns A pointer to the vector of lights belonging to the Scene.
     */
    std::vector<Light*>* getLightsVector(){return &lights;}
    
    //! Returns a pointer to the surface with a given ID.
    /*!
        \param _id The ID of the surface to be returned.
        \returns A pointer to the surface.
    */
    Surface* surfaceFromID(std::string _id);

    //! Returns a pointer to the triangle with a given ID.
    /*!
        \param _id The ID of the triangle to be returned.
        \returns A pointer to the triangle.
    */
    Triangle* triangleFromID(std::string _id);

    //! Returns a pointer to the sphere with a given ID.
    /*!
        \param _id The ID of the sphere to be returned.
        \returns A pointer to the sphere.
    */
    Sphere* sphereFromID(std::string _id);

    //! Returns a pointer to the mesh with a given ID.
    /*!
        \param _id The ID of the mesh to be returned.
        \returns A pointer to the mesh.
    */
    Mesh* meshFromID(std::string _id);

    //! Adds a surface to the scene and associates to it an ID (unique identifier).
    /*!
        \param _surface Pointer to the surface to be appended.
        \param _id The ID to be associated to the surface.
    */
    void addSurface(Surface* _surface, std::string _id) {surfaces.push_back(_surface);_surface->setID(_id);};   

    //! Adds a surface to the scene 
    /*!
        \param _surface Pointer to the surface to be appended.
    */
    void addSurface(Surface* _surface) {surfaces.push_back(_surface);};

    //! Removes a surface from the scene, given its position in the vector of surfaces.
    /*!
     \param _index The position of the surface to be removed in the vector of surfaces.
     */
    void removeSurface(int _index) {surfaces.erase(surfaces.begin()+_index);};

    //! Adds a light source to the scene
    /*!
        \param _surface Pointer to the light source to be appended.
    */
    void addLight(Light* _light) {lights.push_back(_light);};

    //! Triggers the intersection tests over all geometry contained in this scene and a given Ray. 
    //! Each Surface is responsible for its own intersection test.
    /*!
   		\param r Ray to be tested
   		\param h_r hit record that stores the information regarding the first intersection between r and the scene's geometry
   		\param t_min controls the closest allowed intersection 
   		\param t_max controls the furthest allowed intersection
   		\param light_geode Pointer to a Surface object associated to the Light source and, so, that shall not be included in the intersection tests.
    */
    bool hit(const Ray &r, hit_rec &h_r, double t_min, double t_max, Surface *light_geode);
    
    //! Returns a reflection Ray, given an incinding Ray and corresponding surface-hit record
    /*!
    	\param ray incinding Ray
    	\param h_r hit record 
    	\returns reflection Ray
    */
    Ray reflection(Ray &ray, hit_rec h_r);
    
    //! Returns a refraction Ray, given an incinding Ray and corresponding surface-hit record
    /*!
    	\param ray incinding Ray
    	\param h_r hit record 
    	\returns refraction Ray
    */
    Ray refraction(Ray &ray, hit_rec h_r, double &R);
    
    //! Triggers the application of all transforms defined for all Surface and Light objects with setTransform()
    void transform();

    void setBVH();

};


#endif
