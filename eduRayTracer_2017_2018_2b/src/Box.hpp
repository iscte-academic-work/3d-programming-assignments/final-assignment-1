/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_BOX_
#define _H_BOX_

#include "Vertex.hpp"
#include "Ray.hpp"

class Box{
public:

    Vec min, max, extent;

    Box () {}

    Box (Vec p);

    Box (Vec min, Vec max);
        
    ~Box(){}

    bool hit(const Ray& r, double *tnear, double *tfar);

    void expandToInclude(const Box& b);
  
    void expandToInclude(const Vec& p);
  
    uint32_t maxDimension();
    
    double surfaceArea();

};

#endif
