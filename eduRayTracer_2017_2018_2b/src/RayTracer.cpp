/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "RayTracer.hpp"
#include "time.h"
#include <iomanip>
#include <iostream>

RayTracer::RayTracer(Camera *_camera, Scene *_scene) : camera(_camera), scene(_scene) {
    image = new Vec[1000 * 1000];
    pixels = new char[1000 * 1000 * 3];
    samples = 1;
    shadow_samples = 0;
    hops = 0;
    tmp = 0;
}

Vec RayTracer::get_shadding(hit_rec &h_r, bool bump){
    
    Vec hit_point = h_r.point;
    Vec hit_normal = h_r.normal;
    Surface* surface = h_r.surface;

    Vec eye_norm = (camera->e - hit_point).normalize();
    
    Vec normal_perturbation;
    Vec normal_norm = hit_normal;

    //PROCEDURAL_1
    // ...

    //PROCEDURAL_2
    // ...

    Vec shade(0,0,0);
    
    if (surface->m->emissive){
        if (surface->m->tex_map)
            return surface->m->tex_pixel(h_r.tex_coord);
        else
            return surface->m->cr;
    }
    
    for (int i=0; i<scene->lights.size();i++){
        
        if (!scene->lights[i]->on)
            continue;

        //AT_LIGHT
        // ...

        Vec light_norm = (scene->lights[i]->o - hit_point).normalize();
         
        Vec ca = surface->m->ca;
        Vec cr = surface->m->cr;
        Vec cp = surface->m->cp;
        double p = surface->m->p;
        Vec cl = scene->lights[i]->cl;

        //HEADLIGHT
        //...

        if (surface->m->tex_map){
            cr = surface->m->tex_pixel(h_r.tex_coord);
        }
        
        //PROCEDURAL_3
        // ...

        if (shadow_samples > 0){
            Vec attenuation(0,0,0);
            if (shadow_samples == 1){
                attenuation = shadow_rays(scene, h_r, scene->lights[i], 0);
            }else{
                for (int j=0; j<shadow_samples; j++){
                    int ii;
                    double rand_fac = 0.3;
                    //if (j==0) rand_fac = 0;
                    Vec rnd_vector((rand() % 200 - 100)/100., (rand() % 200 - 100)/100., (rand() % 200 - 100)/100.);
                    attenuation = attenuation + shadow_rays(scene, h_r, scene->lights[i], rnd_vector*rand_fac);
                }
                attenuation = attenuation / double(shadow_samples);
            }
            cl = cl.mult(attenuation);
        }

        Vec diffuse = cr.mult(ca+cl*fmax(normal_norm.dot(light_norm),0));
        
        Vec r=light_norm*-1 + normal_norm*2*light_norm.dot(normal_norm);

        Vec specular = cl.mult(cp*pow(fmax(eye_norm.dot(r.normalize()),0),p));
        
        if (light_norm.dot(normal_norm) < 0)
            specular = Vec(0,0,0);
        
        if (surface == scene->lights[i]->geode)
            diffuse = scene->lights[i]->cl + scene->lights[i]->geode->m->ca;
        
        shade = shade + (diffuse + specular)*(1./(pow((scene->lights[i]->o - hit_point).norm(),scene->lights[i]->decay)));
    }
    
    
    return shade;
    
}

Vec RayTracer::shadow_rays(Scene *scene, hit_rec h_r, Light* light, Vec rnd){

    int hops_shadow = hops;
    Vec acc = Vec(1,1,1);

    while (hops_shadow-- > -1){

        Ray ray = Ray(h_r.point, ((light->o+rnd) - h_r.point));

        hit_rec h_r_tmp;
        bool hit = scene->hit(ray, h_r, 0, 1, light->geode);

        if (!hit){ 
            return acc;
        }else if (h_r.surface == light->geode){ 
            return acc;
        }else if (!h_r.surface->m->refractive) 
            return Vec(0,0,0);

        double R; 
        if (h_r.surface->m->rft == 1.0){
            R = 0.0;
        }else{
            scene->refraction(ray, h_r, R);
        }

        Vec cr = h_r.surface->m->cr;
        if (h_r.surface->m->tex_map){
            cr = h_r.surface->m->tex_pixel(h_r.tex_coord);
        }
        acc = acc.mult(cr*(1.-R));
    }      

    return Vec(0,0,0);
}

Vec RayTracer::ray_shading(Scene *scene, Ray &ray, int hops){
    
    Vec shade(0,0,0);
    hit_rec h_r;
    
    if (scene->hit(ray, h_r,  0.0, 1e20, NULL)){
        
        //RANGE_IMAGE
        //...

        shade = get_shadding(h_r, 1);
        
        if (h_r.surface->m->reflective==0 && h_r.surface->m->refractive==0)
            return shade;
        else if (hops > 0){
            if (h_r.surface->m->reflective){
                Ray ray_reflect = scene->reflection(ray, h_r);
                shade = shade + ray_shading(scene, ray_reflect, hops-1)*h_r.surface->m->decay;
            }else{
                if (h_r.surface->m->refractive){

                   if (h_r.surface->m->rft != 1.0){
                        //std::cout << "Reflection" << std::endl;
                        Ray ray_reflect = scene->reflection(ray, h_r);
                        shade =  ray_shading(scene, ray_reflect, hops-1);
                   }

                    double R;
                    Ray ray_transm = scene->refraction(ray, h_r, R);
                    Vec cr = h_r.surface->m->cr;
                    if (h_r.surface->m->tex_map)
                        cr = h_r.surface->m->tex_pixel(h_r.tex_coord);
                    shade =  (shade*R + (ray_shading(scene, ray_transm, hops-1)*(1.-R)).mult(cr))*h_r.surface->m->decay;                    
                }
            }
        }
    }

    return shade;
}

int RayTracer::get_visible_surface(int i, int j, hit_rec &h_r){
    
    int index = -1;
    Ray ray = camera->view_ray(i, j);
        
    hit_rec h_r_tmp;
    h_r.t = 1e20;
    for (int i=0; i<scene->surfaces.size(); i++){            
        if (scene->surfaces[i]->hit(ray, h_r_tmp, 0.1, 1e20) && (h_r_tmp.t < h_r.t)){
            h_r = h_r_tmp;
            h_r.surface = scene->surfaces[i];
            index = i;
        }
    }
        
    if (h_r.t < 1e20){
        return index;
    }else
        return -1;
}

void RayTracer::get_pixel_from_3D(Vec point, Vec &pixel){

    // to camera coordinates
    double rot[4][4] = {camera->u.x,camera->u.y,camera->u.z,0,
                        camera->v.x,camera->v.y,camera->v.z,0,
                        camera->w.x,camera->w.y,camera->w.z,0,
                        0,0,0,1};
    Mat view = (Mat(rot)*Mat::Translate(-camera->e.x, -camera->e.y, -camera->e.z));
    Vec point2 = view*point;

    // applying perspective (x = fX/Z, y = fY/Z) - homogenisation
    pixel.x = camera->d*point2.x/point2.z; 
    pixel.y = camera->d*point2.y/point2.z; 
    pixel.z = point2.z;

    // to screen coordinates
    pixel.x = camera->nx - ((camera->nx/2.0)*pixel.x + (camera->nx-1.0)/2.0);
    pixel.y = camera->ny - ((camera->ny/2.0)*pixel.y + (camera->ny-1.0)/2.0);

}

void RayTracer::render(){
    tmp+=1;

    double rnd1, rnd2;
    for (int i=0; i<camera->nx*camera->ny; i++)
        image[i].x = image[i].y = image[i].z = 0;
    

    int count = 0;
    
    std::cout << std::endl << "Rendering: ";

    #pragma omp parallel for

    for (int j=0; j < camera->ny; j++){
        
        if (count % 10 == 0)
            std::cout << std::setprecision(1) << std::fixed << (float(count)/(camera->nx*camera->ny))*100. << "% " << std::endl;
        
        //#pragma omp parallel for
        
        for (unsigned short i=0; i < camera->nx; i++){
        
            count++;
            
            Vec shade(0,0,0);
            
            for (unsigned short ni=0; ni < samples; ni++){
                
                for (unsigned short nj=0; nj < samples; nj++){
                    
                    if (samples == 1){
                        rnd1 = rnd2 = 0.5;
                    }else{
                        #ifdef REGULAR_SAMPLING
                            rnd1 = 0.5;
                            rnd2 = 0.5;
                        #else
                            rnd1 = (rand() % 100)/100.;
                            rnd2 = (rand() % 100)/100.;
                        #endif
                    }

                    Ray ray = camera->view_ray(i + (ni+rnd1)/samples, j + (nj+rnd2)/samples);

                    shade = shade + ray_shading(scene, ray, hops);
                    
                    
                }
            }
            int index = (camera->ny-j-1)*camera->nx+i;
            image[index] = shade/float(samples*samples);
        }

    }
    
    for (int i=0; i<camera->nx*camera->ny; i++){

        //INTENSITY_IMAGE 1
        //...
        
        pixels[i*3] = (char)toInt(image[i].x);
        pixels[i*3+1] = (char)toInt(image[i].y);
        pixels[i*3+2] = (char)toInt(image[i].z);

    }
    
    std::cout << std::endl;
    
}

void RayTracer::save_to_file() {

    time_t rawtime;
    struct tm * timeinfo;
    char filename [80];
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    strftime (filename,80,"../renders/render-%F_%T.ppm",timeinfo);
    puts (filename);

    FILE *f = fopen(filename, "w");
    
    fprintf(f, "P3\n%d %d\n%d\n", camera->nx, camera->ny, 255);
    
    for (int i=0; i<camera->nx*camera->ny; i++){

        //INTENSITY_IMAGE 2
        //...

        fprintf(f,"%d %d %d ", toInt(image[i].x), toInt(image[i].y), toInt(image[i].z));

        pixels[i*3] = (char)toInt(image[i].x);
        pixels[i*3+1] = (char)toInt(image[i].y);
        pixels[i*3+2] = (char)toInt(image[i].z);

    }
    
    fclose(f);
}
