/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2017)
 */

#include "Scene.hpp"

Scene::Scene() : tmp(0) {

    root_tf = Transform(0,0,0, 0,0,0, 1,1,1, 1);

    // A slot to create a sphere which will be located where the camera is looking at
    //surfaces.push_back(new Sphere());
    surfaces.push_back(new Sphere(Vec(1e10,1e10,1e10), 1, NULL, false));
}

Surface* Scene::surfaceFromID(std::string _id){
    for (int i=0; i<surfaces.size(); i++){
        if (surfaces[i]->id == _id)
            return surfaces[i];
    }
    std::cout << std::endl << "Error: surface with id " << _id << " does not exist!" << std::endl; 
    exit(1);
}

Triangle* Scene::triangleFromID(std::string _id){
    return dynamic_cast<Triangle*>(surfaceFromID(_id));
}

Sphere* Scene::sphereFromID(std::string _id){
    return dynamic_cast<Sphere*>(surfaceFromID(_id));
}

Mesh* Scene::meshFromID(std::string _id){
    return dynamic_cast<Mesh*>(surfaceFromID(_id));
}


void Scene::setBVH(){

    objects.clear();
    for (int i=0;i<surfaces.size();i++){
        //if (dynamic_cast<Sphere*> (surfaces[i]) != NULL)
            surfaces[i]->addSurfaces(objects);
    }
}

bool Scene::hit(const Ray &r, hit_rec &h_r, double t_min, double t_max, Surface *light_geode){
    
    bool hit = bvh.getIntersection(Ray(r.o+r.d*0.00000005,r.d), &h_r, false);

    if (h_r.t < t_min || h_r.t > t_max)
        return false;

    //if (h_r.surface == light_geode)
    //    return false;
    //else
    return hit;
}

Ray Scene::reflection(Ray &ray, hit_rec h_r){

    Vec r;
    Vec incident = ray.d.normalize();
    if (h_r.surface->m->bump){
        Vec random_vector;
        random_vector = Vec(sin(10.*h_r.point.x+tmp),sin(10.*h_r.point.y+tmp*2),0)*0.01;
        Vec normal_norm = (random_vector + h_r.normal).normalize();
        r=incident - normal_norm*2.*incident.dot(normal_norm);
    }else
        r=incident - h_r.normal*2.*incident.dot(h_r.normal);
    
    return Ray(h_r.point, r);
}
    
Ray Scene::refraction(Ray &ray, hit_rec h_r, double &R){
        
    double rf = 1;
    double rft = h_r.surface->m->rft;//1.3;

    Vec n;
    if (h_r.surface->m->bump){
        Vec random_vector;
        random_vector = Vec(sin(10*h_r.point.x+tmp),sin(10*h_r.point.y+tmp*2),0)*0.03;
        Vec normal_norm = (random_vector + h_r.normal).normalize();
        n = normal_norm;
    }else
        n = h_r.normal;
     
    Vec d = ray.d.normalize();

    double c;
    Vec transmission;
    double R0;
    if (d.dot(n)<0){
        transmission = ((d-n*(d.dot(n)))*rf)/rft - n * sqrt(1.-(((rf*rf)*(1.-pow(d.dot(n),2)))/(rft*rft)));
        c = (d*-1).dot(n);
        R0 = ((rft - 1)*(rft - 1))/((rft + 1)*(rft + 1));
        R = R0 + (1 - R0)*pow((1.-c),5);
    }else{
        n = n*-1;
        transmission = ((d-n*(d.dot(n)))*rft)/rf - n * sqrt(1.-(((rft*rft)*(1.-pow(d.dot(n),2)))/(rf*rf)));
        c = transmission.dot(n*(-1));
        if (1.-(((rft*rft)*(1.-pow(d.dot(n),2)))/(rf*rf)) < 0){
            //std::cout << "TIR!!!!!!!!!!!!!!!!!!!!" << std::endl;
            R = 1;
        }else{
            R0 = ((rf - 1)*(rf - 1))/((rf + 1)*(rf + 1));
            R = R0 + (1 - R0)*pow((1.-c),5);
        }

    }
     
    return Ray(h_r.point, transmission);
}
    
void Scene::transform(){

    tmp+=10;

    root_tf.update_global_tf();
    
    for (int i=0;i<surfaces.size();i++)
        surfaces[i]->transform();

    for (int i=0;i<lights.size();i++)
        lights[i]->transform();
}

