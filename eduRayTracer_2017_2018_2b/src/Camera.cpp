/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Camera.hpp"

Camera::Camera (double _l, double _r, double _t, double _b, double _d, int _nx, int _ny, int _viewport_width, int _viewport_height) : 
                l(_l), r(_r), t(_t), b(_b), d(_d), nx(_nx), ny(_ny), viewport_width(_viewport_width), viewport_height(_viewport_height) {
    e = Vec(0, 0, 0);
    u = Vec(1,0,0);
    v = Vec(0,1,0);
    w = Vec(0,0,1);
    a = Vec(0,0,-10);
}


void Camera::look_at (Vec eye, Vec at, Vec up){

    tf = NULL;

    // saving current camera state
    Vec old[5] = {a,e,u,v,w};

    // computing new camera state
    a = at;
    e = eye;
    Vec g = a-e;
    w = g*(-1./g.norm());
    u = (up%w)*(1./(up%w).norm());
    v = w%u;

    // restoring old camera state in case of infeasible pose
    if (isnan(u.x)){
        std::cout << std::endl << "Error: infeasible pose. Restoring old one." << std::endl << std::endl;
        a=old[0]; e=old[1]; u=old[2]; v=old[3]; w=old[4];
    }

}

void Camera::setTransform(Transform* _tf){
    tf = _tf;
    orientation_fixed = false;
    transform();
}

void Camera::transform(){

    if (tf == NULL)
        return;

    // saving current camera state
    Vec old[5] = {a,e,u,v,w};

    // computing new camera state
    Mat tf_no_trans = tf->getGlobalMat();
    // remove translation component to affect uvw vectors (this could be implemented with homogeneous 'a' and 'e' vectors with w=0)
    tf_no_trans.data[0][3] = tf_no_trans.data[1][3] = tf_no_trans.data[2][3] = 0;
    a = tf->getGlobalMat() * Vec(0,0,-1);
    e = tf->getGlobalMat() * Vec(0,0,0);
    if (orientation_fixed){
        look_at(e,a,Vec(0,1,0));
    }else{
        u = tf_no_trans * Vec(1,0,0);
        v = tf_no_trans * Vec(0,1,0);
        w = tf_no_trans * Vec(0,0,1);
    }

    // restoring old camera state in case of infeasible pose
    if (isnan(u.x)){
        std::cout << std::endl << "Error: infeasible pose. Restoring old one." << std::endl << std::endl;
        a=old[0]; e=old[1]; u=old[2]; v=old[3]; w=old[4];
    }
}

void Camera::transform(Mat m){

    tf = NULL;

    e = Vec(m.data[0][3], m.data[1][3], m.data[2][3]);
    u = Vec(m.data[0][0], m.data[1][0], m.data[2][0]);
    v = Vec(m.data[0][1], m.data[1][1], m.data[2][1]);
    w = Vec(m.data[0][2], m.data[1][2], m.data[2][2]);
}

void Camera::move(Vec motion){
    e = e + motion;
    look_at (e, a, Vec(0,1,0));
}
    
void Camera::move_at(Vec motion){
    a = a + motion;
    look_at(e, a, Vec(0,1,0));
}
    
Ray Camera::view_ray(const double i, const double j){
        
    Vec p = Vec(l+(r-l)*(i+0.5)/double(nx), b+(t-b)*(j+0.5)/double(ny), -d);
        
    Vec s = (e + u*p.x + v*p.y + w*p.z);
        
    return Ray(e,s-e);
        
}

