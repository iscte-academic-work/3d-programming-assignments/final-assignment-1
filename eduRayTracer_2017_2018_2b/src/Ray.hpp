/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_RAY_
#define _H_RAY_

#include "Vec.hpp"

//! A class to create and manipulate 3D rays
class Ray {

public:
	//! Ray's origin vector
    Vec o;
    //! Rays's direction vector
    Vec d;

    Vec inv_d;


    //! A Ray Constructor. A ray is defined as o + td, where o is the origin of the ray and d is the direction vector.
    /*!
    	\param _o Ray's origin Vector
    	\param _d Ray's direction Vector
    */
    Ray(Vec _o, Vec _d) : o(_o), d(_d), inv_d(Vec(1,1,1).div(d)) {}
    
    //! Returns the point along the ray for a given t
    /*!
    	\param t Parameter to scale the direction vector from the origin
    */
    Vec tip_point(double t) { return (o + d*t); }
};

#endif
