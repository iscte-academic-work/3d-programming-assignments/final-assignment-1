/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#ifndef _H_Mesh_
#define _H_Mesh_

#include "Surface.hpp"
#include "Ray.hpp"
#include "Triangle.hpp"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>

//! A class to create and manipulate triangular meshes
class Mesh : public Surface{

public:

	//! Vector of pointers to the Surface objects that compose this Mesh
    std::vector<Surface*> surfaces;

	std::vector<std::vector<Surface*>> verts;
	
	//! Triggers the application of all transforms defined for all Surface and Light objects with setTransform()
	void transform();

	//! Triggers the computation of the normal vectors for all Surface objects stored in the Vector surfaces
    void normals_at_vertexes();
       
    //! A constructor to build a Mesh from an .obj file.
    /*!
    	\param filename Path to the .obj file
		\param m Pointer to object Material to be applied to the Mesh
		\param _smooth_shading Flag stating if the Mesh is to be rendered with Phong interpolation
    */
    Mesh (const char* filename, Material *_m, bool _smooth_shading);
    
    
    //! A constructor to build a Mesh from an .obj file.
    /*!
        \param _id A string containing the ID of the mesh
        \param filename Path to the .obj file
        \param m Pointer to object Material to be applied to the Mesh
        \param _smooth_shading Flag stating if the Mesh is to be rendered with Phong interpolation
    */
    Mesh (std::string _id, const char* filename, Material *m, bool _smooth_shading);

    //! Associates a Transform node to this Mesh. The method transform() can then be used to change the Mesh's pose/scale based on _tf.
    /*!
     \param _tf pointer to the Transform node
    */
    void setTransform(Transform *_tf);

    //! Sets the material for this Mesh.
    /*!
     \param _m A pointer to the Material associated to this Mesh.
     */
    virtual void setMaterial(Material *_m);

    //! Adds all triangles (their pointers, actually) belonging to this Mesh to a given vector of surfaces.
    /*!
     \param objects The vector of surfaces that will receive all triangles contained in this Mesh.
     */
    virtual void addSurfaces(std::vector<Surface*> &objects);

    //! Triggers the intersection tests over all geometry contained in this Mesh, given a Ray.
    //! Each Surface is responsible for its own intersection test.
    /*!
   		\param r Ray to be tested
   		\param h_r hit record that stores the information regarding the first intersection between r and the mesh's geometry
   		\param t_min controls the closest allowed intersection
   		\param t_max controls the furthest allowed intersection
     */
    bool hit(const Ray &r, hit_rec &h_r, double t_min, double t_max);

    //! Computes the bounding box of this mesh
    void bounding_box();

    //! Returns the bounding box of this mesh.
    /*!
     \returns The bounding box of this triangle.
     */
    virtual Box getBBox();

    //! Returns the centroid of this mesh.
    /*!
     \returns The centroid of this triangle.
     */
    virtual Vec getCentroid();

};

#endif
