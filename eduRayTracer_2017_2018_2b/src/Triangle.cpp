/*
 * eduRayTracer - An Educational Ray Tracing Software Package
 * Contributor(s): Pedro Santana - ISCTE-IUL (2014-2016)
 */

#include "Triangle.hpp"

Triangle::Triangle (Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m) : Surface(_m), va(_va), vb(_vb), vc(_vc), cw(_cw){

    vertexes.push_back(_va);
	vertexes.push_back(_vb);
	vertexes.push_back(_vc);

    Vec v1 = (vc->o-va->o);
    Vec v2 = (vb->o-va->o);
    
    if (cw)
        n = (v1%v2).normalize();
    else
        n = (v2%v1).normalize();
    
    local_n = n;

    va->n = n;
    vb->n = n;
    vc->n = n;

    va->local_n = n;
    vb->local_n = n;
    vc->local_n = n;

    smooth_shading = false;

    bounding_box();
}

Triangle::Triangle (Vertex *_va, Vertex *_vb, Vertex *_vc, bool _cw, Material *_m, bool _smooth_shading) : Triangle(_va, _vb, _vc, _cw, _m){
    smooth_shading = _smooth_shading;
    
    bounding_box();
}

bool Triangle::hit(const Ray &r, hit_rec &h_r, double t_min, double t_max){

    double a = va->o.x-vb->o.x;   double d = va->o.x-vc->o.x;   double g = r.d.x;
    double b = va->o.y-vb->o.y;   double e = va->o.y-vc->o.y;   double h = r.d.y;
    double c = va->o.z-vb->o.z;   double f = va->o.z-vc->o.z;   double i = r.d.z;
    
    double j = va->o.x-r.o.x;   double k = va->o.y-r.o.y;   double l = va->o.z-r.o.z;
    
    double M = a*(e*i- h*f) + b*(g*f- d*i) + c*(d*h - e*g);
    
    h_r.t = -(f*(a*k - j*b) + e*(j*c - a*l) + d*(b*l - k*c))/M;
    
    if (h_r.t<t_min || h_r.t>t_max)
        return false;


    h_r.gamma = (i*(a*k - j*b) + h*(j*c - a*l) + g*(b*l - k*c))/M;
    
    if (h_r.gamma<0 || h_r.gamma>1)
        return false;
    
    h_r.beta = (j*(e*i - h*f) + k*(g*f - d*i) + l*(d*h - e*g))/M;
    if (h_r.beta<0 || h_r.beta>1-h_r.gamma)
        return false;
    
    h_r.alpha = 1-h_r.gamma-h_r.beta;
    
    h_r.point = r.o + r.d*h_r.t;
    
    if (smooth_shading)
        h_r.normal = va->n*h_r.alpha + vb->n*h_r.beta + vc->n*h_r.gamma;    
    else
        h_r.normal = n;
   
    
    #ifdef IMPLICIT_DOUBLE_SIDE
    // -- to handle implicitly double sided materials 
    // -- be aware that some rendering artifacts are likely to appear in imported meshes
    if (r.d.dot(h_r.normal) > r.d.dot(h_r.normal*(-1)))
        h_r.normal = h_r.normal*(-1);
    #endif

    h_r.tex_coord = va->tex_coord*h_r.alpha + vb->tex_coord*h_r.beta + vc->tex_coord*h_r.gamma;
    
    h_r.surface = this;

    return true;
    
}

Box Triangle::getBBox(){
    return *b_box;
}
    
Vec Triangle::getCentroid(){
    Vec cent = (vertexes[0]->o + vertexes[1]->o + vertexes[2]->o)*(1./3.);
    return cent;
}

void Triangle::addSurfaces(std::vector<Surface*> &objects){
    objects.push_back(this);    
}

void Triangle::bounding_box(){
 
    if (b_box == NULL){
        b_box = new Box(Vec(1e20,1e20,1e20), Vec(-1e20,-1e20,-1e20));
    }

    b_box->min = Vec(1e20,1e20,1e20); 
    b_box->max = Vec(-1e20,-1e20,-1e20);

    for (int i=0;i<vertexes.size();i++){
        if (vertexes[i]->o.x < b_box->min.x) b_box->min.x = vertexes[i]->o.x;
        if (vertexes[i]->o.x > b_box->max.x) b_box->max.x = vertexes[i]->o.x;
        if (vertexes[i]->o.y < b_box->min.y) b_box->min.y = vertexes[i]->o.y;
        if (vertexes[i]->o.y > b_box->max.y) b_box->max.y = vertexes[i]->o.y;
        if (vertexes[i]->o.z < b_box->min.z) b_box->min.z = vertexes[i]->o.z;
        if (vertexes[i]->o.z > b_box->max.z) b_box->max.z = vertexes[i]->o.z;
    }
 
 }

