/*
* eduRayTracer - An Educational Ray Tracing Software Package
* Author: Pedro Santana - ISCTE-IUL (2014-2017)
*/

*** AVISO LEGAL *****

O eduRayTracer, assim como os materiais que lhe estão associados (e.g., código fonte, documentação, laboratórios), são para serem usados exclusivamente no contexto da UC de P3D do ISCTE-IUL, não podendo ser distribuídos, sob qualquer forma, fora do contexto da UC de P3D do ISCTE-IUL. 

*********************
 
------------------------------------
Indice:
------------------------------------

1. Dicas de instalação do eduRayTracer em Linux
2. Estrutura do eduRaytracer
3. Como compilar o seu código
4. Como interagir com eduRayTracer durante a sua execução
5. Como parar processo de rendering demorado
6. Como gravar animações
7. Tutoriais para importar .ppm/.obj e utilização do IDE QT Creator 

------------------------------------
Dicas de instalação do eduRayTracer em Linux
------------------------------------

Não precisa de executar as seguintes instalações se estiver a utilizar a máquina virtual fornecida. 
Contudo, precisa de descarregar e instalar o eduRayTracer.

- Instalar o g++, o SDL2.0 e o OpenMP:

   1. Execute no terminal: sudo apt-get install g++
   2. Execute no terminal: sudo apt-get install libsdl2-dev
   3. Execute no terminal: sudo apt-get install libgomp1
    

- Instalar o editor de texto Geany:

  1. Execute no terminal: sudo apt-get install geany


- Para instalar o SDL2.0 noutra distribuição de Linux: 

  1. Recorra às dicas disponibilizadas no seguinte site: https://wiki.libsdl.org/Installation 


------------------------------------
Estrutura do eduRaytracer
------------------------------------

O pacote de software eduRayTracer contém as seguintes pastas:

/src: Pasta com o código fonte base da API do eduRayTracer. Este código fonte implementa a grande parte dos conteúdos teóricos leccionadas ao longo do semestre. Este código fonte não é suposto ser alterado no primeiro projeto prático, podendo ser no segundo projeto, onde é suposto desenvolver uma nova feature para o eduRayTracer.

/examples: Pasta com código fonte de exemplos de cenas criadas a partir da API do eduRayTracer. Os vários exemplos serão realizados nas aulas laboratoriais, servido de base para o seu primeiro projeto prático. 

/doc: Pasta contendo a documentação principal do eduRayTracer. Para aceder aos vários documentos deverá abrir o ficheiro ‘index.html’ num web browser. Pode também aceder ao manual num ficheiro único através do ficheiro ‘ref_man.pdf’.

/renders: Pasta onde normalmente se guardam as imagens geradas pelo software. Sempre que pressionar 'g' na janela principal do eduRayTracer, uma nova imagem será guardada nesta pasta com formato .ppm.

/data: Pasta onde normalmente se guardam os dados a serem importados pelo eduRayTracer, como ficheiros .ppm contendo imagens para serem utilizadas como texturas ou ficheiros .obj contedo malhas poligonais para serem incorporadas na cena.

------------------------------------
Como compilar o seu código
------------------------------------

Antes de se poder compilar um novo exemplo, ou seja, um novo ficheiro .cpp guardado na pasta /examples, é necessário incluir o mesmo no Makefile do projeto, que também se encontra na pasta /examples. A seguinte figura apresenta um excerto do ficheiro Makefile que permitiria compilar um ficheiro denominado de hello_world.cpp:

  ... 

  all: hello_world ...

  ...

  hello_world: $(SRC_OBJ) hello_world.cpp

        $(CC) -o $@.out $^ $(CFLAGS)


A compilação propriamente dita é realizada da seguinte forma:                   

 1. Abra um terminal.
 2. Vá para a pasta onde se encontra o ficheiro hello_world.cpp executando: cd [full-path]/eduRayTracer/examples
 3. Inicie a compilação executando: make hello_world
 4. Se a compilação não encontrou erros, pode então executar o seu programa: ./hello_world.out
 5. Para sair da aplicação pressione 'q'

------------------------------------
Como interagir com eduRayTracer durante a sua execução
------------------------------------
 
Evite pressionar uma tecla antes do processo de rendering anterior terminar.

Teclas:

 arrows: move along the xy-plane
 q: exit the program
 a,z: move along the z-axis
 r: render the current scene
 m: swicht between moving camera and moving 'look at' point 
 h: turn on/off reflections and transparencies
 s: switch to hard shadows and soft shadows
 d: turn on/off antialiasing 
 +: increase render resolution 
 -: decrease render resolution 
 f: change to 100x100 resolution 
 g: save the rendered image to a timestamped .ppm file


------------------------------------
Como parar processo de rendering demorado
------------------------------------

 Deverá (para o caso do hello_world):

     1. Abrir um terminal
     2. Executar: killall -9 hello_world.out

------------------------------------
Como gravar animações
------------------------------------

Para poder gravar animações, poderá instalar o software ImageMagick, que pode ser instalado fazendo no terminal (forneça a password que usou para entrar na máquina virtual):

  sudo apt-get install imagemagick

Para criar a animação terá de gravar uma série de imagens pressionado em ‘g’ na interface gráfica. Sempre que pressionar essa tecla, uma imagem é gerada e gravada na pasta /renders. Antes de iniciar a gravação deverá apagar os ficheiros já existentes nessa mesma pasta fazendo:

  rm ~/eduRayTracer/renders/*.ppm

Após ter gravado todas as imagens, deverá convertê-las num único ficheiro animado .gif fazendo:

  cd [full_path]/eduRayTracer/renders/ 

  convert -delay 1 -loop 1 *.ppm animation.gif

Para visualizar a animação poderá abrir o ficheiro com um web browser, como o firefox.


------------------------------------
Tutoriais para importar .ppm/.obj e utilização do IDE QT Creator 
------------------------------------

Site para fazer download de malhas poligonais:

  http://tf3dm.com


Notas sobre a utilização de malhas poligonais importadas:

- Na versão atual do eduRayTracer, apenas se aceitam malhas poligonais com até uma textura associada.

- Na versão atual do eduRayTracer, as malhas poligonais têm de passar pelo Mesh Lab (http://meshlab.sourceforge.net) antes de serem utilizadas (ver tutorial).


Pasta na Google Drive com tutoriais para utilização de texturas e malhas no eduRayTracer e utilização do IDE QT Creator:

  https://drive.google.com/folderview?id=0Bz3Cpi2F_GQ6T0ZHNS0yLW5SVkk&usp=sharing


