//
// Created by abeltenera on 09/11/19.
//

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>

#include "Material.hpp"
#include "Transform.hpp"
#include "Mesh.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "Scene.hpp"
#include "Sphere.hpp"
#include "RayTracer.hpp"
#include "common/Cuboid.hpp"
#include "common/Curve3D.hpp"
#include "utils.hpp"
#include "Material.hpp"
#include "common/ProtectionRail.hpp"
#include "common/StraightRoad.hpp"
#include "common/CurvedRoad.hpp"
#include "Box.hpp"
#include "common/CopCar.hpp"
#include "common/CarSiren.hpp"
#include "controllers/Driver.hpp"
#include "controllers/GateRemote.hpp"
#include "common/RoadGate.hpp"
#include <vector>
#include "common/SRoad.hpp"

CopCar* car;
CarSiren* siren;
Camera *camera;
void redraw(RayTracer *renderer, SDL_Event &event);
Driver* driver;
RoadGate* roadGate;
GateRemote* gateRemote;
Scene * scene;
hit_rec h_r;

int main(int argc, char **)
{
    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL);
    // -- Camera settings
    camera = new Camera(-1, 1, 1, -1, 2, 100, 100, 400, 400);

    // -- Structure to hold the scene geometry and materials
    scene = new Scene();
    Material * m_sky = new Material(Vec(0,0,0), Vec(0,0,0), Vec(0.3,0.3,0.3), 0, 1, 1.1, true, false, false, false, "../data/sky.ppm");

    Sphere *sky = new Sphere(Vec(0,0,0), 200, m_sky, true);

    sky->setTransform(new Transform(0,-M_PI_2,0,0,0,0,1,1,1,1));

    scene->addSurface(sky);

    std::vector<RoadGate*> roadGates;

    Mat testRotation = (Mat::RotZ(M_PI)* Mat::RotY(M_PI/2)*Mat::RotX(M_PI/2));



    //CornellBox(scene, new Transform(0.0, 0.0, 0.0, 0, 0, -10, 5, 5, 5, 1));
    //CornellBoxTextured(scene, new Transform(0.0, 0.0, 0.0, 0, 0, -10, 1.5, 1.5, 1.5, 1));

    LightBulb(scene, Vec(0,7,-10), Vec(1,1,1), 0.5, NULL);

    // Test cuboid
    std::cout << "Test 1" << std::endl;
    Transform *tf = new Transform(M_PI_2/2, 0, 0, 0, 0, -10, 1, 1, 1, 1);
    Transform *tf_car = new Transform(0, 0, 0, 0, 0, 0, 1, 1, 1, 1);
    Transform *tf_base = new Transform(Mat::Translate(0,0,0), 1);
    //Transform *tf_car = new Transform(testRotation, 1);


    Transform *tf_sphere = new Transform(Mat::Translate(0,0,-10)*Mat::Scale(10,10,10),1);
    Transform *tf_sphere2 = new Transform(0, 0, 0, 5, 0, -10, 0.5, 0.5, 0.5, 1);
    Material* mtex= new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, "../data/die_texture.ppm");

    /*Cuboid* cuboid = new Cuboid(1,2,3);
    std::cout << "Test 2" << std::endl;
    Cuboid* cuboid2 = new Cuboid(1,2,3);
    Mat m1 = Mat::Translate(1,1,1);

    //cuboid->addCuboidToScene(tf,scene);

    //cuboid->addCuboidToScene(m1,tf, mtex, scene, false);
    ProtectionRail* protectionRail = new ProtectionRail(1,3,7);
    std::cout << "Test 3" << std::endl;
    //protectionRail->addProtectionRailToScene(tf, scene);
    /*StraightRoad(double _rw, double _rh, double _rl,
            double _prw, double _prh, int _railsPerSide,
            double _sw, double _sh,
            Material *_roadTopMaterial, Material *_sidewalkTopMaterial, Material *_protectionRailMaterial);*/

    StraightRoad* straightRoad = new StraightRoad(3, 0.25, 6, 0.1, 0.5, 7, 0.5, 0.30, mtex, mtex, mtex);
    //straightRoad->addStraightRoadToScene(tf, scene);
    //StraightRoad *straightRoad = new StraightRoad();
    //straightRoad->loadDecorations(2, "meshes/CartoonTree.obj", "meshes/StreetLamp.obj", defaultMaterial, defaultMaterial, tf, scene);
    Material* treeMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, "textures/TreeTexture.ppm");
    CurvedRoad* curvedRoad = new CurvedRoad(3, 0.25, 10,
                                            0.1,0.5,7,
                                            0.5,0.3,0, M_PI_2, 7,
                                            mtex,NULL,mtex );

    curvedRoad->addCurvedRoadToScene(tf_car,scene,true);
    curvedRoad->loadDecorations(2, "meshes/CartoonTree.obj", "meshes/StreetLamp.obj", treeMaterial, defaultMaterial, tf, scene);

    //tf->setMat(tf->getMat()*Mat::RotY(M_PI_2)*tf->getMat().invert());
    //Curve3D* curve3D = new Curve3D(1,1,10, 0, M_PI, 7);
    //std::cout<< "Build curve3D";
    //curve3D->addCurve3DToScene(tf, scene, false);

    Cuboid* rectangle = new Cuboid(200, 0, 200,mtex);
    //rectangle->setDrawOnlyTopSurfaceMode(true);
    //rectangle->addCuboidToScene(tf_base, scene);


    SRoad* sRoad = new SRoad(3,0.25,10,0.1,0.5,7,0.5,0.3,7,mtex,NULL,mtex);
   // sRoad->addSRoadToScene(tf_base,scene,true, true, 2, "meshes/CartoonTree.obj", "meshes/StreetLamp.obj", defaultMaterial, defaultMaterial);
    //tf_base->setMat(tf->getMat()*Mat::Translate(0,5,0)*tf->getMat().invert());
    //tf_base->setMat(tf->getMat()*Mat::RotY(M_PI)*tf->getMat().invert());

    // -------------------------------

    Material *m_car = new Material(Vec(1,0.5,0), Vec(1.,0.5,0), Vec(0.3,0.3,0.3), 200, 1, 1.3, false, false, false, false, NULL);
    //car = new CopCar(6, NULL, "car1", tf_car, m_car, scene);

   // driver = new Driver(car, SDL_SCANCODE_G,  SDL_SCANCODE_J,  SDL_SCANCODE_Y,  SDL_SCANCODE_F,  SDL_SCANCODE_H,  SDL_SCANCODE_M);

    /*Sphere* lightBulb = new Sphere(Vec(0,0,0), 1, defaultMaterial, false);
    lightBulb->setTransform(tf_sphere);
    scene->addSurface(lightBulb);

    Sphere* lightBulb2 = new Sphere(Vec(0,0,0), 0.5, defaultMaterial, false);
    lightBulb2->setTransform(tf_sphere2);
    scene->addSurface(lightBulb2);*/

    roadGate = new RoadGate("gate1", 0.5, 2, 5,m_car, defaultMaterial, tf_base, scene, false );
    roadGate->rotateGate(M_PI_2/2);

    roadGates.push_back(roadGate);
    //roadGate->setGateAngle(0);
    gateRemote = new GateRemote(roadGates);

    //tf->setMat(tf->getMat()*Mat::RotY(M_PI_2)*tf->getMat().invert());
    //tf->setMat(tf->getMat()*Mat::Translate(0,0,5));//*tf->getMat().invert());

   // roadGate->setGateAngle(M_PI_2/2);

    //siren = new CarSiren(1, 0.2,1, tf, m_car, m_car,scene, "siren", 12);
    //CarSiren* siren2 = new CarSiren(1, 0.2, tf_car, m_car, m_car,scene, "siren", 12);
    /*Mesh *mesh_car = new Mesh("meshes/LowPolyFiatUNO2.obj", m_car,false);
    mesh_car->setTransform(tf_car);
    scene->addSurface(mesh_car, "car1");
    mesh_car->bounding_box();
    Box carBox = mesh_car->getBBox();
    Vec min = carBox.min;
    Vec max = carBox.max;
    Vec extent = carBox.extent;
    std::cout << "Box min: x " << min.x << " y " << min.y << " z " << min.z << std::endl;
    std::cout << "Box max: x " << max.x << " y " << max.y << " z " << max.z << std::endl;
    */

    // Create a RayTracer to render the scene from the camera location
    RayTracer renderer(camera, scene);


    // render the scene and react to keyboard events
    //main_loop(renderer, NULL, true, true, true);
    main_loop(renderer, &redraw, true, true, true);
}

void redraw(RayTracer *renderer, SDL_Event &event) {
    std::cout << "\nRedraw\n";

    gateRemote->OpenIfHit(renderer,event,h_r,scene);




    //car->moveCar(1,2,1);
    //car->rotateCar(0,45,0);
    //car->printBoundingBox();

    //driver->drive(event);
    //Mat t_cam_offset = Mat::FullPose(0, 0, 0, 0,2,10, 1,1,1);
    //camera->transform(car->getTransform()->getMat() * t_cam_offset);
    //car->printTfMat();

    /*if (event.type == SDL_MOUSEBUTTONDOWN){

        std::cout << "ASDASDASDASDASDASDASDAS" << event.button.button << std::endl;

        std::cout << "Mouse's button pressed!" << std::endl;
        //siren->rotateSiren();
        if (event.button.button == SDL_BUTTON_LEFT){

            std::cout << "Mouse's left button pressed!" << std::endl;
            car->rotateCar(0,M_PI_2/2,0);
            car->turnSirens(true);
        }

        if (event.button.button == SDL_BUTTON_RIGHT){

            std::cout << "Mouse's right button pressed!" << std::endl;
            car->rotateCar(0,-M_PI_2/2,0);
            car->turnSirens(true);
        }


        //
    }

    if(event.type == SDL_KEYDOWN){

        std::cout << "ASDASDASDASDASDASDASDAS " << typeid(event.button.button).name() << std::endl;


        if(event.button.button == SDL_SCANCODE_L){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->moveCar(0,0,1);
        }
        if(event.button.button == SDL_SCANCODE_O){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->moveCar(0,0,-1);
        }

        if(event.button.button == SDL_SCANCODE_N){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->driveCar(M_PI/4, 1);
        }

        if(event.button.button == SDL_SCANCODE_M){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->driveCar(-M_PI/5, 2);
        }

        if(event.button.button == SDL_SCANCODE_K){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->driveCar(0, -1);
        }

        if(event.button.button == SDL_SCANCODE_U){
            std::cout << "Keyboard button pressed!" << std::endl;
            car->driveCar(0, 1);
        }
    }
*/

}
