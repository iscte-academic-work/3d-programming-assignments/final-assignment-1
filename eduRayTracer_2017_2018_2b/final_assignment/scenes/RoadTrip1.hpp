//
// Created by abeltenera on 02/01/20.
//

#ifndef FINAL_ASSIGNMENT_ROADTRIP1_HPP
#define FINAL_ASSIGNMENT_ROADTRIP1_HPP

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>

#include "Material.hpp"
#include "Transform.hpp"
#include "Mesh.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "Scene.hpp"
#include "Sphere.hpp"
#include "RayTracer.hpp"
#include "../common/StraightRoad.hpp"
#include "../common/WavingFlag.hpp"
#include "utils.hpp"

#include "../common/CopCar.hpp"
#include "../controllers/Driver.hpp"
#include "../controllers/GateRemote.hpp"

#include "../common/SRoad.hpp"
#include "../helpers/AppProperties.hpp"

enum CameraModes{
    FREE = 0,
    FIRST_PERSON = 1,
    LOOK_AT_CLICKED_POINT = 2,
    TOP_VIEW = 3
};

enum CarDrivingMode{
    AUTOPILOT = 0,
    MANUAL = 1
};
class RoadTrip1 {
public:
    Vec defaultAtVec = Vec(0,0,0);
    const std::string sRoad1SurfaceSuffix = "sRoad1";
    const std::string sRoad2SurfaceSuffix = "sRoad2";
    const std::string seg1SurfaceSuffix = "seg1";
    const std::string seg2SurfaceSuffix = "seg2";
    const std::string seg3SurfaceSuffix = "seg3";
    const std::string seg4SurfaceSuffix = "seg4";
    const std::string seg5SurfaceSuffix = "seg5";
    const std::string curve1SurfaceSuffix = "curve1";
    const std::string curve2SurfaceSuffix = "curve2";
    const std::string curve3SurfaceSuffix = "curve3";
    const std::string curve4SurfaceSuffix = "curve4";

    const char* roadTextureLocation = "textures/road_texture.ppm";

    const char* roadGateTextureLocation = "textures/roadGate.ppm";
    const char* carTextureLocation = "textures/cartexture.ppm";
    const char* treeTextureLocation = "textures/treetex.ppm";
    const char* rgPoleTextureLocation = "textures/poletext.ppm";
    const char* rustyTextureLocation = "textures/rusty.ppm";
    const char* rockTextureLocation = "textures/Rock-Texture-Surface.ppm";
    const char* greenGroundTextureLocation = "textures/green_ground_texture.ppm";
    const char* sidewalkTextureLocation = "textures/sidewalk_texture.ppm";
    const char* protectionRailTextureLocation = "textures/asbestos_texture.ppm";
    const char* streetLampMeshLocation = "meshes/StreetLamp.obj";
    //const char* treeMeshLocation = "meshes/CartoonTree.obj";
    const char* treeMeshLocation = "meshes/myowntree2.obj";
    const char* rockMeshLocation = "meshes/Rock3.obj";
    //const char* treeMeshLocation = "meshes/lowpolytree.obj";
    //const char* carMeshLocation = "meshes/LowPolyFiatUNO2.obj";
    const char* carMeshLocation = "meshes/vwpassat.obj";
    const std::string carSufaceId = "cop_car_user";
    const std::string botCarSurfaceIdPrefix = "cop_car_bot";
    const std::string boulderSurfaceIdPrefix = "boulder";

    RoadTrip1(bool createEmptyRoad, Scene* scene, Mat roadDisplacement);
    ~RoadTrip1();

    void loadScene();
    RayTracer getRenderer();
    void redraw(RayTracer *renderer, SDL_Event &event);

private:
    bool isEmptyRoad = true;
    CarDrivingMode driveMode = CarDrivingMode::AUTOPILOT; //sets the drive mode for the selected car
    Driver* selectedDriver;

    const double decorationHeight = 2;
    bool botCarSelected = false;
    std::string selectedBotCarSurfaceId = "";
    double speedIncreaseStep = 0.1;
    double botCarManualControlEnabled = false;


    std::vector<Driver*> botCars;
    Mat lastMatWithoutCollision = Mat();
    int boulderCount = 0;
    int carCount = 0;
    bool cameraInFPMode = false; // If true, the camera commutes to first player mode
    int cameraMode = 0;

    Mat carInitialMat = Mat();
    const double botCarMaxSpeed = 0.5;
    const double botCarMaxReverseSpeed = 1.0;
    const double botCarThrottleSteps = 1;
    const double botCarReverseSteps = 5;

    const double carDistanceToLeft = 1.75;
    const double rockWidth = 1;
    const double sRoadLength = 10;
    const double numberOfDivisionsCurve = 10;
    const double railsPerSideCurve = 10;
    const double rw = 3, rh = 0.25, prw = 0.1, prh=0.3, sw = 0.5, sh=0.3;

    hit_rec h_r;
    const double copCarLength = 1.5;
    Transform* rootTransform;
    Mat cameraMat; // saves the last transform of the camera before commuting to first player mode
    Camera *camera;
    GateRemote* gateRemote;
    Scene * scene;
    SRoad* sRoad;
    RayTracer renderer = RayTracer(camera, scene);
    Material* rockMaterial;


    WavingFlag* flag;

    // Flag properties
    Vec wind = Vec(1.0, 0.0, 1.0);
    double windStrength=3.0;
    double tmp=0.0;
    double currentSine;
    double sangle=0.0;
    bool up=true;
    bool down=false;
    double radian=M_PI/180;
    double crit_angle=sin(5*windStrength*radian);



    Driver *createDriverInstance(Material* material, Transform *carTf);
};


#endif //FINAL_ASSIGNMENT_ROADTRIP1_HPP
