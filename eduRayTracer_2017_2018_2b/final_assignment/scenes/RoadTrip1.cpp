//
// Created by abeltenera on 02/01/20.
//

#include "RoadTrip1.hpp"

RoadTrip1* roadTrip;
RoadTrip1* roadTrip2;
void redraw(RayTracer *renderer, SDL_Event &event);
const char* skyTextureLocation = "textures/sky.ppm";
void printPost(){
    std::cout << "Welcome to P3D Final Assignment" << std::endl;
    std::cout << "\nGeneric controls:" << std::endl;
    std::cout << "=> O (otter) - Commute camera between the following modes:" << std::endl;
    std::cout << "\t*Free" << std::endl;
    std::cout << "\t*Third person" << std::endl;
    std::cout << "\t*Focused (use also the mouse left click to focus on a point)" << std::endl;
    std::cout << "\t*Top view" << std::endl;
    std::cout << "=> P - Commute car control modes:" << std::endl;
    std::cout << "\t*Autopilot" << std::endl;
    std::cout << "\t*Manual control" << std::endl;
    std::cout << "=> Mouse click:" << std::endl;
    std::cout << "\t*On road: creates an obstacle" << std::endl;
    std::cout << "\t*On car: selects a car" << std::endl;
    std::cout << "\t*On road gate crossbar: opens it (the bigger the distance to its origin, the wider it opens)" << std::endl;
    std::cout << "\t*On road gate pole: closes it" << std::endl;
    std::cout << "=> Keypad +: increase car max speed" << std::endl;
    std::cout << "=> Keypad -: decrease car speed" << std::endl;
    std::cout << "=> Keypad *: add car to scene" << std::endl;
    std::cout << "\nManual car drive controls:" << std::endl;
    std::cout << "=> Numpad 8: throttle forward" << std::endl;
    std::cout << "=> Numpad 4: turn left" << std::endl;
    std::cout << "=> Numpad 6: turn right" << std::endl;
    std::cout << "=> Numpad 5: brake" << std::endl;
    std::cout << "=> Numpad 2: reverse" << std::endl;
    std::cout << "=> Numpad 7: turn siren on/off" << std::endl;
    std::cout << "\nTo refresh it is recomended to use the \".\" dot key " << std::endl;
    std::cout << "\nOther controls: check the eduRayTracer documentation " << std::endl;
    std::cout << "\nIt is recommended to press O (otter) after screen loads " << std::endl;
}

int main(int argc, char **){
    int createSecond = 0;
    int disableCout = 0;
    printPost();
    std::cout << "\nCreate second road trip? (0-No/1-Yes) ";
    std::cin >> createSecond;
    std::cout << "\nDisable cout? (0-No/1-Yes) ";
    std::cin >> disableCout;

    if(disableCout == 1)
        std::cout.rdbuf(NULL);

    Scene* scene = new Scene();
    // Instance of the main road
    roadTrip = new RoadTrip1(false,scene, Mat::Translate(0,0,0));

    // Instance of the second road if the user chooses so
    if(createSecond==1)
        roadTrip2 =  new RoadTrip1(true,scene, Mat::Translate(50,0,50)* Mat::RotY(M_PI_2));

    RayTracer renderer = roadTrip->getRenderer();

    // Loads sky
    Material* m_sky = new Material(Vec(0,0,0), Vec(0,0,0), Vec(0.3,0.3,0.3), 0, 1, 1.1, true, false, false, false, skyTextureLocation);
    Sphere *sky = new Sphere(Vec(0,0,0), 200, m_sky, true);
    sky->setTransform(new Transform(0,-M_PI_2,0,0,0,0,1,1,1,AppProperties::getTransformId()));
    scene->addSurface(sky);
    // Loads the main light
    LightBulb(scene, Vec(0,30,-10), Vec(1,1,1), 0.1, NULL);

    // Loads rendering loop
    main_loop(renderer, &redraw, true, true, true);

}

void redraw(RayTracer *renderer, SDL_Event &event) {
    roadTrip->redraw(renderer, event);
}


RoadTrip1::RoadTrip1(bool createEmptyRoad, Scene* _scene, Mat roadDisplacement) {
    std::cout << "Never ends"<< std::endl;
    isEmptyRoad = createEmptyRoad;
    std::vector<RoadGate*> roadGates;
    if(!isEmptyRoad)
        camera = new Camera(-1, 1, 1, -1, 2, 100, 100, 400, 400);
    scene = _scene;
    // Creates material instances

    Material* carMaterial = new Material(Vec(0.1,0.7,0.2), Vec(0.1,0.7,0.2), Vec(0.1,0.1,0.1), 0.2, 0, 0, false, true, false, false, NULL);
    //Material* treeMaterial = new Material(Vec(0.1,0.7,0), Vec(0.1,0.1,0.1), Vec(0.1,0.1,0.1), 0.2, 0, 0, false, true, false, false, NULL);
    Material* lampMaterial = new Material(Vec(0.4,0.4,0.4), Vec(0.2,0.2,0.2), Vec(0.1,0.1,0.1), 0.2, 0, 0, false, true, false, false, NULL);

    Material* greenGroundMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, true, greenGroundTextureLocation);
    //Material* carMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, carTextureLocation);
    Material* treeMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.5,0.5,0.5), 5, 0.3, 1.3, false, false, false, false, treeTextureLocation);
    Material* roadMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, roadTextureLocation);
    Material* rgPoleMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.5,0.5,0.5), 5, 0.3, 1.3, false, true, false, false, rgPoleTextureLocation);
    Material* roadGateMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, roadGateTextureLocation);
    Material* sideWalkMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, sidewalkTextureLocation);
    Material* protectionRailMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, protectionRailTextureLocation);
    Material* rustyRailMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, rustyTextureLocation);
    rockMaterial = new Material (Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 5, 0.3, 1.3, false, false, false, false, rockTextureLocation);

    Material* flagTex1 = new Material(Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, true, false, NULL);
    Material* flagTex2 = new Material(Vec(0,0,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, false, false,NULL);
    Material* flagTex3 = new Material(Vec(1,0,0), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, false, false,"textures/checkeredFlag.ppm");

    flag= new WavingFlag(flagTex1, flagTex2, flagTex3,windStrength);
    wind=Vec(1.0, 0.0, 1.0);

    // Creates root transform
    rootTransform = new Transform(Mat::Translate(0,0,0), AppProperties::getTransformId());
    Transform* rootRoadTransform = new Transform(Mat::Translate(0,0,0),AppProperties::getTransformId());


    // Adds s roads
    SRoad* sRoad = new SRoad(rw,rh,sRoadLength,prw,prh,railsPerSideCurve,sw,sh,numberOfDivisionsCurve,roadMaterial,sideWalkMaterial, rustyRailMaterial);
    sRoad->setTreeDensity(0.01);
    sRoad->setStreetLampDensity(0.01);
    double roadTotalWidth = rw+2*sw; //width of the road having the sidewalk into consideration
    double sRoadBoxLength = sRoad->getSRoadBoxLength();
    double sRoadBoxWidth = sRoad->getSRoadBoxWidth();
    double curveRadius = (sRoadBoxLength/4);
    double curveLength = curveRadius*M_PI_2;
    double straightRoad1Length = sRoadBoxLength*2;
    double straightRoad2Length = sRoadBoxLength*3/4 + roadTotalWidth/2 - prw;
    double straightRoad3Length = sRoadBoxLength + curveRadius/2 + 2*sw + prw;//straightRoad1Length*3/4 - curveRadius -2*sw;// - 2*curveRadius;

    Transform* sRoad1Transform = new Transform(Mat::Translate(0,0,0), AppProperties::getTransformId());
    Transform* sRoad2Transform = new Transform(Mat::Translate(sRoadBoxLength/2 + curveRadius -rw -sw,0,-straightRoad1Length/2-sRoadBoxLength - sw + curveRadius)*Mat::RotY(-M_PI/2), AppProperties::getTransformId());
    sRoad->setSurfaceIdSuffix(sRoad1SurfaceSuffix);
    sRoad->addSRoadToScene(sRoad1Transform,scene,true, true, 2, treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial);
    sRoad->setSurfaceIdSuffix(sRoad2SurfaceSuffix);
    sRoad->addSRoadToScene(sRoad2Transform,scene,true,true, 2,  treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial);


    // Curved road builder

    CurvedRoad* curve = new CurvedRoad(rw,rh, curveLength, prw, prh, 7, sw,sh, 0, M_PI_2, 7, roadMaterial, sideWalkMaterial, rustyRailMaterial);
    curve->setStreetLampDensity(0.5);
    curve->setTreeDensity(0.7);
    Transform* curve1Transform = new Transform(Mat::Translate(-straightRoad1Length,0,(sRoadBoxLength)/2 - curveRadius - sw)*Mat::RotY(-M_PI_2) , AppProperties::getTransformId());
    Transform* curve2Transform = new Transform(Mat::Translate(-straightRoad1Length,0, -straightRoad1Length/2 - curveRadius -sRoadBoxLength/2 -sw)*Mat::RotY(-M_PI) , AppProperties::getTransformId());
    Transform* curve3Transform = new Transform(Mat::Translate(0,0, -straightRoad1Length/2 - curveRadius -sRoadBoxLength/2 -sw)*Mat::RotY(M_PI/2) , AppProperties::getTransformId());
    Transform* curve4Transform = new Transform(Mat::Translate(straightRoad2Length,0, -sRoadBoxLength/2 - curveRadius/2 +sw + prw) , AppProperties::getTransformId());

    curve->setSurfaceIdSuffix(curve1SurfaceSuffix);
    curve->addCurvedRoadToScene(curve1Transform,scene, true);
    curve->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, curve1Transform , scene);
    curve->setSurfaceIdSuffix(curve2SurfaceSuffix);
    curve->addCurvedRoadToScene(curve2Transform,scene, true);
    curve->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, curve2Transform , scene);
    curve->setSurfaceIdSuffix(curve3SurfaceSuffix);
    curve->addCurvedRoadToScene(curve3Transform,scene, true);
    curve->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, curve3Transform , scene);
    curve->setSurfaceIdSuffix(curve4SurfaceSuffix);
    curve->addCurvedRoadToScene(curve4Transform,scene, true);
    curve->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, curve4Transform , scene);

    // Straight road builder
    StraightRoad* straightRoad1 = new StraightRoad(rw, rh, straightRoad1Length, prw, prh, 20, sw, sh, roadMaterial, sideWalkMaterial, rustyRailMaterial);
    Transform* segment1Transform = new Transform(Mat::Translate(-straightRoad1Length/2, 0, (sRoadBoxLength-roadTotalWidth)/2)*Mat::RotY(-M_PI/2), AppProperties::getTransformId());
    Transform* segment2Transform = new Transform(Mat::Translate(-curveRadius-straightRoad1Length + rw/2, 0, -straightRoad1Length/4-curveRadius-sw), AppProperties::getTransformId());
    Transform* segment3Transform = new Transform(Mat::Translate(-straightRoad1Length/2, 0, - (straightRoad1Length/2 + curveRadius + sRoadBoxLength/2  + roadTotalWidth + sw +sw/2 + prw ))*Mat::RotY(-M_PI/2), AppProperties::getTransformId());

    straightRoad1->setSurfaceIdSuffix(seg1SurfaceSuffix);
    straightRoad1->addStraightRoadToScene(segment1Transform, scene);
    straightRoad1->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, segment1Transform , scene);
    straightRoad1->setSurfaceIdSuffix(seg2SurfaceSuffix);
    straightRoad1->addStraightRoadToScene(segment2Transform, scene);
    straightRoad1->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, segment2Transform , scene);
    straightRoad1->setSurfaceIdSuffix(seg3SurfaceSuffix);
    straightRoad1->addStraightRoadToScene(segment3Transform, scene);
    straightRoad1->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, segment3Transform , scene);

    StraightRoad* straightRoad2 = new StraightRoad(rw, rh, straightRoad2Length, prw, prh, 10, sw, sh, roadMaterial, sideWalkMaterial, rustyRailMaterial);
    Transform* segment4Transform = new Transform(Mat::Translate(straightRoad2Length/2, 0, -sRoadBoxLength/2 + roadTotalWidth/2)*Mat::RotY(-M_PI/2), AppProperties::getTransformId());
    straightRoad2->setSurfaceIdSuffix(seg4SurfaceSuffix);
    straightRoad2->addStraightRoadToScene(segment4Transform,scene);
    straightRoad2->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, segment4Transform , scene);

    StraightRoad* straightRoad3 = new StraightRoad(rw, rh, straightRoad3Length, prw, prh, 10, sw, sh, roadMaterial, sideWalkMaterial, rustyRailMaterial);
    Transform* segment5Transform = new Transform(Mat::Translate(sRoadBoxLength - roadTotalWidth/2 + curveRadius -rw -sw , 0, -(straightRoad3Length/2 + curveRadius -3*sw - prw+ curveRadius /2 + sRoadBoxWidth/2)), AppProperties::getTransformId());
    straightRoad3->setSurfaceIdSuffix(seg5SurfaceSuffix);
    straightRoad3->addStraightRoadToScene(segment5Transform, scene);
    straightRoad3->loadDecorations(decorationHeight,treeMeshLocation, streetLampMeshLocation, treeMaterial, lampMaterial, segment5Transform , scene);

    if(!isEmptyRoad) {

        // Road gate
        Transform* roadGate1Transform = new Transform(Mat::Translate(-straightRoad1Length/3,0.6,(sRoadBoxLength)/2 -rw-sw)*Mat::RotY(-M_PI_2), AppProperties::getTransformId());
        Transform* roadGate2Transform = new Transform(Mat::Translate(sRoadBoxLength - roadTotalWidth/2 + curveRadius -rw -sw -rw/2, 0.6, -(straightRoad3Length/2 + curveRadius -3*sw - prw+ curveRadius /2 + sRoadBoxWidth/2)), AppProperties::getTransformId());
        RoadGate* roadGate1 = new RoadGate("gate1", 0.20, 0.5, 0.9*rw, rgPoleMaterial, roadGateMaterial, roadGate1Transform, scene, true );
        RoadGate* roadGate2 = new RoadGate("gate2", 0.20, 0.5, 0.9*rw, rgPoleMaterial, roadGateMaterial, roadGate2Transform, scene, true );

        roadGates.push_back(roadGate1);
        roadGates.push_back(roadGate2);

        // Sets gate remote
        gateRemote = new GateRemote(roadGates);
        std::cout<< "\n\n Got here 1 \n\n\n";
        carInitialMat = Mat::Translate(-straightRoad1Length/2, rh/2, (sRoadBoxLength-roadTotalWidth)/2)*Mat::RotY(-M_PI/2);

        // Loads car and respective driver

        Transform *carTransform = new Transform(
                Mat::Translate(-straightRoad1Length / 2, rh / 2, (sRoadBoxLength - roadTotalWidth) / 2) *
                Mat::RotY(-M_PI / 2), AppProperties::getTransformId());
        selectedDriver = createDriverInstance(carMaterial, carTransform);
        botCars.push_back(selectedDriver);

        // Loads flag
        Transform* tf_flag1 = new Transform(Mat::Translate(-straightRoad1Length/3 - 1,0,(sRoadBoxLength)/2 -rw-sw -3)*Mat::RotY(-M_PI_2), AppProperties::getTransformId());
        flag->createFlag(scene, tf_flag1, wind);


        // Loads ground
        Cuboid *rectangle = new Cuboid(200, 0, 200, greenGroundMaterial);
        rectangle->setDrawOnlyTopSurfaceMode(true);
        rectangle->addCuboidToScene(rootTransform, scene);
        rootTransform->addChild(carTransform);
        rootTransform->addChild(roadGate1Transform);
        rootTransform->addChild(roadGate2Transform);
        rootTransform->addChild(tf_flag1);
    }
    // Sets transform hierarchies
    rootTransform->addChild(rootRoadTransform);

    rootRoadTransform->addChild(sRoad1Transform);
    rootRoadTransform->addChild(sRoad2Transform);
    rootRoadTransform->addChild(curve1Transform);
    rootRoadTransform->addChild(curve2Transform);
    rootRoadTransform->addChild(curve3Transform);
    rootRoadTransform->addChild(curve4Transform);
    rootRoadTransform->addChild(segment1Transform);
    rootRoadTransform->addChild(segment2Transform);
    rootRoadTransform->addChild(segment3Transform);
    rootRoadTransform->addChild(segment4Transform);
    rootRoadTransform->addChild(segment5Transform);


   if(!isEmptyRoad) {
       camera->setTransform(rootTransform);
       defaultAtVec = camera->a;
       renderer = RayTracer(camera, scene);
   }

    if(isEmptyRoad) {
        rootRoadTransform->setMat(rootRoadTransform->getMat() * roadDisplacement);
        rootTransform->update_global_tf();
    }

}

RoadTrip1::~RoadTrip1() {
    delete [] camera;
    delete []  gateRemote;
    delete [] scene;
    delete [] selectedDriver;
    delete [] rootTransform;
    delete [] sRoad;
    delete [] rockMaterial; 
}


RayTracer RoadTrip1::getRenderer() {return renderer;}

void RoadTrip1::redraw(RayTracer *renderer, SDL_Event &event) {
    if(!isEmptyRoad) {
        srand(time(NULL));
        flag->animateFlag();
        Mat t_cam_offset = Mat::FullPose(0, 0, 0, 0, 2, 5, 1, 1, 1);
        // Checks if gate was clicked and opens hit.
        gateRemote->OpenIfHit(renderer, event, h_r, scene);
        // ==========================
        // Management of key events
        // note: some are managed inside the Driver class!
        // =========================
        if (event.type == SDL_KEYDOWN) {
            // Key P -> Commutes between drive modes
            if (event.button.button == SDL_SCANCODE_P && selectedDriver != NULL) {
                driveMode = driveMode == CarDrivingMode::AUTOPILOT ? CarDrivingMode::MANUAL : CarDrivingMode::AUTOPILOT;
            }

            // Key O (O from Otter) -> Commutes between camera modes (If car was selected)
            if (event.button.button == SDL_SCANCODE_O && selectedDriver != NULL) {
                cameraMode = cameraMode == CameraModes::TOP_VIEW ? CameraModes::FREE : cameraMode + 1;
            }

            // Keypad *  -> Adds a car to the scene
            if (event.button.button == SDL_SCANCODE_KP_MULTIPLY) {
                // Material of random color
                Material *botCarMaterial = new Material(Vec(0.7, 0.2, 0.2), Vec(0.7, 0.2, 0.2), Vec(0.1, 0.1, 0.1), 0.2,
                                                        0, 0, false, true, false, false, NULL);
                // Creates the transform starting from the beginning of the trip
                Transform *botCarTransform = new Transform(carInitialMat, AppProperties::getTransformId());

                // Creates a bot car and driver
                Driver *botDriver = createDriverInstance(botCarMaterial, botCarTransform);
                rootTransform->addChild(botCarTransform);
                // Pushes created car into bot array
                botCars.push_back(botDriver);
                if (selectedDriver == NULL)
                    selectedDriver = botDriver;
            }

            // Key Up or Down: Increases or decreases speed of selected car
            if (selectedDriver != NULL) {
                double speedIncrease = event.button.button == SDL_SCANCODE_KP_PLUS ? speedIncreaseStep :
                                       (event.button.button == SDL_SCANCODE_KP_MINUS ? -speedIncreaseStep : 0);
                selectedDriver->increaseMaxThrottle(speedIncrease);
            }
        }

        // ====================
        // Management of click events
        // ====================

        // Gets the clicked point
        // Note: gate remote already updates the hit_rec
        Vec clickedPoint = h_r.point;
        Surface *clickedSurface = h_r.surface;
        std::string clickedSurfaceId = clickedSurface != NULL ? clickedSurface->id : "";
        if (event.type == SDL_MOUSEBUTTONDOWN && !clickedSurfaceId.empty()) {
            // If road was clicked, adds a boulder
            if (StringUtils::stringContains(clickedSurfaceId, "sr_road")) {

                double rand1 = 0.5 + double(rand() % 51) / 100.0;
                double rand2 = 0.5 + double(rand() % 51) / 100.0;
                double rand3 = 0.5 + double(rand() % 51) / 100.0;
                double rand4 = 0.5 + double(rand() % 51) / 100.0;

                std::cout << "A point in the road was clicked " << clickedPoint.x << " " << clickedPoint.y << " "
                          << clickedPoint.z << std::endl;

                std::string boulderId = boulderSurfaceIdPrefix + std::to_string(boulderCount);

                Mesh *rockMesh = new Mesh(boulderId, rockMeshLocation, rockMaterial, false);

                // We need to resize the rock to a proper size
                // Gets the bounding box of the lamp
                Box rockBox = rockMesh->getBBox();
                Vec rockMin = rockBox.min;
                Vec rockMax = rockBox.max;

                // Gets scaling matrix for rock, to resize it to a desired width
                Mat rockScaleMat = MathUtils::resizeMeshMat(rockMesh, rockWidth * rand1, X);
                // Creates transform matrix for rock. Scales to the desired width, and translates to the clicked point
                Transform *rockTf = new Transform(
                        Mat::Translate(clickedPoint.x, clickedPoint.y + rh / 2, clickedPoint.z) * rockScaleMat *
                        Mat::RotY(2 * M_PI * rand2) * Mat::RotX(2 * M_PI * rand3) * Mat::RotZ(2 * M_PI * rand4),
                        AppProperties::getTransformId());
                rockMesh->setTransform(rockTf);

                rootTransform->addChild(rockTf);
                scene->addSurface(rockMesh, boulderId);
                boulderCount++;
            }
                // If car was clicked
            else if (StringUtils::stringContains(clickedSurfaceId, botCarSurfaceIdPrefix)) {
                std::vector<Driver *>::iterator it = std::find_if(botCars.begin(), botCars.end(),
                                                                  [&clickedSurfaceId](Driver *driver) {
                                                                      return driver->getCarSurfaceId() ==
                                                                             clickedSurfaceId;
                                                                  }
                );
                selectedDriver = *it;
                if (selectedDriver != NULL)
                    std::cout << "Car with surface id " << selectedDriver->getCarSurfaceId() << " was selected.";
            }

        }


        // ==================
        // Setting the camera mode according to the user last choice
        //===================

        // There must be at least one car in scene to select a camera mode
        if (selectedDriver != NULL) {
            std::cout << "\nCamera mode " << cameraMode << " observing driver " << selectedDriver->getCarSurfaceId()
                      << std::endl;
            Mat carMat = selectedDriver->hasCollided() ? selectedDriver->getLastMatWithoutCollision()
                                                       : selectedDriver->getCar()->getTransform()->getMat();

            switch (cameraMode) {
                case CameraModes::FIRST_PERSON:
                    std::cout << "First person mode" << std::endl;
                    camera->transform(carMat * t_cam_offset);
                    break;
                case CameraModes::LOOK_AT_CLICKED_POINT:
                    std::cout << "Look at clicked point mode" << std::endl;
                    camera->transform(carMat * t_cam_offset);
                    camera->look_at(camera->e, clickedPoint, Vec(0, 1, 0));
                    break;
                case CameraModes::TOP_VIEW:
                    std::cout << "Top view mode" << std::endl;
                    t_cam_offset = Mat::FullPose(0, -M_PI_2, M_PI_2, 0, 20, 0, 1, 1, 1);
                    camera->transform(carMat * t_cam_offset);
                    break;
                default:
                    std::cout << "Free mode" << std::endl;
                    break;
            }
        }

        // ===========
        // Call for car drive control
        // ===========

        if (cameraMode != CameraModes::FREE && driveMode == CarDrivingMode::MANUAL) {
            selectedDriver->drive(event, scene);
        }

        for (auto it = botCars.begin(); it != botCars.end(); ++it) {
            Driver *botCar = *it;

            if (botCar != selectedDriver || driveMode == CarDrivingMode::AUTOPILOT)
                botCar->autoDrive(event, scene);
        }
    }
}

Driver* RoadTrip1::createDriverInstance(Material* material, Transform *carTf) {
    srand(time(NULL));
    double rand1 = double(rand()%101) / 100.0;
    std::string botCarIdSuffix = botCarSurfaceIdPrefix + std::to_string(carCount++);
    // Creates car
    CopCar *botCar = new CopCar(copCarLength, carMeshLocation, botCarIdSuffix, carTf,
                                material, scene);
    // Associates car to driver
    Driver *botDriver = new Driver(botCar, SDL_SCANCODE_KP_4,  SDL_SCANCODE_KP_6,  SDL_SCANCODE_KP_8,  SDL_SCANCODE_KP_5,  SDL_SCANCODE_KP_2,  SDL_SCANCODE_KP_7);
    botDriver->setDriveParams(M_PI/4,6,rand1*botCarMaxSpeed, botCarThrottleSteps, botCarMaxReverseSpeed, botCarReverseSteps,carDistanceToLeft);

    return botDriver;
}