//
// Created by cmdesktop on 24/12/19.
//

#include "GateRemote.hpp"

GateRemote::GateRemote(std::vector<RoadGate *> _roadGates) {
    roadGates = _roadGates;
}

GateRemote::~GateRemote() {
}

void GateRemote::OpenIfHit(RayTracer *renderer, SDL_Event &event, hit_rec &h_r, Scene* scene) {
    if(event.type == SDL_MOUSEBUTTONDOWN){
        std::cout << "Going to check if a gate was clicked" << std::endl;
        // Determines which object was clicked by the mouse pointer
        // First translates the pointer position to pixel coordinates
        float scaled_x, scaled_y;
        scaled_x = float(event.button.x) * float(renderer->camera->nx) / float(renderer->camera->viewport_width);
        scaled_y = float(renderer->getCamera()->ny) - float(event.button.y) * float(renderer->getCamera()->ny) / float(renderer->getCamera()->viewport_height);

        // Then obtains the index of the clicked surface
        int surface_index = renderer->get_visible_surface(scaled_x,scaled_y, h_r);

        // Gets the surface object corresponding to the index obtained
        Surface* surface = (*(scene->getSurfacesVector()))[surface_index];
        std::string surfaceId  = surface->id;

        std::cout << "\n\n Clicked Surface at (x,y)=("<< scaled_x << ", " << scaled_y <<") with index "<< surface_index << " and id " << surfaceId << " was clicked" << std::endl;

        if(StringUtils::stringContains(surfaceId, AppProperties::gatePostIdPrefix()) || StringUtils::stringContains(surfaceId, AppProperties::gateIdPrefix()) ) {

            std::cout << "\n\n\n\n Gate crossbar or pole was clicked!!!!!!!!!!!!!!! \n\n\n\n" << std::endl;

            bool postClicked = false;
            bool gateClicked = false;
            // Gets the RoadGate object that contains this surface
            std::vector<RoadGate *>::iterator it = std::find_if(roadGates.begin(), roadGates.end(),
                                                                [&surfaceId, &postClicked, &gateClicked](RoadGate *rg) {
                                                                    gateClicked = rg->getGateSurfaceId() == surfaceId;
                                                                    postClicked = rg->getPostSurfaceId() == surfaceId;
                                                                    return postClicked || gateClicked;
                                                                });
            RoadGate *selectedRoadGate = *it;

            // Rotates the gate
            // The gate can be rotated at the maximum of 90º
            // The more far from the gate beginning we click
            if (selectedRoadGate != NULL) {

                if (gateClicked) {

                    Transform *gateTransform = surface->getTransform();
                    Vec beginningPoint = Vec(gateTransform->getGlobalMat().data[0][3],
                                             gateTransform->getGlobalMat().data[1][3],
                                             gateTransform->getGlobalMat().data[2][3]);
                    double gateLength = selectedRoadGate->length;
                    Vec hitPoint = h_r.point;
                    double distance = (hitPoint - beginningPoint).norm();
                    double distanceProportion = distance / gateLength;
                    std::cout << " Distance " << distance << " Gate width " << gateLength << std::endl;
                    selectedRoadGate->rotateGate(M_PI_2 * distanceProportion);
                } else if (postClicked) {
                    selectedRoadGate->closeGate();
                }

            }
        }

    }
}

void GateRemote::closeAllGates() {
    for (auto it = roadGates.begin(); it != roadGates.end(); ++it){
        RoadGate* roadGate = *it;
        roadGate->closeGate();
    }
}