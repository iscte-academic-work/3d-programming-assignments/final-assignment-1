//
// Created by cmdesktop on 24/12/19.
//

#ifndef FINAL_ASSIGNMENT_GATEREMOTE_HPP
#define FINAL_ASSIGNMENT_GATEREMOTE_HPP

#include "../common/RoadGate.hpp"
#include "RayTracer.hpp"
#include "Surface.hpp"
#include "SDL2/SDL.h"
#include <algorithm>
#include <vector>
#include <cmath>
#include <tgmath.h>
#include "../helpers/AppProperties.hpp"
#include "../helpers/StringUtils.hpp"
//Controls the opening and closing operations of all gates in game
class GateRemote {
public:
    GateRemote(std::vector<RoadGate*> _roadGates);
    ~GateRemote();

    void OpenIfHit(RayTracer *renderer,SDL_Event &event, hit_rec &h_r, Scene* scene);
    void closeAllGates();
private:
    std::vector<RoadGate*> roadGates;
};


#endif //FINAL_ASSIGNMENT_GATEREMOTE_HPP
