//
// Created by cmdesktop on 29/11/19.
//

#include "Driver.hpp"

Driver::Driver(CopCar *_car) {
    car = _car;
}

Driver::Driver(CopCar * _car, SDL_Scancode _steerLeftKey, SDL_Scancode _steerRightKey, SDL_Scancode _throttleKey,
                SDL_Scancode _brakeKey, SDL_Scancode _reverseKey, SDL_Scancode _toggleSirenKey) {

    car = _car;
    steerLeftKey = _steerLeftKey;
    steerRightKey = _steerRightKey;
    throttleKey = _throttleKey;
    brakeKey = _brakeKey;
    reverseKey = _reverseKey;
    toggleSirenKey = _toggleSirenKey;
    std::cout<< "\n\n CREATE DRIVER 6 7 \n\n\n" << (_car == NULL);
    sirenOn = car->getSirenState();
    std::cout<< "\n\n CREATE DRIVER 6 \n\n\n";
}

Driver::~Driver() {
    delete [] car;
}

void Driver::setAutopilotParams(double _maxThrottle, int _throttleSteps, double _maxReverse, int _reverseSteps, double _distanceToLeft) {
    maxThrottle = _maxThrottle;
    throttleSteps = _throttleSteps;
    throttleStep = maxThrottle/throttleSteps;

    maxReverse = _maxReverse;
    reverseSteps = _reverseSteps;
    reverseStep = maxReverse/reverseSteps;

    distanceToLeft = _distanceToLeft;
}

void Driver::setDriveParams(double _steerMaxAngle, int _steerSteps, double _maxThrottle, int _throttleSteps, double _maxReverse, int _reverseSteps, double _distanceToLeft){
    steerMaxAngle = _steerMaxAngle;
    steerSteps = _steerSteps;
    steerStep = steerMaxAngle/steerSteps;

    setAutopilotParams(_maxThrottle, _throttleSteps, _maxReverse, _reverseSteps, _distanceToLeft); //just to reduce code
}

void Driver::autoDrive(SDL_Event &event, Scene* scene) {
    std::cout.precision(5);
    //First we get the original bounding box (without transforms)
    Box carBox = car->getOriginalBBox();
    Vec cbMin = carBox.min;
    cbMin.print();
    Vec cbMax = carBox.max;

    // Calculates the Bounding box centroid
    Vec bBoxCentroid = carBox.min + (carBox.max-carBox.min)/2;

    // ==================
    //  Origin vectors
    // ==================

    // Front face origin vectors
    Vec frontFaceCentroid = bBoxCentroid;
    frontFaceCentroid.z = cbMin.z; // Note: car is oriented to -z, that is why we use cbMin and not cbMax on here
    Vec frontFaceMiddleTop = frontFaceCentroid;
    frontFaceMiddleTop.y=cbMax.y;
    Vec frontFaceMiddleBottom = frontFaceMiddleTop;
    frontFaceMiddleBottom.y=cbMin.y+(cbMax.y-cbMin.y)*0.1; //This modifier elevates a little bit the sensor
    std::cout << "\n frontFaceMiddleBottom " ;
    frontFaceMiddleBottom.print();
    std::cout << std::endl;
    //frontFaceMiddleBottom.y = cbMin.y+0.5*(cbMax.y-cbMin.y);

    // Back face origin vectors
    Vec backFaceCentroid = bBoxCentroid;
    backFaceCentroid.z = cbMax.z - 0.15*(cbMax.z - cbMin.z);

    // Calculates origin points for rays
    // Left face centroid
    Vec leftFaceCentroid = bBoxCentroid;
    leftFaceCentroid.x = cbMin.x;

    // Right face centroid
    Vec rightFaceCentroid = bBoxCentroid;
    rightFaceCentroid.x = cbMax.x + cbMax.x*0.1;
    // Right face front centroid.
    Vec rightFaceMiddleLeft = rightFaceCentroid;
    Vec rightFaceMiddleRight  = rightFaceCentroid;
    rightFaceMiddleLeft.z = cbMin.z;
    rightFaceMiddleRight.z = cbMax.z;

    // =====================
    // Direction vectors
    // ======================
    Transform* carTransform = car->getTransform();
    Mat tf_with_trans = carTransform->getGlobalMat();
    Mat tf_no_trans = carTransform->getGlobalMat();
    tf_no_trans.data[0][3] = tf_no_trans.data[1][3] = tf_no_trans.data[2][3] = 0;
    Vec u = tf_no_trans * Vec(1,0,0); //right
    Vec v = tf_no_trans * Vec(0,1,0); //top
    Vec iu = tf_no_trans * Vec(-1,0,0); //left
    Vec w = tf_no_trans * Vec(0,0,-1); //front. it is -1, because car is spawning opposing to z

    // =========================
    // Applies current transform mat to origin vectors
    // =========================
    bBoxCentroid = tf_with_trans*bBoxCentroid;
    backFaceCentroid = tf_with_trans*backFaceCentroid;
    rightFaceCentroid = tf_with_trans*rightFaceCentroid;
    rightFaceMiddleLeft = tf_with_trans*rightFaceMiddleLeft;
    rightFaceMiddleRight = tf_with_trans*rightFaceMiddleRight;
    leftFaceCentroid = tf_with_trans*leftFaceCentroid;
    frontFaceCentroid = tf_with_trans*frontFaceCentroid;
    frontFaceMiddleTop = tf_with_trans*frontFaceMiddleTop;
    frontFaceMiddleBottom = tf_with_trans*frontFaceMiddleBottom;


    // To determine the ray that goes from the center of the right face of BBox and always point to the right
    // we need to calculate the origin vector (the center of the right face of BBox), and the direction
    Vec centerRightDirection = (rightFaceCentroid-bBoxCentroid).norm();

    // ======================
    // Ray creation
    // Ray name nomenclature
    // [direction: front, right, ...]_[bounding box face]_[location in face]
    // ======================

    // Creates the rays that point to the right of the car
    //Ray centerRightRay(rightFaceCentroid, u);
    Ray right_rightFace_centroid(rightFaceCentroid, u);
    Ray right_rightFace_middle_left(rightFaceMiddleLeft, u);
    Ray right_rightFace_middle_right(rightFaceMiddleRight, u);

    // Creates the rays that point to the left of the car
    Ray left_leftFace_centroid(leftFaceCentroid, iu);

    // Creates the rays that point to the front of the car
    Ray front_car_centroid(bBoxCentroid,w); //this ray comes from inside the car and points to the front
    Ray front_rightFace_middle_left(rightFaceMiddleLeft, w); //this ray points to the front of the car and its origin is at the middle left of the right face
    Ray front_frontFace_centroid(frontFaceCentroid, w);
    Ray front_frontFace_middle_top(frontFaceMiddleTop,w);
    Ray front_frontFace_middle_bottom(frontFaceMiddleBottom, w);
    Ray front_backFace_centroid(backFaceCentroid, w);

    // Creates the rays that poit to the top of the car
    Ray top_car_centroid(bBoxCentroid, v);

    hit_rec hr;
    if(setCollision) {
        std::cout << " \n EMBRACE YOURSELVES. CAR IS GOING TO ROLLOVER !!! \n";
        scene->hit(top_car_centroid, hr, 0, 300, NULL);
        //if floor is reached, movement is stopped
        bool reachedFloor = (hr.surface != NULL && StringUtils::stringContains(hr.surface->id, "sr_road"));

        if(!reachedFloor)
            car->rollOver();
    }
    else{

        double addToThrottle = throttleStep;
        bool slowDown = false;
        double distanceToCollision = 0;
        // =======================
        // Obstacle Collision check
        // =======================

        scene->hit(front_frontFace_middle_bottom, hr, 0, 300, NULL);
        if (hr.surface != NULL && StringUtils::stringContains(hr.surface->id, "boulder")) {
            double distanceToBoulder = (frontFaceMiddleBottom - hr.point).norm();
            setCollision = distanceToBoulder < 0.5;
        }
        // Second verification for collision. This time we are testing a ray that comes from inside the car
        // so if it detects any surface than this own car, it will start a collision
        scene->hit(front_car_centroid, hr, 0, 1, NULL);
        setCollision = setCollision || (!setCollision && hr.surface != NULL && !hr.surface->id.empty() && hr.surface->id != car->getSurfaceId());
        setCollision = !firstAutoDriveIteration && setCollision;
        if(hr.surface!=NULL){

            std::cout << "\n\n\n\n 2222222222222222\n\n WHAT THE HEEEELLLLLL " << hr.surface->id << "        " << car->getSurfaceId()<< std::endl;
        }

        std::cout << " Is car going to collide? " << setCollision;


        // ==================
        // Check if it is to slow down
        // ==================

        scene->hit(front_frontFace_centroid, hr, 0, 300, NULL);
        // Checks if another car is close
        if (hr.surface != NULL && StringUtils::stringContains(hr.surface->id, CopCar::carSurfaceIdPrefix())){
            std::cout << "\n SURFACE AUTO HIT HOPE IT IS ANOTHER CAR " << hr.surface->id;
            double distanceToOtherCar = (hr.point - frontFaceMiddleBottom).norm();
            if(distanceToOtherCar <= distanceToBrakeFromCar){
                slowDown = true;
                distanceToCollision = distanceToOtherCar;
            }
        }

        // Checks for a close road gate
        scene->hit(front_frontFace_middle_top, hr, 0, 300, NULL);

        // Checks if gate was hit and if was hit, determines the throttle to be added
        if (hr.surface != NULL && StringUtils::stringContains(hr.surface->id, AppProperties::gateIdPrefix())){
            double distanceToGate = (hr.point - frontFaceMiddleTop).norm();
            std::cout << "\n GAAAAATE Gate at a distance of " << distanceToGate << std::endl;

            if (distanceToGate <= distanceToBrakeFromGate) {
                slowDown = true;
                distanceToCollision = distanceToGate;
            }

        }

        addToThrottle = slowDown ? -(currentThrottle / distanceToCollision) : addToThrottle;


        // ===============
        // Driving params such as steering
        // ===============


        // To determine the steering angle we need to obtain the distance vectors between the right sensors
        // to the hit points. Then we calculate the cos between the magnitude of this last vector
        // in relation to the magnitude of the vector formed between the sensors origin points


        scene->hit(left_leftFace_centroid, hr, 0, 300, NULL);
        std::string railHitMiddleId = "";
        if (hr.surface != NULL) {
            railHitMiddleId = hr.surface->id;
            std::cout << "SURFACE AUTO HIT " << hr.surface->id << " at point ";
            hr.point.print();
            std::cout << std::endl;
        }
        Vec railMiddleLeftSensorHitPoint = hr.point;

        scene->hit(right_rightFace_middle_left, hr, 0, 300, NULL);
        std::string railHitFrontId = "";
        if (hr.surface != NULL) {
            railHitFrontId = hr.surface->id;
            std::cout << "SURFACE AUTO HIT " << hr.surface->id << " at point ";
            hr.point.print();
            std::cout << std::endl;
        }
        Vec railFrontSensorHitPoint = hr.point;
        scene->hit(right_rightFace_middle_right, hr, 0, 300, NULL);
        std::string railHitBackId = "";
        if (hr.surface != NULL) {
            railHitBackId = hr.surface->id;
            std::cout << "SURFACE AUTO HIT " << hr.surface->id << " at point ";
            hr.point.print();
            std::cout << std::endl;
        }
        Vec railBackSensorHitPoint = hr.point;

        double steerAngle = previousAddSteer;
        double sideAdjustment = 0;

        // Determines steering
        // Checks if both hit surfaces are protection rails
        if (StringUtils::stringContains(railHitFrontId, railSurfaceIdPrefix) &&
            StringUtils::stringContains(railHitBackId, railSurfaceIdPrefix)) {

            Vec railSensorDistanceVec =
                    rightFaceMiddleLeft - rightFaceMiddleRight; // distance between sensor origins

            std::cout << "\n railSensorDistanceVec  ";
            railSensorDistanceVec.print();

            // calculates the steering
            Vec distanceRightFrontSensorHitPoint = railFrontSensorHitPoint - rightFaceMiddleLeft;
            std::cout << "\n distanceRightFrontSensorHitPoint  " << distanceRightFrontSensorHitPoint.norm() << " ";
            distanceRightFrontSensorHitPoint.print();
            Vec distanceRightBackSensorHitPoint = railBackSensorHitPoint - rightFaceMiddleRight;
            std::cout << "\n distanceRightBackSensorHitPoint  " << distanceRightBackSensorHitPoint.norm() << " ";
            distanceRightBackSensorHitPoint.print();
            std::cout << std::endl;
            steerAngle = atan((distanceRightBackSensorHitPoint.norm() - distanceRightFrontSensorHitPoint.norm()) /
                              railSensorDistanceVec.norm());
            std::cout << "\n previous added steer" << previousAddSteer << "\n steerAngle  " << steerAngle
                      << std::endl;

            //steerAngle = steerAngle - currentSteerAngle;
        }

        // Determines the adjustment to guarantee that the car stays at the the right lane
        if (StringUtils::stringContains(railHitMiddleId, railSurfaceIdPrefix)) {
            double middleSensorLeftHitPointDistance = (leftFaceCentroid -
                                                       railMiddleLeftSensorHitPoint).norm(); // Calculates the distance between centroid and hitpoint of the right face centroid sensor
            sideAdjustment = distanceToLeft - middleSensorLeftHitPointDistance;
        }

        // Calls for car control
        previousAddSteer = steerAngle;
        double steerMax = steerAngle >= 0 ? steerMaxAngle : 0;
        double steerMin = steerAngle >= 0 ? 0 : -steerMaxAngle;
        controlCar(steerAngle, addToThrottle, steerMax, steerMin, sideAdjustment, slowDown);

    }

    if(!setCollision)
        lastMatWithoutCollision = car->getTransform()->getMat();
    firstAutoDriveIteration = false;
}


// This function shall be called during render phase
// TODO: There is a bug that gets the wheel stuck to the left sometimes
void Driver::drive(SDL_Event &event, Scene* scene) {
    std::cout << "Going to drive " << std::endl;
    hit_rec hr;
    // ========================
    // Collision check
    // in contrary to auto drive, just checks if the centroid hits anything
    // than the car. Also, checks if gate is hit (using top vector)
    // =========================
    //First we get the original bounding box (without transforms)
    Box carBox = car->getOriginalBBox();
    Vec cbMin = carBox.min;
    cbMin.print();
    Vec cbMax = carBox.max;

    double carLength = std::abs(cbMax.z-cbMin.z);
    // Calculates the Bounding box centroid
    Vec bBoxCentroid = carBox.min + (carBox.max-carBox.min)/2;

    // Origin vectors
    Vec frontFaceMiddleTop = bBoxCentroid;
    frontFaceMiddleTop.y=cbMax.y;
    frontFaceMiddleTop.z=cbMin.z;

    // =====================
    // Direction vectors
    // ======================
    Transform* carTransform = car->getTransform();
    Mat tf_with_trans = carTransform->getGlobalMat();
    Mat tf_no_trans = carTransform->getGlobalMat();
    // For the direction vectors, the translation part is deleted
    tf_no_trans.data[0][3] = tf_no_trans.data[1][3] = tf_no_trans.data[2][3] = 0;
    Vec w = tf_no_trans * Vec(0,0,-1); // "front"
    Vec v = tf_no_trans * Vec(0,1,0); // top

    // =========================
    // Applies current transform mat to origin vectors
    // =========================
    bBoxCentroid = tf_with_trans*bBoxCentroid;
    frontFaceMiddleTop = tf_with_trans*frontFaceMiddleTop;

    // ======================
    // Ray creation
    // Ray name nomenclature
    // [direction: front, right, ...]_[bounding box face]_[location in face]
    // ======================
    Ray front_car_centroid(bBoxCentroid, w);
    Ray front_frontFace_middle_top(frontFaceMiddleTop, w);
    Ray top_car_centroid(bBoxCentroid, v);

    // If setCollision is set to true, a collision sequence will start
    if(setCollision) {
        std::cout << " \n EMBRACE YOURSELVES. CAR IS GOING TO ROLLOVER !!! \n";
        scene->hit(top_car_centroid, hr, 0, 300, NULL);
        //if floor is reached, movement is stopped
        bool reachedFloor = (hr.surface != NULL && StringUtils::stringContains(hr.surface->id, "sr_road"));

        if(!reachedFloor)
            car->rollOver();
    }
    else{

        // Verifies if any obstacle is hit
        scene->hit(front_frontFace_middle_top, hr, 0, 300, NULL);
        // Mostly, checks if a gate is close, and if too close, sets a collision
        if (hr.surface != NULL && !hr.surface->id.empty() && hr.surface->id != car->getSurfaceId()){
            double distanceToObstacle = (frontFaceMiddleTop - hr.point).norm();
            setCollision = distanceToObstacle < carLength / 3;

        }
        scene->hit(front_car_centroid, hr, 0, 300, NULL);
        setCollision = setCollision || (!setCollision && hr.surface != NULL && !hr.surface->id.empty() && hr.surface->id != car->getSurfaceId());




        // This variables will save the amount to add to currentSteer and currentThrottle
        double addToSteer = 0;
        double addToThrottle = 0;
        double steerMax;
        double steerMin;
        bool isBraking = false;
        // Determines the driving params based on the pressed keys
        if(event.type == SDL_KEYDOWN) {
            if(event.button.button == steerLeftKey){
                addToSteer = steerStep;
                steerMin = 0;
                steerMax = steerMaxAngle;
            }
            if(event.button.button == steerRightKey){
                addToSteer = -steerStep;
                steerMin = -steerMaxAngle;
                steerMax = 0;
            }
            if(event.button.button == throttleKey){
                addToThrottle = throttleStep;
            }
            if(event.button.button == reverseKey)
                addToThrottle = -reverseStep;

            if(event.button.button == brakeKey) {
                isBraking = true;
                addToThrottle = currentThrottle  == 0 ? 0 : (currentThrottle > 0 ? -throttleStep : reverseStep);
            }

            if(event.button.button == toggleSirenKey)
                sirenOn = !sirenOn;
        }
        if(!setCollision)
            lastMatWithoutCollision = car->getTransform()->getMat();
        controlCar(addToSteer, addToThrottle, steerMax, steerMin,0, isBraking);
    }

}

void Driver::controlCar(double addToSteer, double addToThrottle, double steerMax, double steerMin, double sideAdjustment, bool isBraking) {
    std::cout << "Previous steer " << currentSteerAngle << " previous throttle " << currentThrottle << std::endl;
    std::cout << "Going to add to steer " << addToSteer << " add to throttle " << addToThrottle << std::endl;
    std::cout << "Going to adjust side to " << sideAdjustment << std::endl;
    currentSteerAngle += addToSteer;
    currentSteerAngle = MathUtils::clamp(currentSteerAngle, steerMin, steerMax);

    currentThrottle += addToThrottle;

    if(isBraking){
        currentThrottle = MathUtils::clamp(currentThrottle, addToThrottle >= 0 ? -maxReverse : 0, addToThrottle>=0 ?  0 : maxThrottle);
        std::cout << " \n\n " << addToThrottle << " BRAAAAAAAAAAAAAKING Braking to" << currentThrottle << std::endl;
    }
    else
        currentThrottle = MathUtils::clamp(currentThrottle, -maxReverse, maxThrottle);

    std::cout << "Current speed " << currentThrottle << " Max speed car can reach " << maxThrottle << std::endl;
    std::cout << "Current steer " << currentSteerAngle << " current throttle " << currentThrottle << std::endl;


    car->driveCar(currentSteerAngle, -currentThrottle,sideAdjustment); //the throttle is inverted because car is reversed in z

    car->turnSirens(sirenOn);
}

void Driver::setDistanceToBrakeFromGate(double _distanceToBrakeFromGate) {
    distanceToBrakeFromGate = _distanceToBrakeFromGate;
}