//
// Created by cmdesktop on 29/11/19.
//

#ifndef FINAL_ASSIGNMENT_DRIVER_HPP
#define FINAL_ASSIGNMENT_DRIVER_HPP

#include "Scene.hpp"
#include "SDL2/SDL.h"
#include "../common/CopCar.hpp"
#include "../helpers/MathUtils.hpp"
#include "../helpers/StringUtils.hpp"
#include "Ray.hpp"
#include <math.h>
#include "../helpers/AppProperties.hpp"

class Driver {
public:
    /*!
     * Constructor to be called for autopilot control
     * @param _car
     */
    Driver(CopCar * _car);
    /*!
     * Constructor to be called for manual control of the car
     * @param _car
     * @param _steerLeftKey
     * @param _steerRightKey
     * @param _throttleKey
     * @param _brakeKey
     * @param _reverseKey
     * @param _toggleSirenKey
     */
    Driver(CopCar * _car, SDL_Scancode _steerLeftKey, SDL_Scancode _steerRightKey, SDL_Scancode _throttleKey, SDL_Scancode _brakeKey, SDL_Scancode _reverseKey, SDL_Scancode _toggleSirenKey);
    ~Driver();

    void drive(SDL_Event &event, Scene* scene);
    void autoDrive(SDL_Event &event, Scene* scene);
    void setDriveParams(double _steerMaxAngle, int _steerSteps, double _maxThrottle, int _throttleSteps, double _maxReverse, int _reverseSteps, double distanceToLeft);
    void setAutopilotParams(double _maxThrottle, int _throttleSteps, double _maxReverse, int _reverseSteps, double distanceToLeft);
    void setDistanceToBrakeFromGate(double distanceToBrakeFromGate);
    bool hasCollided(){
        return setCollision;
    }

    std::string getCarSurfaceId(){
        return car->getSurfaceId();
    }

    void increaseMaxThrottle(double increaseValue){
        maxThrottle = MathUtils::clamp(maxThrottle + increaseValue, 0, 15);
    }

    CopCar* getCar(){ return car;}
    Mat getLastMatWithoutCollision(){ return lastMatWithoutCollision;}
private:
    Mat lastMatWithoutCollision = Mat();
    double firstAutoDriveIteration = true;
    double setCollision = false;
    double distanceToBrakeFromGate = 4;
    double distanceToBrakeFromCar = 5;
    double distanceToLeft = 1.75; // Saves the distance we must guarantee to the left
    const std::string railSurfaceIdPrefix = "sr_protection_rail";
    double previousAddSteer = 0;
    double steerMaxAngle = M_PI/4; // Maximum angle the wheel can take to left or right
    int steerSteps = 4;       // Number of steps the wheel takes from 0 to +- steerMaxAngle
    double steerStep = steerMaxAngle/steerSteps;

    double maxThrottle = 1;
    int throttleSteps = 5;
    double throttleStep = maxThrottle/throttleSteps;

    double maxReverse = 1;
    int reverseSteps = 5;
    double reverseStep = maxReverse/reverseSteps;

    double currentThrottle=0;
    double currentSteerAngle=0;

    bool sirenOn = false;

    SDL_Scancode steerLeftKey, steerRightKey, throttleKey, brakeKey, reverseKey, toggleSirenKey;
    CopCar* car;

    void controlCar(double addToSteer, double addToThrottle, double steerMax, double steerMin, double sideAdjustment, bool isBraking);
};


#endif //FINAL_ASSIGNMENT_DRIVER_HPP
