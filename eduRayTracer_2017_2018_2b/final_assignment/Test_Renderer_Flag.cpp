#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <math.h>


#include "Transform.hpp"
#include "Mesh.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "Scene.hpp"
#include "RayTracer.hpp"
#include "common/Cuboid.hpp"
#include "common/WavingFlag.hpp"
#include "utils.hpp"
#include "Material.hpp"
#include "Box.hpp"
#include "Mat.hpp"
#include "Vec.hpp"






Camera* camera;

WavingFlag* flag;

Vec wind;
double windStrength=3.0;

double tmp=0.0;

double currentSine;
double sangle=0.0;
bool up=true;
bool down=false;
double radian=M_PI/180;
double crit_angle=sin(5*windStrength*radian);

void redraw(RayTracer *renderer, SDL_Event &event){
		flag->animateFlag();
	
	
	
}

int main(int argc, char **){
	
	Scene * scene = new Scene();
	
	Material* mTex1 = new Material(Vec(1,1,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, true, false, NULL);
	Material* mTex2 = new Material(Vec(0,0,1), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, false, false,NULL);
	Material* mTex3 = new Material(Vec(1,0,0), Vec(1,1,1), Vec(0.1,0.1,0.1), 1, 1, 1.0, true, false, false, false,"textures/checkeredFlag.ppm");
    
    flag= new WavingFlag(mTex1, mTex2, mTex3,windStrength);
    
    wind=Vec(1.0, 0.0, 1.0);
    std::cout << "flag Created" << std::endl;
    camera = new Camera(-1, 1, 1, -1, 1.5, 100, 100, 400, 400);
    camera->look_at(Vec(0,0,10),Vec(0,-2,0),Vec(0,1,0));
     
    Transform* tf_flag = new Transform(0.0, 0.0, 0.0, 0, -2.0, 0, 1, 1, 1, 1);
	
    
	
	
    //CornellBoxTextured(scene, new Transform(0.0, 0.0, 0.0, 0, 0, 0, 1, 1, 1, 1));
    
   
   
    LightBulb(scene, Vec(0,4.5,0), Vec(1,1,1), 1, NULL);
    
    flag->createFlag(scene, tf_flag, wind);
    
    scene->getRootTransform()->addChild(tf_flag);
    
	// Create a RayTracer to render the scene from the camera location
    RayTracer renderer(camera, scene);
    
    // render the scene and react to keyboard events
    main_loop(renderer, &redraw, true, true, true);
}


