//
// Created by abeltenera on 08/01/20.
//

#ifndef FINAL_ASSIGNMENT_STRINGUTILS_HPP
#define FINAL_ASSIGNMENT_STRINGUTILS_HPP
// Include the string library
#include <string>
#include<iostream>
#include "AppProperties.hpp"
class StringUtils {
public:
    static bool stringContains(std::string s1, std::string s2);
};


#endif //FINAL_ASSIGNMENT_STRINGUTILS_HPP
