//
// Created by abeltenera on 11/01/20.
//

#ifndef FINAL_ASSIGNMENT_APPPROPERTIES_HPP
#define FINAL_ASSIGNMENT_APPPROPERTIES_HPP
#include <iostream>
class AppProperties {
public:
    static int getTransformId();
    static int NEXT_TRANSFORM_ID;

    static std::string gateIdPrefix(){
        return "sr_gate_crossbar";
    }
    static std::string gatePostIdPrefix(){
        return "sr_gate_pole";
    }

};


#endif //FINAL_ASSIGNMENT_APPPROPERTIES_HPP
