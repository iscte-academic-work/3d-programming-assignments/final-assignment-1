//
// Created by abeltenera on 08/01/20.
//

#include "StringUtils.hpp"

bool StringUtils::stringContains(std::string s1, std::string s2) {
    bool result = false;
    if (s1.find(s2) != std::string::npos) {
        result = true;
    }
    return result;
}