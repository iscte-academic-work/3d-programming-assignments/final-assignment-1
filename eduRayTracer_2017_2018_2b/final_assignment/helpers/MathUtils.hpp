//
// Created by cmdesktop on 30/11/19.
//

#ifndef FINAL_ASSIGNMENT_MATHUTILS_HPP
#define FINAL_ASSIGNMENT_MATHUTILS_HPP

#include "Mesh.hpp"
#include "Mat.hpp"
#include "AppProperties.hpp"

enum AXIS { X, Y, Z};
class MathUtils {
public:
    static double clamp(double value, double min, double max);

    static Mat resizeMeshMat(Mesh* mesh, double finalSize, AXIS axis);

};


#endif //FINAL_ASSIGNMENT_MATHUTILS_HPP
