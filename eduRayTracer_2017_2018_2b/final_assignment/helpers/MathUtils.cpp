//
// Created by cmdesktop on 30/11/19.
//

#include "MathUtils.hpp"

/*!
 * Returns the result of the clamp operation of a value.
 * @param value value to be clamped
 * @param min minimum value to be returned
 * @param max maximum value to be returned
 * @return result of the clamp operation of param value
 */
double MathUtils::clamp(double value, double min, double max) {
    return value < min ? min : (value > max ? max : value);
}

/*!
 * Creates a scaling matrix for a mesh, maintaining the aspect ratio in all dimensions.
 * @param mesh
 * @param finalSize the intended size
 * @param axis axis to be used to compute
 * @return scaling matrix. The scaling will be equal for all axis
 */
Mat MathUtils::resizeMeshMat(Mesh *mesh, double finalSize, AXIS axis) {

    Box box = mesh->getBBox();
    Vec boxMin = box.min;
    Vec boxMax = box.max;
    double currentSize;
    switch (axis){
        case X:
            currentSize = std::abs(boxMax.x - boxMin.x);
            break;
        case Y:
            currentSize = std::abs(boxMax.y - boxMin.y);
            break;
        case Z:
            currentSize = std::abs(boxMax.z - boxMin.z);
            break;
    }

    double scalingFactor = finalSize / currentSize;
    // Scale matrix that will resize the mesh to the intended length
    Mat scaleMat = Mat::Scale(scalingFactor,scalingFactor,scalingFactor);
    return scaleMat;
}