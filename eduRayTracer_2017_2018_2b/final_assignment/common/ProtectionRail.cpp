//
// Created by abeltenera on 11/11/19.
//

#include "ProtectionRail.hpp"

ProtectionRail::ProtectionRail(double _w, double _h, double _l) : ProtectionRail(_w,_h, _l, new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL)){}

ProtectionRail::ProtectionRail(double _w, double _h, double _l, Material* _protectionRailMaterial) {
    width=_w;
    height=_h;
    length=_l;
    protectionRailMaterial = _protectionRailMaterial;
}


void ProtectionRail::addProtectionRailToScene(Transform *_t, Scene *_scene) {

    Cuboid* pillar = new Cuboid(width, height/2, width, protectionRailMaterial, protectionRailMaterial,
            protectionRailMaterial, protectionRailMaterial, protectionRailMaterial, protectionRailMaterial); // Defines the pillar
    Cuboid* crossbar = new Cuboid(width,height/2, length, protectionRailMaterial, protectionRailMaterial,
            protectionRailMaterial, protectionRailMaterial, protectionRailMaterial, protectionRailMaterial); // Defines the crossbar

    // Pillar initial transforms
    Mat p1TfMat = Mat::Translate(0, -height/4, 0.4*length);
    Mat p2TfMat = Mat::Translate(0, -height/4, -0.4*length);

    // Crossbar initial transform
    Mat tTfMat = Mat::Translate(0,height/4,0);
    pillar->setSurfaceId(surfaceId);
    crossbar->setSurfaceId(surfaceId);
    pillar->addCuboidToScene(p1TfMat, _t, _scene);
    pillar->addCuboidToScene(p2TfMat, _t, _scene);
    crossbar->addCuboidToScene(tTfMat, _t, _scene);

}

void ProtectionRail::setSurfaceId(std::string _surfaceId) {
    surfaceId =_surfaceId;
}