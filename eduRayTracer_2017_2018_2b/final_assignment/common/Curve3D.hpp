//
// Created by abeltenera on 10/11/19.
//

#ifndef FINAL_ASSIGNMENT_CURVE3D_HPP
#define FINAL_ASSIGNMENT_CURVE3D_HPP

#include "Material.hpp"
#include "Transform.hpp"
#include "Scene.hpp"
#include "../helpers/AppProperties.hpp"

/*!
 * Note: In the current phase this curve only creates 90 degree curves.
 * This class is similar to cuboid. Imagine it as a cuboid curved to the sides
 */
class Curve3D {

public:

    std::string surfaceId="";

    //! Note about the dimensions: this dimensions define the curve dimensions if it was straight.
    //! Width of the Curve3D
    double width;
    //! Length of the Curve3D. Corresponds to the size of the outer curve
    double length;
    //! Height of the Curve3D
    double height;

    //! startingAngle and endingAngle determine where the curve starts and ends in degrees
    double startingAngle;
    double endingAngle;

    //! How many divisions to be defined along the curve length
    int numberOfDivisions;

    Material* topSurfaceMat;
    Material* bottomSurfaceMat;
    Material* leftSurfaceMat;
    Material* rightSurfaceMat;
    Material* frontSurfaceMat;
    Material* backSurfaceMat;

    /*!
     * @param _w width
     * @param _l length
     * @param _h height
    */
    Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions);
    Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions, Material* _topSurfaceMat);
    Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions, Material* _topSurfaceMat,  Material* _bottomSurfaceMat,  Material* _leftSurfaceMat,  Material* _rightSurfaceMat,  Material* _frontSurfaceMat, Material* _backSurfaceMat);
    ~Curve3D();

    /*!
     *
     * @param _t transform
     * @param _m height
     * @param _scene scene to be applied
     * @param _jt Flag to indicate if the material is just to be applied to the top of the cuboid
    */
    void addCurve3DToScene(Transform* _t, Scene* _scene, bool _distortTexture);

    void setSurfaceId(std::string _surfaceId);

    double getOuterRay();
    double getInnerRay();
    double getAmplitude();

private:
    Vec* topSurfacePoints;
    // Defines the texture points to be applied to the top and bottom surface, when not in distort mode
    // Consider this as the sticker mode points
    Vec* stickerModeTexturePoints;
    // Defines the texture points to be applied to the top and bottom surface, when in distort mode.
    // For the inner and outer surface, this will always be applied
    Vec* distortModeTexturePoints;
    Vec* bottomSurfacePoints;
    Vec* outerSurfacePoints;
    Vec* innerSurfacePoints;
    Vec* frontSurfacePoints;
    Vec* backSurfacePoints;
    double innerRay;
    double outerRay;
    double amplitude;
    double amplitudeStep;
    double textureOuterRay;
    double textureInnerRay;

    void createTriangles(Scene* scene, Material *m, Transform *tf, Vec* surfacePoints, Vec* texturePoints, int numberOfDivisions, int currentDivision);
    // TODO : This was copied from lab. Create your own!
    void createTriangle(Scene* scene, Material *m, Transform *tf, Vec v1, Vec v2, Vec v3, Vec vt1, Vec vt2, Vec vt3);
};


#endif //FINAL_ASSIGNMENT_CURVE3D_HPP
