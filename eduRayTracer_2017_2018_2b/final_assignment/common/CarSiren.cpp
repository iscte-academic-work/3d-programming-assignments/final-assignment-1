//
// Created by abeltenera on 26/11/19.
//

#include "CarSiren.hpp"
CarSiren::CarSiren(double _size, double _decay, double sphereScalingFactor, Transform *_tf_siren, Material* containerMaterial, Material* bulbMaterial, Scene* _scene, std::string _surfaceId, int _transformId) {

    tf_siren =_tf_siren;
    size = _size;
    surfaceId = _surfaceId;

    sirenContainer = new Cuboid(size, size, size, containerMaterial,containerMaterial,containerMaterial,containerMaterial,containerMaterial,containerMaterial);
    sirenContainer->setDrawTunnelMode(true); //The front and back surface of the cuboid won't be drawn
    sirenContainer->setSurfaceId(surfaceId);
    sirenContainer->addCuboidToScene(tf_siren, _scene);
    double bulbRadius = (size)*sphereScalingFactor;
    lightBulb = new Sphere(Vec(0,0,0), bulbRadius, bulbMaterial, false);
    _scene->addSurface(lightBulb, _surfaceId);
    lightBulb->setTransform(tf_siren);
    light = new Light (lightBulb->vertexes[0]->local_o, lightBulb->m->getCr(), _decay, lightBulb);
    _scene->addLight(light);
    light->setTransform(tf_siren);

}

CarSiren::~CarSiren() {
    delete [] tf_siren;
    delete [] light;
    delete [] lightBulb;
    delete [] sirenContainer;
}

void CarSiren::setLightState(bool state) {
    lightState = state;
    light->on = lightState;
}

void CarSiren::rotateSiren(double angleStep) {

    Mat rotation = Mat::RotY(angleStep);

    tf_siren->setMat(tf_siren->getMat() * rotation);
}