//
// Created by abeltenera on 21/12/19.
//

#include "RoadGate.hpp"

RoadGate::RoadGate(std::string _baseSurfaceId, double _w, double _h, double _l, Material *_pm, Material *_gm, Transform *_t, Scene *_scene,
                   bool _distortTexture) {
    width = _w;
    height = _h;
    length = _l;

    postMaterial = _pm;
    gateMaterial = _gm;

    baseSurfaceId = _baseSurfaceId;

    Cuboid* post = new Cuboid(width, height, width, postMaterial, postMaterial, postMaterial, postMaterial, postMaterial, postMaterial);
    Cuboid* gate = new Cuboid(length, width, width, gateMaterial, gateMaterial, gateMaterial, gateMaterial, gateMaterial, gateMaterial);
    post->setSurfaceId(AppProperties::gatePostIdPrefix()+_baseSurfaceId);
    gate->setSurfaceId(AppProperties::gateIdPrefix()+_baseSurfaceId);

    //postTransform = new Transform(Mat::Translate(-length/2+width/2,height/2,0),_transformId);
    //gateTransform = new Transform(Mat::Translate(length/2-width/2,height/2,0),_transformId);

    // O PROBLEMA É QUE ESTAS TRANSFORMS TÊM DE SER ADICIONADAS NO FIM

    //postTransform = new Transform(Mat::Translate(0,0,0),AppProperties::getTransformId());
    postTransform=_t;
    gateTransform = new Transform(Mat::Translate(0,0,0),AppProperties::getTransformId());

    post->addCuboidToScene(Mat::Translate(0,-height/2,0),_t, _scene);
    gate->addCuboidToScene(Mat::Translate(length/2,0,0),gateTransform, _scene);
    postTransform->addChild(gateTransform);
    currentAngle = 0;
}

RoadGate::~RoadGate() {
    delete [] postTransform;
    delete [] gateTransform;
}

void RoadGate::rotateGate(double angle) {

    // Prevents gate opening to an angle bigger than max angle
    if(currentAngle + angle > MAX_ANGLE)
        angle = MathUtils::clamp(MAX_ANGLE - currentAngle,0, MAX_ANGLE);

    if(angle>0) {
        gateTransform->setMat(gateTransform->getMat() * Mat::RotZ(angle));
        postTransform->update_global_tf();
        currentAngle += angle;
    }
}

void RoadGate::closeGate() {
    gateTransform->setMat(gateTransform->getMat() * Mat::RotZ(-currentAngle));
    postTransform->update_global_tf();
    currentAngle=0;
}

std::string RoadGate::getGateSurfaceId() {
    return AppProperties::gateIdPrefix()+baseSurfaceId;
}

std::string RoadGate::getPostSurfaceId() {
    return AppProperties::gatePostIdPrefix()+baseSurfaceId;
}
