//
// Created by abeltenera on 21/12/19.
//

#ifndef FINAL_ASSIGNMENT_ROADGATE_HPP
#define FINAL_ASSIGNMENT_ROADGATE_HPP

#include "Cuboid.hpp"
#include "Transform.hpp"
#include "Mat.hpp"
#include "../helpers/MathUtils.hpp"
#include "../helpers/AppProperties.hpp"
/*!
 *    ||||||||||||||||||||||||||||||||||||||||||
 *    ||
 *    ||
 *    ||
 *    ||
 *
 */

class RoadGate {
public:
    // width defines the thickness of each part of the road gate
    // length defines the whole width
    double width, height, length;

    Material* postMaterial;
    Material* gateMaterial;

    RoadGate(std::string _baseSurfaceId, double _w, double _h, double _l, Material* _pm, Material* _gm, Transform* _t, Scene* _scene, bool _distortTexture);
    ~RoadGate();
    void rotateGate(double angle);
    void closeGate();
    std::string getGateSurfaceId();
    std::string getPostSurfaceId();

private:
    const double MAX_ANGLE = M_PI_2;
    double currentAngle;
    std::string baseSurfaceId;
    Transform* postTransform;
    Transform* gateTransform;
    Scene* scene;

};


#endif //FINAL_ASSIGNMENT_ROADGATE_HPP
