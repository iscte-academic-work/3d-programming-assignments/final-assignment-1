//
// Created by abeltenera on 02/01/20.
//

#include "SRoad.hpp"

SRoad::SRoad(double _rw, double _rh, double _rl, double _prw, double _prh, int _numberOfRailsOuterCurve, double _sw, double _sh,
            int _numberOfDivisions, Material *_roadTopMaterial,
            Material *_sidewalkTopMaterial, Material *_protectionRailMaterial) {
    rw = _rw;
    rh =_rh;
    rl = _rl;
    prw = _prw;
    prh =_prh;
    sw = _sw;
    sh =_sh;
    startingAngle = 0;
    endingAngle = M_PI_2;
    numberOfRailsOuterCurve = _numberOfRailsOuterCurve;
    numberOfDivisions = _numberOfDivisions;
    roadTopMaterial = _roadTopMaterial;
    sidewalkTopMaterial=_sidewalkTopMaterial;
    protectionRailMaterial=_protectionRailMaterial;
    amplitude = endingAngle - startingAngle;
    outerRailAmplitudeStep= amplitude / numberOfRailsOuterCurve;


    // Radius of the whole road (including sidewalks)
    outerRadius = rl/M_PI_2 + sw;
    innerRadius = outerRadius-rw-2*sw;
    roadWidth = rw+2*sw;
    zDisplacement = outerRadius - roadWidth/2;
}


SRoad::~SRoad() {
    delete [] roadTopMaterial;
    delete [] sidewalkTopMaterial;
    delete [] protectionRailMaterial;
}

void SRoad::addSRoadToScene(Transform *_t, Scene *_scene, bool _distortTexture, bool loadDecorations,
                            double decorationsHeight, const char *treeMeshLocation, const char *streetLampMeshLocation,
                            Material *materialTree, Material *materialLamp) {

    int id = _t->id;
    // Intantiates the helper object to create curves
    CurvedRoad* curvedRoad = new CurvedRoad(rw,rh, rl, prw, prh, numberOfRailsOuterCurve, sw,sh, startingAngle,
                                            endingAngle,numberOfDivisions, roadTopMaterial, sidewalkTopMaterial,
                                            protectionRailMaterial);


    curvedRoad->setTreeDensity(treeDensity);
    curvedRoad->setStreetLampDensity((streetLampDensity));
    Transform* tf0 = new Transform(Mat::Translate(0,0,0), AppProperties::getTransformId());
    Transform* tf1 = new Transform(Mat::Translate(0,0,zDisplacement), AppProperties::getTransformId());
    Transform* tf2 = new Transform(Mat::Translate(0,0,zDisplacement)*Mat::RotY(M_PI/2), AppProperties::getTransformId());
    Transform* tf3 = new Transform(Mat::Translate(0,0,-zDisplacement)*Mat::RotY(-M_PI/2), AppProperties::getTransformId());
    Transform* tf4 = new Transform(Mat::Translate(0,0,-zDisplacement)*Mat::RotY(-M_PI), AppProperties::getTransformId());

    curvedRoad->setSurfaceIdSuffix(surfaceIdSuffix);
    curvedRoad->addCurvedRoadToScene(tf1, _scene,_distortTexture);
    curvedRoad->addCurvedRoadToScene(tf2, _scene,_distortTexture);
    curvedRoad->addCurvedRoadToScene(tf3, _scene,_distortTexture);
    curvedRoad->addCurvedRoadToScene(tf4, _scene,_distortTexture);

    if(loadDecorations){
        curvedRoad->loadDecorations(decorationsHeight,treeMeshLocation,streetLampMeshLocation,materialTree, materialLamp, tf1, _scene);
        curvedRoad->loadDecorations(decorationsHeight,treeMeshLocation,streetLampMeshLocation,materialTree, materialLamp, tf2, _scene);
        curvedRoad->loadDecorations(decorationsHeight,treeMeshLocation,streetLampMeshLocation,materialTree, materialLamp, tf3, _scene);
        curvedRoad->loadDecorations(decorationsHeight,treeMeshLocation,streetLampMeshLocation,materialTree, materialLamp, tf4, _scene);
    }

    _t->addChild(tf0);
    tf0->addChild(tf1);
    tf0->addChild(tf2);
    tf0->addChild(tf3);
    tf0->addChild(tf4);

}

void SRoad::setSurfaceIdSuffix(std::string _surfaceIdSuffix) {surfaceIdSuffix = _surfaceIdSuffix;}

double SRoad::getSRoadBoxLength() {
    return 2*(outerRadius + zDisplacement);
}

double SRoad::getSRoadBoxWidth() {
    return 2*outerRadius;
}