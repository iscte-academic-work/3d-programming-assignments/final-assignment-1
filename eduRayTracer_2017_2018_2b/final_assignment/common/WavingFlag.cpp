
#include "WavingFlag.hpp"

WavingFlag::WavingFlag(Material* m1, Material* m2, Material* m3, double _windStrength){


    m_base=m1;
    m_pole=m2;
    m_flag=m3;

    tf_base=new Transform();
    tf_lowerPole=new Transform();
    tf_midPole=new Transform();
    tf_upperPole=new Transform();
    tf_rFlag=new Transform();
    tf_mFlag=new Transform();
    tf_lFlag=new Transform();

    windStrength=_windStrength;

    tmp=0.0;

    currentSine=0.0;
    sangle=0.0;
    up=true;
    down=false;
    radian=M_PI/180;
    crit_angle=sin(5*windStrength*radian);

}

void WavingFlag::createFlag(Scene* scene, Transform* _tf, Vec w){
    tf = _tf;
    Vec posFlag=Vec(tf->getMat().data[0][3],tf->getMat().data[1][3],tf->getMat().data[2][3]);
    Vec flagOrientation=posFlag+w;
    Mat mat=Mat::LookAt(posFlag, (flagOrientation), Vec(0,1,0));

    tf->setMat(tf->getMat()* mat) ;
    tf_base->setMat(tf->getMat());

    Cuboid* base=new Cuboid(1.0,0.5,1.0, m_base, true);
    Cuboid* lower_pole=new Cuboid(0.5, 1.5, 0.5, m_pole,true);
    tf_lowerPole = new Transform(0.0, 0.0, 0.0, 0, 0.5, 0, 1.0, 1.0, 1.0, 2);

    Cuboid* mid_pole=new Cuboid(0.5, 1.5, 0.5, m_pole,true);
    tf_midPole = new Transform(0.0, 0.0, 0.0, 0,0.75, 0, 1.0, 1.0, 1.0, 3);

    Cuboid* upper_pole=new Cuboid(0.5, 1.5, 0.5, m_pole,true);
    tf_upperPole = new Transform(0.0, 0.0, 0.0, 0, 0.75, 0, 1.0, 1.0, 1.0, 4);

    Cuboid* r_flag=new Cuboid(1, 1.0, 0.125, m_flag,true);
    tf_rFlag = new Transform(0.0, 0.0, 0.0, -0.40, 0, 0, 1.0, 1.0, 1.0, 5);

    Cuboid* mid_flag=new Cuboid(1, 1.0, 0.125, m_flag,true);
    tf_mFlag = new Transform(0.0, 0.0, 0.0, -0.65, 0, 0, 1.0, 1.0, 1.0, 6);

    Cuboid* l_flag=new Cuboid(1, 1.0, 0.125, m_flag,true);
    tf_lFlag = new Transform(0.0, 0.0, 0.0, -0.65, 0, 0, 1.0, 1.0, 1.0, 7);
    //Main Transform from flag
    tf->addChild(tf_base);
    tf_base->addChild(tf_lowerPole);
    tf_lowerPole->addChild(tf_midPole);
    tf_midPole->addChild(tf_upperPole);
    tf_upperPole->addChild(tf_rFlag);
    tf_rFlag->addChild(tf_mFlag);
    tf_mFlag->addChild(tf_lFlag);
    //Creating Creating Child Transforms in poles and flag to handle the rotation
    Transform* tf_rot_lp=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 21);
    tf_lowerPole->addChild(tf_rot_lp);

    Transform* tf_rot_mp=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 31);
    tf_midPole->addChild(tf_rot_mp);

    Transform* tf_rot_up=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 41);
    tf_upperPole->addChild(tf_rot_up);

    Transform* tf_rot_rf=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 51);
    tf_rFlag->addChild(tf_rot_rf);

    Transform* tf_rot_mf=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 61);
    tf_mFlag->addChild(tf_rot_mf);

    Transform* tf_rot_lf=new Transform(0.0, 0.0, 0.0, 0,0, 0, 1, 1, 1, 71);
    tf_lFlag->addChild(tf_rot_lf);

    base->addCuboidToScene(tf_base, scene);
    lower_pole->addCuboidToScene(tf_rot_lp, scene);
    mid_pole->addCuboidToScene(tf_rot_mp, scene);
    upper_pole->addCuboidToScene(tf_rot_up, scene);
    r_flag->addCuboidToScene(tf_rot_rf, scene);
    mid_flag->addCuboidToScene(tf_rot_mf, scene);
    l_flag->addCuboidToScene(tf_rot_lf, scene);
}


void WavingFlag::animateFlag(){
    currentSine=sin(windStrength*tmp*radian);

    Transform* tf_lp=tf_lowerPole->getChild(1);
    Transform* tf_mp=tf_midPole->getChild(1);
    Transform* tf_up=tf_upperPole->getChild(1);

    Transform* tf_rf=tf_rFlag->getChild(1);
    Transform* tf_mf=tf_mFlag->getChild(1);
    Transform* tf_lf=tf_lFlag->getChild(0);


    if(up){
        tmp++;
        currentSine=sin(windStrength*tmp*radian);
        sangle=(currentSine-sin(windStrength*(tmp-1)*radian));

        std::cout<<currentSine<<std::endl;
        if(currentSine>=crit_angle && up){
            std::cout<<"Above crit angle"<<std::endl;

            up=false;
            down=true;



        }


    }
    if(down){
        tmp--;
        currentSine=sin(windStrength*tmp*radian);
        sangle=(currentSine-sin(windStrength*(tmp+1)*radian));
        std::cout<<currentSine<<std::endl;
        if(currentSine<=-crit_angle && down){
            std::cout<<"Below crit angle"<<std::endl;
            tmp++;
            up=true;
            down=false;
            sangle=(currentSine-sin(windStrength*(tmp-1)*radian));
            currentSine=sin(windStrength*tmp*radian);

        }
    }

    tf_lp->setMat(tf_lp->getMat()*Mat::RotZ(sangle));
    tf_mp->setMat(tf_mp->getMat()*Mat::RotZ(-sangle));
    tf_up->setMat(tf_up->getMat()*Mat::RotZ(sangle));


    tf_rf->setMat(tf_rf->getMat()*Mat::RotY(sangle));
    tf_mf->setMat(tf_mf->getMat()*Mat::RotY(-sangle));
    tf_lf->setMat(tf_lf->getMat()*Mat::RotY(sangle));

    tf->update_global_tf();
}
    
