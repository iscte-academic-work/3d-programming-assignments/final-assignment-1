//
// Created by abeltenera on 11/11/19.
//

#ifndef FINAL_ASSIGNMENT_PROTECTIONRAIL_HPP
#define FINAL_ASSIGNMENT_PROTECTIONRAIL_HPP

#include "Cuboid.hpp"
#include "Material.hpp"
#include "../helpers/AppProperties.hpp"

class ProtectionRail {
public:
    double width,height,length;

    ProtectionRail(double _w, double _h, double _l);
    ProtectionRail(double _w, double _h, double _l, Material* _protectionRailMaterial);
    ~ProtectionRail();

    /*!
     *
     * @param _t transform
     * @param _scene scene to be applied
    */
    void addProtectionRailToScene(Transform* _t, Scene* _scene);

    void setSurfaceId(std::string _surfaceId);

private:
    std::string surfaceId = "";
    Material *protectionRailMaterial;

};


#endif //FINAL_ASSIGNMENT_PROTECTIONRAIL_HPP
