//
// Created by abeltenera on 09/11/19.
//

#include "Cuboid.hpp"

Cuboid::Cuboid(double _w, double _h, double _l) {
    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL);
    width = _w;
    height = _h;
    length = _l;
    topSurfaceMat= defaultMaterial;
    bottomSurfaceMat=defaultMaterial;
    leftSurfaceMat=defaultMaterial;
    rightSurfaceMat=defaultMaterial;
    frontSurfaceMat=defaultMaterial;
    backSurfaceMat=defaultMaterial;
}

Cuboid::Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat){
    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL);
    width = _w;
    height = _h;
    length = _l;
    topSurfaceMat= _topSurfaceMat;
    bottomSurfaceMat=defaultMaterial;
    leftSurfaceMat=defaultMaterial;
    rightSurfaceMat=defaultMaterial;
    frontSurfaceMat=defaultMaterial;
    backSurfaceMat=defaultMaterial;
}

Cuboid::Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat, Material* _bottomSurfaceMat, Material* _leftSurfaceMat,
               Material* _rightSurfaceMat, Material* _frontSurfaceMat, Material* _backSurfaceMat) {

    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL); // TODO Set this material as an instance object
    width = _w;
    height = _h;
    length = _l;
    topSurfaceMat= _topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;
    bottomSurfaceMat=_bottomSurfaceMat == NULL ? defaultMaterial : _bottomSurfaceMat;
    leftSurfaceMat=_leftSurfaceMat == NULL ? defaultMaterial : _leftSurfaceMat;
    rightSurfaceMat=_rightSurfaceMat == NULL ? defaultMaterial : _rightSurfaceMat;
    frontSurfaceMat=_frontSurfaceMat == NULL ? defaultMaterial : _frontSurfaceMat;
    backSurfaceMat=_backSurfaceMat == NULL ? defaultMaterial : _backSurfaceMat;
}
Cuboid::Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat,bool allSurfaces){
    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL); // TODO Set this material as an instance object
    width = _w;
    height = _h;
    length = _l;
    if(allSurfaces){
        topSurfaceMat= _topSurfaceMat;
        bottomSurfaceMat=_topSurfaceMat;
        leftSurfaceMat=_topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;
        rightSurfaceMat=_topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;
        frontSurfaceMat=_topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;
        backSurfaceMat=_topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;

	}
}

Cuboid::~Cuboid(){
    delete [] topSurfaceMat;
    delete [] bottomSurfaceMat;
    delete [] leftSurfaceMat;
    delete [] rightSurfaceMat;
    delete [] frontSurfaceMat;
    delete [] backSurfaceMat;
}

void Cuboid::addCuboidToScene(Transform *_tf, Scene *_scene){
    Mat ft = Mat::Translate(0,0,0); // Creates an identity matrix
    addCuboidToScene(ft, _tf, _scene);
}

void Cuboid::setSurfaceId(std::string _surfaceId) {
    surfaceId = _surfaceId;
}

void Cuboid::addCuboidToScene(Mat _ft, Transform *_tf, Scene *_scene) {
    std::cout << "Adding cuboid to scene" << std::endl;
    // Create the vectors
    // vx -> triangle vertex coordinate
    // vtx -> texture coordinate
    double halfWidth = width/2;
    double halfLength = length/2;
    double halfHeight = height/2;

    Vec v1 = createVec(-halfWidth, -halfHeight, halfLength, _ft);
    Vec v2 = createVec(halfWidth, -halfHeight, halfLength, _ft);
    Vec v3= createVec(halfWidth, halfHeight, halfLength, _ft);
    Vec v4= createVec(-halfWidth, halfHeight, halfLength, _ft);

    Vec v5= createVec(-halfWidth, -halfHeight, -halfLength, _ft);
    Vec v6= createVec(halfWidth, -halfHeight, -halfLength, _ft);
    Vec v7= createVec(halfWidth, halfHeight, -halfLength, _ft);
    Vec v8= createVec(-halfWidth, halfHeight, -halfLength, _ft);
    std::cout << "Created cuboid vectors" << std::endl;

    Vec vt1(0,0);
    Vec vt2(1,0);
    Vec vt3(1,1);
    Vec vt4(0,1);
    std::cout << "Created cuboid texture vectors" << std::endl;

    // Top surface
    createTriangle(_scene, topSurfaceMat, _tf, v4, v7, v3, vt1, vt3, vt2);
    createTriangle(_scene, topSurfaceMat, _tf, v7, v4, v8, vt3, vt1, vt4);
    std::cout << "Created top surface" << std::endl;

    if(!drawOnlyTopSurface) {
        // Bottom surface
        createTriangle(_scene, bottomSurfaceMat, _tf, v1, v6, v2, vt1, vt3, vt2);
        createTriangle(_scene, bottomSurfaceMat, _tf, v6, v1, v5, vt3, vt1, vt4);
        std::cout << "Created bottom surface" << std::endl;
        // Left surface
        createTriangle(_scene, leftSurfaceMat, _tf, v4, v5, v1, vt1, vt3, vt2);
        createTriangle(_scene, leftSurfaceMat, _tf, v5, v4, v8, vt3, vt1, vt4);
        std::cout << "Created left surface" << std::endl;
        // Right surface
        createTriangle(_scene, rightSurfaceMat, _tf, v3, v6, v2, vt1, vt3, vt2);
        createTriangle(_scene, rightSurfaceMat, _tf, v6, v3, v7, vt3, vt1, vt4);
        std::cout << "Created right surface" << std::endl;
        if (!drawAsTunnel) {
            // Back surface
            createTriangle(_scene, backSurfaceMat, _tf, v5, v7, v6, vt1, vt3, vt2);
            createTriangle(_scene, backSurfaceMat, _tf, v7, v5, v8, vt3, vt1, vt4);
            std::cout << "Created back surface" << std::endl;
            // Front surface
            createTriangle(_scene, frontSurfaceMat, _tf, v1, v3, v2, vt1, vt3, vt2);
            createTriangle(_scene, frontSurfaceMat, _tf, v3, v1, v4, vt3, vt1, vt4);
            std::cout << "Created front surface" << std::endl;
        }
    }
}


void Cuboid::setDrawTunnelMode(bool _drawAsTunnel) {
    std::cout << "Setting draw as tunnel mode to " << _drawAsTunnel << std::endl;
    drawAsTunnel = _drawAsTunnel;
}

void Cuboid::setDrawOnlyTopSurfaceMode(bool _drawOnlyTopSurface) {
    std::cout << "Setting draw only top surface mode to " << _drawOnlyTopSurface << std::endl;
    drawOnlyTopSurface = _drawOnlyTopSurface;
}

void Cuboid::createTriangle(Scene *scene, Material *m, Transform *tf, Vec v1, Vec v2, Vec v3, Vec vt1, Vec vt2,
                            Vec vt3)
{
    Triangle *t;
    // Verifies this Material has a texture
    if(m->tex_map){
        t = new Triangle(new Vertex(v1, vt1), new Vertex(v2, vt2), new Vertex(v3, vt3), true, m, false);
    }
    else{
        t = new Triangle(new Vertex(v1), new Vertex(v2), new Vertex(v3), true, m, false);
    }
    t->setTransform(tf);
    std::cout << "Adding triangle with surface id " << surfaceId << std::endl;
    scene->addSurface(t, surfaceId);
}

Vec Cuboid::createVec(double x, double y, double z, Mat m){
    return m*Vec(x,y,z);
}
