//
// Created by abeltenera on 16/11/19.
//

#ifndef FINAL_ASSIGNMENT_STRAIGHTROAD_HPP
#define FINAL_ASSIGNMENT_STRAIGHTROAD_HPP

#include "Material.hpp"
#include "Transform.hpp"
#include "Mesh.hpp"
#include "Box.hpp"
#include "Scene.hpp"
#include <cstdlib>
#include <ctime>
#include <math.h>
#include "../helpers/MathUtils.hpp"
#include "../helpers/AppProperties.hpp"
class StraightRoad {
public:
    const std::string rightSideWalkSurfacePrefix = "sr_sidewalk_right_";
    const std::string leftSideWalkSurfacePrefix = "sr_sidewalk_left_";
    const std::string roadSurfacePrefix = "sr_road_";
    const std::string leftProtectionRailSurfacePrefix = "sr_protection_rail_left_";
    const std::string rightProtectionRailSurfacePrefix = "sr_protection_rail_right_";

    std::string surfaceIdSuffix = "";

    double rw, rh, rl; // road width, height, length
    double prw, prh, prl; // protection rail width, height, length
    double sw, sh, sl; // sidewalk width, height, length

    Material *roadTopMaterial;
    Material *sidewalkTopMaterial;
    Material *protectionRailMaterial;

    //StraightRoad(double _w, double _h, double _l);
    StraightRoad(double _rw, double _rh, double _rl,
            double _prw, double _prh, int _railsPerSide,
            double _sw, double _sh,
            Material *_roadTopMaterial, Material *_sidewalkTopMaterial, Material *_protectionRailMaterial);
    ~StraightRoad();
    void addStraightRoadToScene(Transform* _t, Scene* _scene);

    void loadDecorations(double decorationsHeight, const char* treeMeshLocation, const char* streetLampMeshLocation, Material *materialTree, Material *materialLamp, Transform *_t, Scene *_scene);

    // Must be called befor addStraightRoadToScene in order to have effect
    void setSurfaceIdSuffix(std::string surfaceIdSuffix);
private:

    int numberOfRailsPerSide;
};


#endif //FINAL_ASSIGNMENT_STRAIGHTROAD_HPP
