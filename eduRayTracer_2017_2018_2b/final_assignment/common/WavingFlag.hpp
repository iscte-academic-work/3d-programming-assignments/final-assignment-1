#ifndef FINAL_ASSIGNMENT_WavingFlag_HPP
#define FINAL_ASSIGNMENT_WavingFlag_HPP

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "../src/Material.hpp"
#include "../src/Scene.hpp"
#include "../src/RayTracer.hpp"
#include "../src/utils.hpp"
#include "Cuboid.hpp"
#include "../src/Transform.hpp"
#include "../src/Mat.hpp"
#include <vector>
 
 
 class WavingFlag {

 public:

     Vec wind;

     Material* m_base;

     Material* m_pole;

     Material* m_flag;

     Transform* tf_base;

     Transform* tf_lowerPole;

     Transform* tf_midPole;

     Transform* tf_upperPole;

     Transform* tf_rFlag;

     Transform* tf_mFlag;

     Transform* tf_lFlag;

     Transform* tf;
	
	double windStrength;

	double tmp;
	double currentSine;
	double sangle;
	double radian;
	double crit_angle;
	bool up;
	bool down;
	

     WavingFlag(Material* m1, Material* m2, Material* m3, double _windStrength);


     ~WavingFlag();

     void createFlag(Scene *scene, Transform* tf, Vec w);

     Transform* getBaseTransform();

     Transform* getLowerPoleTransform();

     Transform* getMidPoleTransForm();

     Transform* getUpperPoleTransform();

     Transform* getRightFlagTransform();

     Transform* getMidFlagTransform();

     Transform* getLeftFlagTransform();
     
     void animateFlag();
 };

#endif
