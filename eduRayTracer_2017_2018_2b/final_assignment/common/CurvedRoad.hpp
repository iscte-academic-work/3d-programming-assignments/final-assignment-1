//
// Created by abeltenera on 17/11/19.
//

#ifndef FINAL_ASSIGNMENT_CURVEDROAD_HPP
#define FINAL_ASSIGNMENT_CURVEDROAD_HPP
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "Curve3D.hpp"
#include "ProtectionRail.hpp"
#include "Transform.hpp"
#include "Vec.hpp"
#include <cmath>
#include <tgmath.h>
#include "../helpers/MathUtils.hpp"
#include "../helpers/AppProperties.hpp"

class CurvedRoad {
public:
    const std::string rightSideWalkSurfacePrefix = "sr_sidewalk_right_";
    const std::string leftSideWalkSurfacePrefix = "sr_sidewalk_left_";
    const std::string roadSurfacePrefix = "sr_road_";
    const std::string leftProtectionRailSurfacePrefix = "sr_protection_rail_left_";
    const std::string rightProtectionRailSurfacePrefix = "sr_protection_rail_right_";

    std::string surfaceIdSuffix = "";

    double rw, rh, rl; // road width, height, length
    double prw, prh; // protection rail width, height, length
    double sw, sh; // sidewalk width, height, length
    double outerPrl; // protection rail length of outer curve
    double innerPrl; // protection rail length of inner curve

    //! startingAngle and endingAngle determine where the curve starts and ends in degrees
    double startingAngle;
    double endingAngle;

    //! How many divisions to be defined along the curve length
    int numberOfDivisions;

    Material *roadTopMaterial;
    Material *sidewalkTopMaterial;
    Material *protectionRailMaterial;

    //CurvedRoad(double _w, double _h, double _l);
    CurvedRoad(double _rw, double _rh, double _rl,
                 double _prw, double _prh, int _numberOfRailsOuterCurve,
                 double _sw, double _sh, double _startingAngle, double _endingAngle, int _numberOfDivisions,
                 Material *_roadTopMaterial, Material *_sidewalkTopMaterial, Material *_protectionRailMaterial);
    ~CurvedRoad();
    void addCurvedRoadToScene(Transform* _t, Scene* _scene, bool _distortTexture);

    void loadDecorations(double decorationsHeight, const char* treeMeshLocation, const char* streetLampMeshLocation, Material *materialTree, Material *materialLamp, Transform *_t, Scene *_scene);

    // Must be called befor addCurvedRoadToScene in order to have effect
    void setSurfaceIdSuffix(std::string surfaceIdSuffix);

    /*!
     * The number of trees to be drawn depends on the number of rails on each side of the curve
     * But if it is too much or too little trees, set tree density to a different value.
     * @param _treeDensity
     */
    void setTreeDensity(double _treeDensity){
        treeDensity = _treeDensity;
    }

    /*!
     * The number of street lamps to be drawn depends on the number of rails on each side of the curve
     * But if it is too much or too little street lamps, set street lamp density to a different value.
     * @param _treeDensity
     */
    void setStreetLampDensity(double _streetLampDensity){
        streetLampDensity = _streetLampDensity;
    }
private:
    double treeDensity = 1;
    double streetLampDensity = 1;
    double outerRailAmplitudeStep;
    double innerRailAmplitudeStep;
    double amplitude;
    int numberOfRailsOuterCurve;
    int numberOfRailsInnerCurve;

    /*!
     * Generates a curved rail chain (note: each rail is straight)
     */
    void generateRailChain(double amplitudeStep, double ray, int numberOfRails, double railLength, int transformId, std::string surfaceId, Scene* scene, Transform* parentTransform);

    void loadLamps(double decorationsHeight, const char* streetLampMeshLocation, Material *materialLamp, Transform *_t, Scene *_scene, bool leftSide);
    void loadTrees(double decorationsHeight, const char* treeMeshLocation, Material *materialTree, Transform *_t, Scene *_scene, bool leftSide);
};


#endif //FINAL_ASSIGNMENT_CURVEDROAD_HPP
