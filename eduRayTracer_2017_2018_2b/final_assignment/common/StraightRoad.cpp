//
// Created by abeltenera on 16/11/19.
//

#include "StraightRoad.hpp"
#include "Cuboid.hpp"
#include "ProtectionRail.hpp"
#include "Transform.hpp"

StraightRoad::StraightRoad(double _rw, double _rh, double _rl, double _prw, double _prh, int _railsPerSide, double _sw,
                           double _sh, Material *_roadTopMaterial, Material *_sidewalkTopMaterial,
                           Material *_protectionRailMaterial) {
    rw = _rw;
    rh =_rh;
    rl = _rl;
    prw = _prw;
    prh =_prh;
    prl = _rl/_railsPerSide;
    sw = _sw;
    sh =_sh;
    sl = _rl;
    numberOfRailsPerSide = _railsPerSide;
    roadTopMaterial = _roadTopMaterial;
    sidewalkTopMaterial=_sidewalkTopMaterial;
    protectionRailMaterial=_protectionRailMaterial;
}

StraightRoad::~StraightRoad(){
    delete [] roadTopMaterial;
    delete [] sidewalkTopMaterial;
    delete [] protectionRailMaterial;
}

void StraightRoad::addStraightRoadToScene(Transform *_t, Scene *_scene) {
    // Class instances that will help create the intended objects for the road
    Cuboid* road = new Cuboid(rw,rh,rl,roadTopMaterial);
    Cuboid* sideWalk = new Cuboid(sw,sh,sl,sidewalkTopMaterial);
    ProtectionRail* protectionRail = new ProtectionRail(prw,prh,prl, protectionRailMaterial);

    // Transforms
    int id = _t->id;
    Transform *roadTransform = _t; // road will contain the parent transform

    double sidewalkXT = sideWalk->width/2 + road->width/2;
    double sidewalkYT = sideWalk->height - road->height;
    Transform *leftSideWalkTransform = new Transform(Mat::Translate(-sidewalkXT, sidewalkYT,0),AppProperties::getTransformId());
    Transform *rightSideWalkTransform = new Transform(Mat::Translate(sidewalkXT, sidewalkYT,0),AppProperties::getTransformId());
    roadTransform->addChild(leftSideWalkTransform);
    roadTransform->addChild(rightSideWalkTransform);

    // Adds road and sidewalk
    road->setSurfaceId(roadSurfacePrefix + surfaceIdSuffix);
    road->addCuboidToScene(roadTransform, _scene);
    sideWalk->setSurfaceId(leftSideWalkSurfacePrefix + surfaceIdSuffix);
    sideWalk->addCuboidToScene(leftSideWalkTransform,_scene);
    sideWalk->setSurfaceId(rightSideWalkSurfacePrefix + surfaceIdSuffix);
    sideWalk->addCuboidToScene(rightSideWalkTransform,_scene);

    // Adds rails chain
    for(double i = 0; i<numberOfRailsPerSide; i++){
        Transform *leftRailTransform = new Transform(Mat::Translate(-(rw/2-prw/2),rh/2+prh/2, rl*(i/numberOfRailsPerSide-0.5) + prl/2),AppProperties::getTransformId());
        Transform *rightRailTransform = new Transform(Mat::Translate(rw/2-prw/2,rh/2+prh/2, rl*(i/numberOfRailsPerSide-0.5) + prl/2),AppProperties::getTransformId());
        roadTransform->addChild(leftRailTransform);
        roadTransform->addChild(rightRailTransform);

        protectionRail->setSurfaceId(leftProtectionRailSurfacePrefix + surfaceIdSuffix);
        protectionRail->addProtectionRailToScene(leftRailTransform,_scene);
        protectionRail->setSurfaceId(rightProtectionRailSurfacePrefix + surfaceIdSuffix);
        protectionRail->addProtectionRailToScene(rightRailTransform,_scene);
    }
}

void StraightRoad::loadDecorations(double decorationsHeight, const char *treeMeshLocation,
                                   const char *streetLampMeshLocation, Material *materialTree, Material *materialLamp,
                                   Transform *_t, Scene *_scene) {
    srand(time(NULL));
    int transformId = _t->id;
    // Number of street lamps per side: must not be more than numberOfRailsPerSide, and bigger than zero

    int numStreetLampsPerSide = rand()%numberOfRailsPerSide + 1;
    double roadSlotLength = rl/numStreetLampsPerSide; //Each lamp will be placed randomly but inside a slot
    // Lamps will be placed randomly, but in a simetrical way and in the sidewalk. Each side of the road shall look similar
    for(double i = 0; i<numStreetLampsPerSide; i++){

        // Loads meshes for right and left lamp
        Mesh* meshLampL = new Mesh(streetLampMeshLocation, materialLamp,false);
        Mesh* meshLampR = new Mesh(streetLampMeshLocation, materialLamp,false);

        // Gets the bounding box of the lamp
        Box lampBox = meshLampL->getBBox();
        Vec lampMin = lampBox.min;
        Vec lampMax = lampBox.max;

        // Gets scaling matrix for lamp, to resize it to a desired height
        Mat lampScaleMat = MathUtils::resizeMeshMat(meshLampL, decorationsHeight, Y);
        double lampScalingFactor = lampScaleMat.data[0][0];

        // Compute lamp height
        double lampHeight = std::abs(lampMin.y - lampMax.y) * lampScalingFactor;

        // The lamp will be placed in the center of the road slot with a perturbation in x and z axis
        // First we calculate the displacements in x axis
        // The displacement (in x) of the street lamp will be half of the roadWidth plus a perturbation
        // This perturbation is a percentage of the sidewalk width
        double lampX = rw/2 + sw*(rand()%51/100.0);
        double lampY = sh/2; //The lamp will be drawn with its base on the center. So we just adjust to the sidewalk height
        double lampZ = rl*(i/numStreetLampsPerSide-0.5)+roadSlotLength/2 + roadSlotLength*(rand()%51/100.0)*pow(-1, rand());
        Transform* leftLampTransform  = new Transform(Mat::Translate(-lampX, lampY, lampZ)*lampScaleMat*Mat::RotY(M_PI), AppProperties::getTransformId());
        Transform* rightLampTransform = new Transform(Mat::Translate(lampX, lampY, lampZ)*lampScaleMat, AppProperties::getTransformId());

        meshLampL->setTransform(leftLampTransform);
        meshLampR->setTransform(rightLampTransform);

        _t->addChild(leftLampTransform);
        _t->addChild(rightLampTransform);

        _scene->addSurface(meshLampL);
        _scene->addSurface(meshLampR);

    }

    // Trees will be placed more irregularly. They will be drawn after the sidewalk
    // Their heights will have a perturbation.
    // They will be drawn in rows aligned with each rail, but with a random perturbation
    for(double i = 0; i<numberOfRailsPerSide; i++) {
        int maxTreeInRowPerSide = ceil(numberOfRailsPerSide/2.0);
        int numberOfTreesInRowPerSide = rand()%numberOfRailsPerSide + 1;

        double treeMinXDisplacementLeft = 0;
        double treeMinXDisplacementRight = 0;
        for(double j = 0; j< numberOfTreesInRowPerSide; j++){
            bool isLeft = pow(-1, rand()) < 0;
            double treeMinXDisplacement = isLeft ? treeMinXDisplacementLeft : treeMinXDisplacementRight;
            Mesh* meshTree = new Mesh(treeMeshLocation, materialTree,false);

            double treeHeight = decorationsHeight + decorationsHeight*(rand()%51/100.0)*pow(-1, rand()); // This perturbation can decrease/increase the tree size down/up to it's half

            // Gets the bounding box of the tree
            Box treeBox = meshTree->getBBox();
            Vec treeMin = treeBox.min;
            Vec treeMax = treeBox.max;

            // Generate scale matrix. The scale will be applied equal to all components
            Mat treeScaleMat = MathUtils::resizeMeshMat(meshTree, treeHeight, Y);

            double treeScalingFactor = treeScaleMat.data[0][0];

            double treeWidth = std::abs(treeMin.x - treeMax.x) * treeScalingFactor;

            // First we calculate the displacements in x axis
            // The displacement (in x) of the tree will be half of the roadWidth plus sidewalk width plus tree width
            treeMinXDisplacement = treeMinXDisplacement == 0 ? rw/2 + sw + treeWidth/2 : treeMinXDisplacement;
            double treeX = treeMinXDisplacement + treeWidth*(rand()%51/100.0)*pow(-1, rand());
            treeMinXDisplacement += treeWidth;
            if(isLeft)
                treeMinXDisplacementLeft = treeMinXDisplacement;
            else
                treeMinXDisplacementRight = treeMinXDisplacement;

            double treeY = sh/2;
            double treeZ = rl*(i/numberOfRailsPerSide-0.5)+prl/2 + prl*(rand()%51/100.0)*pow(-1, rand());

            Transform* tfTree = new Transform(treeScaleMat, AppProperties::getTransformId());

            Transform* treeTransform = NULL;
            if(isLeft)
                treeTransform = new Transform(Mat::Translate(-treeX, treeY, rl*(i/numberOfRailsPerSide-0.5)+prl/2)*treeScaleMat*Mat::RotY(M_PI), AppProperties::getTransformId());
            else
                treeTransform = new Transform(Mat::Translate(treeX, treeY, rl*(i/numberOfRailsPerSide-0.5)+prl/2)*treeScaleMat, AppProperties::getTransformId());

            meshTree->setTransform(treeTransform);

            _t->addChild(treeTransform);

            _scene->addSurface(meshTree);

        }

    }


}

void StraightRoad::setSurfaceIdSuffix(std::string _surfaceIdSuffix) {
    surfaceIdSuffix = _surfaceIdSuffix;
}













