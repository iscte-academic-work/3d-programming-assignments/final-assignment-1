//
// Created by abeltenera on 17/11/19.
//

#include "CurvedRoad.hpp"

CurvedRoad::CurvedRoad(double _rw, double _rh, double _rl, double _prw, double _prh, int _numberOfRailsOuterCurve, double _sw,
                       double _sh, double _startingAngle, double _endingAngle, int _numberOfDivisions,
                       Material *_roadTopMaterial, Material *_sidewalkTopMaterial, Material *_protectionRailMaterial) {

    rw = _rw;
    rh =_rh;
    rl = _rl;
    prw = _prw;
    prh =_prh;
    //prl = _rl/_railsPerSide;
    sw = _sw;
    sh =_sh;
    numberOfRailsOuterCurve = _numberOfRailsOuterCurve;
    startingAngle = _startingAngle;
    endingAngle = _endingAngle;
    numberOfDivisions = _numberOfDivisions;

    roadTopMaterial = _roadTopMaterial;
    sidewalkTopMaterial=_sidewalkTopMaterial;
    protectionRailMaterial=_protectionRailMaterial;
    amplitude = endingAngle - startingAngle;
    outerRailAmplitudeStep= amplitude / numberOfRailsOuterCurve;

}

CurvedRoad::~CurvedRoad() {
    delete [] roadTopMaterial;
    delete [] sidewalkTopMaterial;
    delete [] protectionRailMaterial;
}

void CurvedRoad::addCurvedRoadToScene(Transform *_t, Scene *_scene, bool _distortTexture) {

    // Helper object to create the road
    Curve3D* road = new Curve3D(rw,rh,rl,startingAngle,endingAngle,numberOfDivisions,roadTopMaterial);
    // To build the sidewalks we have to recalculate their proper length
    // Since the road is curved, their lengths will differ from the road
    double roadOuterRay = road->getOuterRay();
    double roadInnerRay = road->getInnerRay();
    double roadAmplitude = road->getAmplitude();
    double rightSidewalkOuterRay = roadOuterRay + sw;
    double rightSidewalkLength = roadAmplitude * rightSidewalkOuterRay;
    double leftSidewalkOuterRay = roadInnerRay;
    double leftSidewalkLength = roadAmplitude * leftSidewalkOuterRay;

    // determines the number of rails of inner curve
    numberOfRailsInnerCurve = ceil(numberOfRailsOuterCurve*leftSidewalkOuterRay/rl);
    innerRailAmplitudeStep = amplitude/numberOfRailsInnerCurve;

    // The rail length is calculated based on the amplitude and ray of the outer curve
    // is given by sqrt((cos(amplitudeStep) - ray) ^ 2  + sen(amplitudeStep)^2)
    outerPrl = std::sqrt(pow(roadOuterRay*(cos(outerRailAmplitudeStep) - 1), 2 ) + pow(roadOuterRay*sin(outerRailAmplitudeStep),2));
    innerPrl = std::sqrt(pow(roadInnerRay*(cos(innerRailAmplitudeStep) - 1), 2 ) + pow(roadInnerRay*sin(innerRailAmplitudeStep),2));
    // Helper objects to create the sidewalks
    Curve3D* leftSidewalk = new Curve3D(sw, sh, leftSidewalkLength, startingAngle, endingAngle, numberOfDivisions, sidewalkTopMaterial);
    Curve3D* rightSidewalk = new Curve3D(sw, sh, rightSidewalkLength, startingAngle, endingAngle, numberOfDivisions, sidewalkTopMaterial);

    double sideWalkVerticalDisplacement = rightSidewalk->height - road->height;

    // Transforms
    int id = _t->id;
    Transform *roadTransform = _t; // road will contain the parent transform
    Transform *leftSideWalkTransform = new Transform(Mat::Translate(0,sideWalkVerticalDisplacement,0),AppProperties::getTransformId());

    Transform *rightSideWalkTransform = new Transform(Mat::Translate(0,sideWalkVerticalDisplacement,0),AppProperties::getTransformId());
    roadTransform->addChild(leftSideWalkTransform);
    roadTransform->addChild(rightSideWalkTransform);
    road->setSurfaceId(roadSurfacePrefix + surfaceIdSuffix);
    road->addCurve3DToScene(roadTransform,_scene,_distortTexture);
    leftSidewalk->setSurfaceId(leftSideWalkSurfacePrefix + surfaceIdSuffix);
    leftSidewalk->addCurve3DToScene(leftSideWalkTransform,_scene,_distortTexture);
    rightSidewalk->setSurfaceId(rightSideWalkSurfacePrefix + surfaceIdSuffix);
    rightSidewalk->addCurve3DToScene(rightSideWalkTransform, _scene, _distortTexture);

    // Calculate the road rails
    // we need to determine the rail length

    // Generates the inner rail

    std::cout << "\n\n\n\n" << " roadInnerRay " << roadInnerRay << " numberOfRailsInnerCurve " << numberOfRailsInnerCurve << " innerRailAmplitudeStep " << innerRailAmplitudeStep << "\n\n\n\n\n" << std::endl;
    generateRailChain(innerRailAmplitudeStep, roadInnerRay, numberOfRailsInnerCurve,innerPrl, id, leftProtectionRailSurfacePrefix + surfaceIdSuffix, _scene, roadTransform);
    std::cout << "\n\n\n\n" << " roadOuterRay " << roadOuterRay << " numberOfRailsOuterCurve " << numberOfRailsOuterCurve << " outerRailAmplitudeStep " << outerRailAmplitudeStep << "\n\n\n\n\n" << std::endl;
    generateRailChain(outerRailAmplitudeStep, roadOuterRay, numberOfRailsOuterCurve, outerPrl, id, rightProtectionRailSurfacePrefix + surfaceIdSuffix, _scene, roadTransform);


}

void CurvedRoad::generateRailChain(double amplitudeStep, double ray, int numberOfRails, double railLength, int transformId, std::string surfaceId, Scene* scene, Transform* parentTransform) {
    ProtectionRail* rail = new ProtectionRail(prw,prh,railLength,protectionRailMaterial);
    rail->setSurfaceId(surfaceId);
    for(int i=1; i <= numberOfRails; i++){

        // The rail is drawn from a point to another, so it is needed to calculate its angle in the XoZ plan
        // to determine it's RotY matrix, and then put it between this points.
        // Each step of this cycle operates inside a portion of a circle. So first we calculate the angle of this points 
        // in the whole circle.
        double portionBeginningAngle = startingAngle + (amplitudeStep*(i-1));
        double portionEndingAngle = startingAngle + (amplitudeStep*i);
        
        // Then we determine the location of each point based on their angles
        double portionBeginningPointX = ray*cos(portionBeginningAngle);
        double portionBeginningPointZ = ray*sin(portionBeginningAngle);
        double portionEndingPointX = ray*cos(portionEndingAngle);
        double portionEndingPointZ = ray*sin(portionEndingAngle);
        // With this values we can determine the vector that represents the distance between this points
        Vec a(portionEndingPointX, portionEndingPointZ);
        Vec b(portionBeginningPointX, portionBeginningPointZ);
        Vec d = a-b; // distance vector between a and b

        // Now we just calculate the corresponding angle
        double inclinationAngle = atan2(d.x,d.y);
        std::cout << "ANGLES ANGLES ANGLES" << " atan " << atan(d.y/d.x) << " atan2 " << atan2(d.y,d.x) << " a cos " << inclinationAngle << std::endl;

        std::cout << "\n\n\n\n" << " ax " << a.x << " bx " << b.x << " prl " << railLength << " Inclination angle " <<  inclinationAngle <<" d norm " << d.norm()  << "\n\n\n\n\n" << std::endl;


        // We also need to calculate the translation matrix. The rail will be centered on the middle of a and b points
        Vec c = d/2 + b;
        std::cout << " cx " << c.x << " cy " << c.y << " cz " << c.z << std::endl;

        Mat rotationY = Mat::RotY(inclinationAngle);
        Mat translate = Mat::Translate(c.x,rh/2+prh/2,c.y);

        Mat transformMatrix = translate * rotationY;

        Transform* railTransform = new Transform (transformMatrix, AppProperties::getTransformId());

        rail->addProtectionRailToScene(railTransform, scene);

        parentTransform->addChild(railTransform);

    }
}


void CurvedRoad::loadDecorations(double decorationsHeight, const char *treeMeshLocation,
                                   const char *streetLampMeshLocation, Material *materialTree, Material *materialLamp,
                                   Transform *_t, Scene *_scene) {
    srand(time(NULL));
    int transformId = _t->id;

    loadLamps(decorationsHeight, streetLampMeshLocation, materialLamp, _t, _scene, false); //right
    loadLamps(decorationsHeight, streetLampMeshLocation, materialLamp, _t, _scene, true);  //left

    loadTrees(decorationsHeight, treeMeshLocation, materialTree, _t, _scene, false);
    loadTrees(decorationsHeight, treeMeshLocation, materialTree, _t, _scene, true);

}

void CurvedRoad::loadTrees(double decorationsHeight, const char *treeMeshLocation, Material *materialTree,
                           Transform *_t, Scene *_scene, bool leftSide) {
    srand(time(NULL));
    int transformId = _t->id;

    //For now just considering right side

    int numberOfRailsCurrentSide = (leftSide ? numberOfRailsInnerCurve : numberOfRailsOuterCurve);
    int maxTreeInRow = ceil(numberOfRailsCurrentSide*treeDensity/2.0) ;
    double roadOuterRay = rl/amplitude;
    double roadInnerRay = roadOuterRay-rw;
    double amplitudeStep = amplitude/numberOfRailsCurrentSide;

    for(double i=0; i<numberOfRailsCurrentSide; i++ ){
        int numberOfTreesInRow = rand() % maxTreeInRow + 1;
        double treeMinDisplacement = 0;

        for (double j = 0; j < numberOfTreesInRow; j++) {
            Mesh *meshTree = new Mesh(treeMeshLocation, materialTree, false);
            double treeHeight = decorationsHeight + decorationsHeight * (rand() % 51 / 100.0) * pow(-1, rand()); // This perturbation can decrease/increase the tree size down/up to it's half

            // Gets the bounding box of the tree
            Box treeBox = meshTree->getBBox();
            Vec treeMin = treeBox.min;
            Vec treeMax = treeBox.max;

            // Generate scale matrix. The scale will be applied equal to all components
            Mat treeScaleMat = MathUtils::resizeMeshMat(meshTree, treeHeight, Y);
            double treeScalingFactor = treeScaleMat.data[0][0];
            double treeWidth = std::abs(treeMin.x - treeMax.x) * treeScalingFactor;

            // Determines the beginning and ending angle of the current slots
            double slotBeginningAngle = startingAngle + (amplitudeStep*(i));
            double slotEndingAngle = startingAngle + (amplitudeStep*i);

            // Calculates at what angle the tree will reside in the curve
            double treeAngle = slotBeginningAngle + amplitudeStep*double(rand() % 51)/100.0;

            double treeDistanceToCenter = 0;

            if(leftSide){
                treeMinDisplacement = treeMinDisplacement == 0 ? roadInnerRay - sw - treeWidth/2 : treeMinDisplacement;
                treeDistanceToCenter = treeMinDisplacement;
                treeMinDisplacement -= treeWidth;
            }
            else
            {
                treeMinDisplacement = treeMinDisplacement == 0 ? roadOuterRay + sw  + treeWidth/2 : treeMinDisplacement;
                treeDistanceToCenter = treeMinDisplacement;
                treeMinDisplacement += treeWidth;
            }

            double treeX = treeDistanceToCenter*cos(treeAngle)+ treeWidth*(double(rand() % 51)/100.0);
            double treeZ = treeDistanceToCenter*sin(treeAngle) + treeWidth*(double(rand() % 51)/100.0);
            double treeY = sh / 2;

            Transform *treeTransform = new Transform(Mat::Translate(treeX, treeY, treeZ) * treeScaleMat, AppProperties::getTransformId());
            meshTree->setTransform(treeTransform);

            _t->addChild(treeTransform);

            _scene->addSurface(meshTree);

        }
    }
}

void CurvedRoad::setSurfaceIdSuffix(std::string _surfaceIdSuffix) {
    surfaceIdSuffix = _surfaceIdSuffix;
}

void CurvedRoad::loadLamps(double decorationsHeight, const char *streetLampMeshLocation,Material *materialLamp, Transform *_t, Scene *_scene,
                           bool leftSide) {

    srand(time(NULL));
    int transformId = _t->id;

    int numStreetLamps = ceil((rand()%(leftSide ? numberOfRailsInnerCurve : numberOfRailsOuterCurve))*streetLampDensity) + 1;
    double roadOuterRay = rl/amplitude;
    double roadInnerRay = roadOuterRay-rw;
    double roadSlotLength = rl / numStreetLamps;
    double amplitudeStep = amplitude/numStreetLamps;
    double sidewalkOuterRay = leftSide? roadInnerRay : (roadOuterRay + sw);
    double sidewalkLength = amplitude * sidewalkOuterRay;

    for (double i = 0 ; i< numStreetLamps; i++){
        // Loads mesh for righ lamp
        Mesh *meshLamp = new Mesh(streetLampMeshLocation, materialLamp, false);

        // Gets the bounding box of the lamp
        Box lampBox = meshLamp->getBBox();
        Vec lampMin = lampBox.min;
        Vec lampMax = lampBox.max;

        // Gets scaling matrix for lamp, to resize it to a desired height
        Mat lampScaleMat = MathUtils::resizeMeshMat(meshLamp, decorationsHeight, Y);
        double lampScalingFactor = lampScaleMat.data[0][0];

        // Determines the beginning and ending angle of the current slots
        double slotBeginningAngle = startingAngle + (amplitudeStep*(i));
        double slotEndingAngle = startingAngle + (amplitudeStep*i);

        // Calculates at what angle the lamp will reside in the curve
        double lampAngle = slotBeginningAngle + amplitudeStep*double(rand() % 51)/100.0;

        double lampDistanceToCenter = ( (leftSide ? roadInnerRay : roadOuterRay) + (leftSide? -sw : sw)*(double(rand() % 101)/100.0));
        double lampX = cos(lampAngle)*lampDistanceToCenter;
        double lampZ = sin(lampAngle)*lampDistanceToCenter;
        double lampY = sh / 2;

        //Mat::RotY(M_PI)
        Transform *lampTransform = NULL;

        if(leftSide)
            lampTransform= new Transform(Mat::Translate(lampX, lampY, lampZ) * lampScaleMat * Mat::RotY(M_PI-lampAngle), AppProperties::getTransformId());
        else
            lampTransform= new Transform(Mat::Translate(lampX, lampY, lampZ) * lampScaleMat * Mat::RotY(-lampAngle), AppProperties::getTransformId());

        meshLamp->setTransform(lampTransform);

        _t->addChild(lampTransform);

        _scene->addSurface(meshLamp);
    }



}