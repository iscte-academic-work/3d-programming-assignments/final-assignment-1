//
// Created by abeltenera on 26/11/19.
//

#ifndef FINAL_ASSIGNMENT_CAR_HPP
#define FINAL_ASSIGNMENT_CAR_HPP

#include <stdlib.h>
#include "Mesh.hpp"
#include "Transform.hpp"
#include "Mat.hpp"
#include "Material.hpp"
#include "Vec.hpp"
#include "Scene.hpp"
#include "Box.hpp"
#include "CarSiren.hpp"
#include "../helpers/MathUtils.hpp"
#include "../helpers/AppProperties.hpp"

// Each instance of this class corresponds to a unique car
class CopCar {
public:
    static const std::string carSurfaceIdPrefix(){return "s_car_";};
    double length; // Only length is defined. The mesh will be resized proportionally
    std::string textureLocation;

    CopCar(double _l, const char* _meshLocation, std::string _surfaceIdSuffix, Transform* _tf_car, Material* m_car, Scene* _scene);

    ~CopCar();

    /*!
     * Applies a translation movement to the car
     * @param x translate x
     * @param y translate y
     * @param z translate z
     */
    void moveCar(double x, double y, double z);

    /*!
     * Applies a rotation to the car
     * @param rotX x rotation
     * @param rotY y rotation
     * @param rotZ z rotation
     */
    void rotateCar(double rotX, double rotY, double rotZ);

    /*!
     * Adds value to steeringAngle
     * @param angleStep
     */
    void steerCar(double angleStep);

    void turnSirens(bool on);

    void rotateSirens(double angleStep);

    void printBoundingBox();

    void printTfMat();

    bool getSirenState();

    std::string getSurfaceId(){
        return surfaceId;
    }

    bool rollOver();//simulates accident

    // Only works in y axis
    void driveCar(double rotY, double throttle, double sideAdjustment);

    Box getBBox();
    Box getOriginalBBox();

    Transform* getTransform();

private:
    double height = 0;
    double yDisplacementRollOver = 0;
    double currentRollOverPitch = 0;
    const double rollOverPitchStep = M_PI/8;
    const double maxRollOverPitch = M_PI;
    Box originalBoundingBox; // Saves the original bounding box, with only the initial scaling applied
    const double maxSteeringAngle = M_PI/4;
    const char* defaultMeshLocation ="meshes/LowPolyFiatUNO2.obj";

    Vec frontVector; //This vector indicates the angle of the car itself

    CarSiren* blueSiren;
    CarSiren* redSiren;
    bool sirenState = false; // False: off, true: on
    Vec red = Vec(1,0,0);
    Vec blue = Vec(0,0,1);
    Material* m_blue_siren = new Material(blue, Vec(0.1,0.1,0.1), Vec(0.03,0.03,0.03), 50, 1, 1.3, false, false, false, false, NULL );
    Material* m_red_siren = new Material(red, Vec(0.1,0.1,0.1), Vec(0.03,0.03,0.03), 50, 1, 1.3, false, false, false, false, NULL );

    Mesh* mesh_car;
    Transform* tf_car;
    Transform* tf_blueSiren;
    Transform* tf_redSiren;
    std::string surfaceId;

    void addSiren(Transform* transform, Material* m_siren);


};


#endif //FINAL_ASSIGNMENT_CAR_HPP
