//
// Created by abeltenera on 09/11/19.
//

#ifndef FINAL_ASSIGNMENT_CUBOID_HPP
#define FINAL_ASSIGNMENT_CUBOID_HPP
#include <stdlib.h>
#include "Material.hpp"
#include "Mat.hpp"
#include "Surface.hpp"
#include "Transform.hpp"
#include "Scene.hpp"
#include "../helpers/AppProperties.hpp"

//! Cuboid represents a basic cuboid.
class Cuboid {
	
public:

    //! Width of the cuboid
    double width;
    //! Length of the cuboid
    double length;
    //! Height of the cuboid
    double height;

    Material* topSurfaceMat;
    Material* bottomSurfaceMat;
    Material* leftSurfaceMat;
    Material* rightSurfaceMat;
    Material* frontSurfaceMat;
    Material* backSurfaceMat;

    // TODO : Add possibility to set transforms for each face of the cube, and defining if it is to map texture or not

    /*!
     * @param _w width
     * @param _l length
     * @param _h height
    */
    Cuboid(double _w, double _h, double _l);

    Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat);

    Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat,  Material* _bottomSurfaceMat,  Material* _leftSurfaceMat,  Material* _rightSurfaceMat,  Material* _frontSurfaceMat, Material* _backSurfaceMat);
	
	Cuboid(double _w, double _h, double _l, Material* _topSurfaceMat,bool allSurfaces);
	
    ~Cuboid();
    /*!
     *
     * @param _t transform
     * @param _scene scene to be applied
    */
    void addCuboidToScene(Transform* _t, Scene* _scene);

    /*!
     *
     * @param _ft represents a transformation to be applied on the points of the cuboid before any transform is made
     * @param _t transform
     * @param _scene scene to be applied
     */
    void addCuboidToScene(Mat _ft, Transform* _t, Scene* _scene);

    /*!
     * Sets the param drawAsTunnel property to the input
     * @param _drawAsTunnel
     */
    void setDrawTunnelMode(bool _drawAsTunnel);

    void setDrawOnlyTopSurfaceMode(bool _drawOnlyTopSurface);

    void setSurfaceId(std::string _surfaceId);

private:

    // TODO put this draw modes into an enum
    // If true, the back and front surfaces won't be drawn
    bool drawAsTunnel = false;
    // If true, just draw top surface
    bool drawOnlyTopSurface = false;
    std::string surfaceId;
    void createTriangle(Scene* scene, Material *m, Transform *tf, Vec v1, Vec v2, Vec v3, Vec vt1, Vec vt2, Vec vt3);
    /*!
     * Creates a vector and applies transform matrix
     * @param x x position
     * @param y y position
     * @param z z position
     * @param m transform matrix
     * @return
     */
    Vec createVec(double x, double y, double z, Mat m);
};


#endif //FINAL_ASSIGNMENT_CUBOID_HPP
