//
// Created by abeltenera on 26/11/19.
//

#include "CopCar.hpp"
//Note: never apply a scalling to this car via transform.
CopCar::CopCar(double _l, const char* _meshLocation, std::string _surfaceIdSuffix, Transform* _tf_car, Material* m_car, Scene* _scene) {
    surfaceId = carSurfaceIdPrefix() + _surfaceIdSuffix;

    std::cout << "\n \n CAR IIIIIIIIIIDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD " << surfaceId << std::endl;

    Transform* tf_car_mesh = new Transform(Mat::RotY(M_PI), AppProperties::getTransformId()); // TODO: Pass angle as param in the future
    Material* defaultMaterial = new Material(Vec(0,0,0), Vec(0,0,0), Vec(0,0,0), 0, 0, 0, false, true, false, false, NULL);
    // Set class properties
    length = _l;
    tf_car = _tf_car;
    // Import mesh
    mesh_car = new Mesh(surfaceId,_meshLocation == NULL ? defaultMeshLocation : _meshLocation, m_car,false);
    mesh_car->setTransform(tf_car_mesh);
    _scene->addSurface(mesh_car, surfaceId);
    mesh_car->bounding_box();

    // Compute scaling based on mesh bounding box
    Box carBox = mesh_car->getBBox();

    Vec boxMin = carBox.min;
    Vec boxMax = carBox.max;
    double scalingFactor = length / std::abs(boxMax.z - boxMin.z);
    double boxLength = std::abs(boxMax.z - boxMin.z) * scalingFactor;
    double boxWidth = std::abs(boxMax.x - boxMin.x) * scalingFactor;
    double boxHeight = std::abs(boxMax.y - boxMin.y) * scalingFactor;
    height = boxHeight;
    // Scaling factor is given by the ration between the intended length and the current length of the car

    // Scale matrix that will resize the car to the intended length
    Mat scaleMat = MathUtils::resizeMeshMat(mesh_car,length, Z);

    originalBoundingBox = Box(scaleMat*carBox.min, scaleMat*carBox.max);

    printBoundingBox();
    tf_car_mesh->setMat(tf_car_mesh->getMat()*scaleMat);
    tf_car->addChild(tf_car_mesh);

    // Calculate siren params
    // Siren size
    double newWidth = boxWidth * scalingFactor;
    double sirenSize = 0.2*newWidth;
    // Siren translation params in relation to the car centroid
    double sirenTY = boxHeight + sirenSize/2;
    double blueSirenTX = 0.2*boxWidth;
    double redSirenTX = -0.2*boxWidth;
    Mat blueSirenT = Mat::Translate(blueSirenTX, sirenTY,0);
    Mat redSirenT = Mat::Translate(redSirenTX, sirenTY,0);
    std::string blueSirenId = surfaceId + ("_blue_siren");
    std::string redSirenId = surfaceId + ("_red_siren");
    Transform* blueSirenTransform = new Transform(blueSirenT, AppProperties::getTransformId());
    Transform* redSirenTransform = new Transform(redSirenT, AppProperties::getTransformId());

    // Creates the sirens
    blueSiren = new CarSiren(sirenSize, 0.2,scalingFactor, blueSirenTransform, defaultMaterial, m_blue_siren,
            _scene, blueSirenId, _tf_car->id);

    redSiren = new CarSiren(sirenSize, 0.2,scalingFactor, redSirenTransform, defaultMaterial, m_red_siren,
                                       _scene, redSirenId, _tf_car->id);
    tf_car->addChild(blueSirenTransform);
    tf_car->addChild(redSirenTransform);

    std::cout << "\n\n CENTROID " << mesh_car->getCentroid().x << " "<< mesh_car->getCentroid().y<< " " << mesh_car->getCentroid().z << std::endl;
    turnSirens(false);

    printTfMat();
}

CopCar::~CopCar() {
    delete [] tf_car;
    delete [] mesh_car;
    delete [] m_blue_siren;
    delete [] m_red_siren;
    delete [] defaultMeshLocation;
    delete [] tf_blueSiren;
    delete [] tf_redSiren;
    delete [] tf_redSiren;
    delete [] redSiren;
    delete [] blueSiren;
}

// This methods will be used during render phase
// TODO: TER EM CONTA OS SLIDES DE CONTROLO FPS
// ALTERAR A ASSINATURA DO MÉTODO PARA APENAS ACEITAR MOVIMENTO PARA A FRENTE E PARA TRÁS
// Movimentação do carro: arranjar forma de ter um vector que olhe sempre para a frente do carro
//                          este vector irá acompanhar o movimento do carro. quando se acelera,
//                          o ângulo pelo qual o carro move é afectado por este vector.
//
//                   não era má ideia ter dois vectores: um que acompanhe o carro (vector frente), e outro de steering (vector volante).
//                      o vector volante afecta o ângulo do vector frente e do , e o vector frente afecta a translação do veiculo
//
//      quando se vira o carro:
//          - O carro roda o ângulo sobre si próprio
//          - O vector frente tem o seu ângulo alterado conforme
//          - O translação é determinada pela aceleração, afectada pelo angulo final do vector frente
//
//      nota: o volante irá ter o seguinte comportamento:
//          - pressiona tecla: define o ângulo do volante para X graus
//          - mantem pressionada: o volante mantém a posição
//          - key up: o volante vira -X graus (volta à posição inicial)
void CopCar::moveCar(double x, double y, double z) {
    rotateSirens(M_PI/3);
    Mat moveM = Mat::Translate(x,y,z);
    tf_car->setMat(moveM*tf_car->getMat());
}

void CopCar::rotateCar(double rotX, double rotY, double rotZ) {
    rotateSirens(M_PI/3);
    std::cout<< "\nRotating car" << std::endl;
    Mat rotation = Mat::RotZ(rotZ)*Mat::RotY(rotY)*Mat::RotX(rotX);
    tf_car->setMat(tf_car->getMat()*rotation);
}

void CopCar::driveCar(double rotY, double throttle, double sideAdjustment) {
    rotateSirens(M_PI/3);
    Mat t_rot = Mat::FullPose(rotY/2,0,0,0,0,0,1,1,1);
    Mat t_trans = Mat::FullPose(0,0,0,sideAdjustment,0.0,throttle,1,1,1);

    tf_car->setMat(tf_car->getMat()*t_rot*t_trans);
}

bool CopCar::rollOver() {
    currentRollOverPitch += rollOverPitchStep;
    double lastYDisplacement = yDisplacementRollOver;
    yDisplacementRollOver = (length)*sin(currentRollOverPitch);// + (currentRollOverPitch >=M_PI_2 ? -yDisplacementRollOver : yDisplacementRollOver) ;// - yDisplacementRollOver;
    //yDisplacementRollOver = length*sin(currentRollOverPitch) + (currentRollOverPitch >=M_PI_2 ? -yDisplacementRollOver : yDisplacementRollOver) ;// - yDisplacementRollOver;
    //yDisplacementRollOver = test ? length : 0;
    double signal = currentRollOverPitch >= M_PI_2? -1 : 1;

    double aditional = (currentRollOverPitch >=maxRollOverPitch-rollOverPitchStep && currentRollOverPitch <= maxRollOverPitch) ? -height/2 : 0;

    std::cout << " \n currentRollOverPitch " << currentRollOverPitch  << " yDisplacementRollOver " << yDisplacementRollOver << std::endl;
    if(currentRollOverPitch <= maxRollOverPitch) {

        Mat t_rot = Mat::FullPose(0, -rollOverPitchStep, 0, 0, 0, 0, 1, 1, 1);
        Mat t_trans_1 = Mat::FullPose(0, 0, 0, 0.0, aditional, 0, 1, 1, 1);
        Mat t_trans_2 = Mat::FullPose(0, 0, 0, 0.0, -lastYDisplacement*signal, 0, 1, 1, 1);
        Mat t_trans_3 = Mat::FullPose(0, 0, 0, 0.0, yDisplacementRollOver*signal, 0, 1, 1, 1);

        //tf_car->setMat(tf_car->getMat() * t_trans_2 * t_rot * t_trans_1);
        tf_car->setMat(tf_car->getMat()*t_trans_1*t_trans_3*t_trans_2* t_rot  );
    }
}


void CopCar::turnSirens(bool on) {
    sirenState = on;
    blueSiren->setLightState(on);
    redSiren->setLightState(on);
}

void CopCar::rotateSirens(double angleStep) {
    if(sirenState){
        blueSiren->rotateSiren(angleStep);
        redSiren->rotateSiren(-angleStep);
    }
}

bool CopCar::getSirenState() {
    return sirenState;
}

Transform * CopCar::getTransform() { return tf_car;}

Box CopCar::getBBox() {
    mesh_car->bounding_box();
    Box carBox = mesh_car->getBBox();
    return carBox;
}
Box CopCar::getOriginalBBox() {
    return originalBoundingBox;
}

void CopCar::printBoundingBox() {

    mesh_car->bounding_box();
    Box carBox = mesh_car->getBBox();
    Vec boxMin = carBox.min;
    Vec boxMax = carBox.max;
    double boxLength = std::abs(boxMax.z - boxMin.z);
    double boxWidth = std::abs(boxMax.x - boxMin.x);
    double boxHeight = std::abs(boxMax.y - boxMin.y);
    std::cout << "\n Car bounding box length " << boxLength << " width " << boxWidth << " height " << boxHeight << std::endl;
    std::cout << "\n Car min ";

    boxMin.print();
    std::cout << " max ";
    boxMax.print();
    std::cout << std::endl;
    std::cout << "Car centroid at ";
    mesh_car->getCentroid().print();
    std::cout << std::endl;

}

void CopCar::printTfMat() {
    Mat carMat = tf_car->getMat();
    std::cout << "Car transform matrix" << std::endl;
    carMat.print();
    std::cout << "Car transform only rotation matrix" << std::endl;

    OnlyRot(carMat).print();
}