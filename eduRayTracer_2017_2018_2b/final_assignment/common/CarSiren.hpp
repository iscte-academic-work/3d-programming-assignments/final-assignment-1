//
// Created by abeltenera on 26/11/19.
//

#ifndef FINAL_ASSIGNMENT_CARSIREN_HPP
#define FINAL_ASSIGNMENT_CARSIREN_HPP
#include <stdlib.h>
#include "Scene.hpp"
#include "Transform.hpp"
#include "Mat.hpp"
#include "Material.hpp"
#include "Sphere.hpp"
#include "Cuboid.hpp"
#include "Light.hpp"
#include "../helpers/AppProperties.hpp"

class CarSiren {
public:
    double size; // will be equal for width, height and length

    /*!
     *
     * @param _size
     * @param _decay
     * @param sphereScalingFactor In the current version of eduRayTracer, Sphere is not scalable. So we will just add a modifier to the radius
     *                              This will just solve a problem in the modelling phase, when it is expected for the whole siren to be
     *                              subject to a scaling transform
     * @param _tf_siren
     * @param containerMaterial
     * @param bulbMaterial
     * @param _scene
     * @param _surfaceId
     * @param _transformId
     */
    CarSiren(double _size, double _decay, double sphereScalingFactor, Transform* _tf_siren, Material* containerMaterial, Material* bulbMaterial,
            Scene* _scene, std::string _surfaceId,int _transformId);
    ~CarSiren();
    void rotateSiren(double angleStep);
    void setLightState(bool state);
private:
    Light* light;
    Sphere* lightBulb;
    Cuboid* sirenContainer;
    bool lightState = true;
    Transform* tf_siren;
    std::string surfaceId;
};


#endif //FINAL_ASSIGNMENT_CARSIREN_HPP
