//
// Created by abeltenera on 10/11/19.
//

#include <cmath>
#include <iostream>
#include "Curve3D.hpp"

//! Constructor: passes input to instance, and creates the vertex arrays

Curve3D::Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions) : Curve3D(_w,_h,_l,_startingAngle,_endingAngle,_numberOfDivisions,NULL,NULL,NULL,NULL,NULL,NULL){}



Curve3D::Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions,
                 Material *_topSurfaceMat) : Curve3D(_w,_h,_l,_startingAngle,_endingAngle,_numberOfDivisions,_topSurfaceMat,NULL,NULL,NULL,NULL,NULL){}

Curve3D::Curve3D(double _w, double _h, double _l, double _startingAngle, double _endingAngle, int _numberOfDivisions,
                 Material *_topSurfaceMat, Material *_bottomSurfaceMat, Material *_leftSurfaceMat,
                 Material *_rightSurfaceMat, Material *_frontSurfaceMat, Material *_backSurfaceMat) {
    Material* defaultMaterial = new Material(Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), Vec(0.5,0.5,0.5), 0, 0, 0, false, true, false, false, NULL); // TODO Set this material as an instance object
    topSurfaceMat= _topSurfaceMat == NULL ? defaultMaterial : _topSurfaceMat;
    bottomSurfaceMat=_bottomSurfaceMat == NULL ? defaultMaterial : _bottomSurfaceMat;
    leftSurfaceMat=_leftSurfaceMat == NULL ? defaultMaterial : _leftSurfaceMat;
    rightSurfaceMat=_rightSurfaceMat == NULL ? defaultMaterial : _rightSurfaceMat;
    frontSurfaceMat=_frontSurfaceMat == NULL ? defaultMaterial : _frontSurfaceMat;
    backSurfaceMat=_backSurfaceMat == NULL ? defaultMaterial : _backSurfaceMat;

    width = _w;
    height = _h;
    length = _l;
    startingAngle = _startingAngle;
    endingAngle = _endingAngle;
    std::cout << " \n starting angle " << startingAngle << " ending angle " << endingAngle << std::endl;
    numberOfDivisions = _numberOfDivisions;

    // Calculate the curve ray. ray = length / amplitude.
    amplitude = std::abs(endingAngle - startingAngle);
    amplitudeStep = amplitude/(numberOfDivisions);
    outerRay = length/amplitude;
    innerRay = outerRay - width;
    textureOuterRay = 1;
    textureInnerRay = 1 * innerRay/outerRay;

    // Determine the top surface vertexes

    // Instantiation of the vertex arrays
    // The front and back surfaces (tips of the curve) will only have 4 vertexes
    // The other surfaces will have two times the (number of divisions plus 1)
    frontSurfacePoints = new Vec[4];
    backSurfacePoints = new Vec[4];
    int numberOfVertexes = (numberOfDivisions + 1)*2;
    topSurfacePoints = new Vec[numberOfVertexes];
    bottomSurfacePoints = new Vec[numberOfVertexes];
    outerSurfacePoints = new Vec[numberOfVertexes];
    innerSurfacePoints = new Vec[numberOfVertexes];
    stickerModeTexturePoints =  new Vec[numberOfVertexes];
    distortModeTexturePoints = new Vec[numberOfVertexes];

    // If we have N divisions, we will have N+1 cross sections.
    // We are iterating though cross sections being the first the front surface, and the last the back surface
    // In each iteration, calculate the vertex points
    int k;
    for(int i = 0; i < numberOfDivisions + 1; i++){ // TODO add the texture coordinates
        k = i*2; //i helps iterate over sections. k helps iterate over the point arrays

        // Calculate the current angle of the cross section in XoZ
        double currentAngle = amplitudeStep * i + startingAngle;

        std::cout <<  " current angle " << currentAngle << std::endl;


        double innerPointX = innerRay * sin(currentAngle);
        double outerPointX = outerRay * sin(currentAngle);
        double innerPointZ = innerRay * cos(currentAngle);
        double outerPointZ = outerRay * cos(currentAngle);

        double textureInnerPointU = textureInnerRay * sin(currentAngle);
        double textureOuterPointU = textureOuterRay * sin(currentAngle);
        double textureInnerPointV = textureInnerRay * cos(currentAngle);
        double textureOuterPointV = textureOuterRay * cos(currentAngle);

        double textureDistortPointV = (1.0/numberOfDivisions) * i;

        // Section vertexes
        // This column belongs to inner surface
        //  ||
        //  \/
        //  v1 __v2  => this line belongs to top Surface
        //    |  |
        //  v3|__|v4 => this line belongs to bottom Surface
        //      /\
        //      ||
        //      This column belongs to outer surface
        // This is a representation of the cross section in XoY. In XoZ is just a line
        Vec v1 = Vec(innerPointX, height/2, innerPointZ);
        Vec v2 = Vec(outerPointX, height/2, outerPointZ);
        Vec v3 = Vec(innerPointX, -height/2, innerPointZ);
        Vec v4 = Vec(outerPointX, -height/2, outerPointZ);
        // Front surface
        if(i==0){
            frontSurfacePoints[0] = v1;
            frontSurfacePoints[1] = v2;
            frontSurfacePoints[2] = v3;
            frontSurfacePoints[3] = v4;
        }
        // Back surface
        if(i==numberOfDivisions){
            backSurfacePoints[0] = v1;
            backSurfacePoints[1] = v2;
            backSurfacePoints[2] = v3;
            backSurfacePoints[3] = v4;
        }


        // Top surface
        topSurfacePoints[k] = v1;
        topSurfacePoints[k+1] = v2;
        // Top surface texture points
        stickerModeTexturePoints[k] = Vec(textureInnerPointU, textureInnerPointV);
        stickerModeTexturePoints[k+1] = Vec(textureOuterPointU, textureOuterPointV);
        // Top surface texture points for distortion purposes
        distortModeTexturePoints[k] = Vec(0, textureDistortPointV);
        distortModeTexturePoints[k+1] = Vec(1, textureDistortPointV);
        // Bottom surface
        bottomSurfacePoints[k] = v3;
        bottomSurfacePoints[k+1] = v4;
        // Inner surface
        innerSurfacePoints[k] = v1;
        innerSurfacePoints[k+1] = v3;
        // Outer surface
        outerSurfacePoints[k] = v2;
        outerSurfacePoints[k+1] = v4;
    }

}

Curve3D::~Curve3D() {
    delete [] topSurfacePoints;
    delete [] bottomSurfacePoints;
    delete [] outerSurfacePoints;
    delete [] innerSurfacePoints;
    delete [] backSurfacePoints;
    delete [] frontSurfacePoints;
    delete [] stickerModeTexturePoints;
    delete [] distortModeTexturePoints;
    delete [] topSurfaceMat;
    delete [] bottomSurfaceMat;
    delete [] leftSurfaceMat;
    delete [] rightSurfaceMat;
    delete [] frontSurfaceMat;
    delete [] backSurfaceMat;
}

/*!
 *
 * @param _t
 * @param _m
 * @param _scene
 * @param _jt
 * @param _distortTexture sets the way the vertexes are mapped. If false vertexes will be mapped directly using sin and cos.
 * If not, it will mapped as if the surface was straight, leading to distortion of texture.
 */
 // TODO currently we are just applying textures to the top
void Curve3D::addCurve3DToScene(Transform *_t, Scene *_scene, bool _distortTexture) {

    Vec* bottomTopTexturePoints = _distortTexture ? distortModeTexturePoints : stickerModeTexturePoints;

    // Texture points for the front and back surfaces
    Vec vt1(0,0);
    Vec vt2(1,0);
    Vec vt3(1,1);
    Vec vt4(0,1);


    int k;
    for(int i=0; i < numberOfDivisions; i++){
        k = i*2;
        // Top surface. Note: this surface has a texture
        createTriangles(_scene, topSurfaceMat, _t, topSurfacePoints, bottomTopTexturePoints, numberOfDivisions, i);
        // Bottom surface
        createTriangles(_scene, bottomSurfaceMat, _t, bottomSurfacePoints, bottomTopTexturePoints, numberOfDivisions, i);
        // Inner surface
        createTriangles(_scene, leftSurfaceMat, _t, innerSurfacePoints, distortModeTexturePoints, numberOfDivisions, i);
        // Outer surface
        createTriangles(_scene, rightSurfaceMat, _t, outerSurfacePoints, distortModeTexturePoints, numberOfDivisions, i);

    }

    // Front surface
    createTriangle(_scene, frontSurfaceMat, _t, frontSurfacePoints[2], frontSurfacePoints[0],frontSurfacePoints[1], vt1, vt4, vt3);
    createTriangle(_scene, frontSurfaceMat, _t, frontSurfacePoints[2], frontSurfacePoints[1],frontSurfacePoints[3], vt1, vt3, vt2);

    // Back surface
    createTriangle(_scene, backSurfaceMat, _t, backSurfacePoints[2], backSurfacePoints[0],backSurfacePoints[1], vt1, vt4, vt3);
    createTriangle(_scene, backSurfaceMat, _t, backSurfacePoints[2], backSurfacePoints[1],backSurfacePoints[3], vt1, vt3, vt2);


 }

double Curve3D::getAmplitude() { return amplitude; }
double Curve3D::getInnerRay() { return innerRay; }
double Curve3D::getOuterRay() { return outerRay; }


void Curve3D::setSurfaceId(std::string _surfaceId) {
    surfaceId = _surfaceId;
}


void Curve3D::createTriangles(Scene *scene, Material *m, Transform *tf, Vec *surfacePoints, Vec *texturePoints,
                              int numberOfDivisions, int currentDivision) {

    int k = currentDivision * 2;
    // Top surface. Note: this surface has a texture
    Vec top_vi  = surfacePoints[k];
    Vec top_vi1 = surfacePoints[k+1];
    Vec top_vi2 = surfacePoints[k+2];
    Vec top_vi3 = surfacePoints[k+3];

    Vec top_vti  = texturePoints[k];
    Vec top_vti1 = texturePoints[k+1];
    Vec top_vti2 = texturePoints[k+2];
    Vec top_vti3 = texturePoints[k+3];

    // Triangle 1
    createTriangle(scene, m, tf, top_vi, top_vi3, top_vi1, top_vti, top_vti3, top_vti1);
    // Triangle 2
    createTriangle(scene, m, tf, top_vi, top_vi2, top_vi3, top_vti, top_vti2, top_vti3);

}


void Curve3D::createTriangle(Scene *scene, Material *m, Transform *tf, Vec v1, Vec v2, Vec v3, Vec vt1, Vec vt2,
                            Vec vt3)
{
    Triangle *t;
    // Verifies this Material has a texture
    if(m->tex_map){
        std::cout << "Texture mapping" << std::endl;
        t = new Triangle(new Vertex(v1, vt1), new Vertex(v2, vt2), new Vertex(v3, vt3), true, m, false);
    }
    else{
        std::cout << "Material mapping" << std::endl;
        t = new Triangle(new Vertex(v1), new Vertex(v2), new Vertex(v3), true, m, false);
    }
    t->setTransform(tf);
    scene->addSurface(t, surfaceId);
}