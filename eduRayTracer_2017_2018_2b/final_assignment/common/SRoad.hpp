//
// Created by abeltenera on 02/01/20.
//

#ifndef FINAL_ASSIGNMENT_SROAD_HPP
#define FINAL_ASSIGNMENT_SROAD_HPP

#include "CurvedRoad.hpp"
#include "../helpers/AppProperties.hpp"
/*!
 * Creates an S segment composed by 4 curved road segments
 * All curves are equal, so the constructor will receive the params to build one curve
 * Each segment has an amplitude of PI/2 degrees
 */
class SRoad {
public:
    std::string surfaceIdSuffix = "";

    double rw, rh, rl; // road width, height, length
    double prw, prh; // protection rail width, height, length
    double sw, sh; // sidewalk width, height, length
    double outerPrl; // protection rail length of outer curve
    double innerPrl; // protection rail length of inner curve

    //! startingAngle and endingAngle determine where the curve starts and ends in degrees
    double startingAngle;
    double endingAngle;

    Material *roadTopMaterial;
    Material *sidewalkTopMaterial;
    Material *protectionRailMaterial;

    //! How many divisions to be defined along the curve length
    int numberOfDivisions;

    SRoad(double _rw, double _rh, double _rl,
               double _prw, double _prh, int _railsPerSide,
               double _sw, double _sh, int _numberOfDivisions,
               Material *_roadTopMaterial, Material *_sidewalkTopMaterial, Material *_protectionRailMaterial);
    ~SRoad();
    void addSRoadToScene(Transform* _t, Scene* _scene, bool _distortTexture, bool loadDecorations,
                            double decorationsHeight, const char* treeMeshLocation, const char* streetLampMeshLocation,
                            Material *materialTree, Material *materialLamp);

    double getSRoadBoxWidth();
    double getSRoadBoxLength();

    void setSurfaceIdSuffix(std::string _surfaceIdSuffix);

    /*!
     * The number of trees to be drawn depends on the number of rails on each side of the curve
     * But if it is too much or too little trees, set tree density to a different value.
     * @param _treeDensity
     */
    void setTreeDensity(double _treeDensity){
        treeDensity = _treeDensity;
    }
    /*!
     * The number of street lamps to be drawn depends on the number of rails on each side of the curve
     * But if it is too much or too little street lamps, set street lamp density to a different value.
     * @param _treeDensity
     */
    void setStreetLampDensity(double _streetLampDensity){
        streetLampDensity = _streetLampDensity;
    }
private:
    double treeDensity = 1;
    double streetLampDensity = 1;
    double outerRailAmplitudeStep;
    double innerRailAmplitudeStep;
    double amplitude;
    int numberOfRailsOuterCurve;
    double outerRadius;
    double innerRadius;
    double roadWidth;
    double zDisplacement;
};


#endif //FINAL_ASSIGNMENT_SROAD_HPP
